package com.os.runner.ui.activity.chat

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ServerValue
import com.google.firebase.database.ValueEventListener
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityChatBinding
import com.os.runner.model.DefaultPojo
import com.os.runner.model.UploadImageResponse
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.ChatConstants.*
import com.os.runner.utility.MessageResponse
import com.os.runner.utility.UiHelper
import com.os.runner.utility.UtilityMethod
import java.io.File


class ChatActivity : BaseBindingActivity(), View.OnClickListener{

    var binding: ActivityChatBinding? = null
    private var viewModel: ChatsListViewModel? = null
    private var adapter: FirebaseRecyclerAdapter<MessageResponse, ChatViewHolder>? = null
    private var selectedDate: String = ""
    private var dateMap: HashMap<Int, String>? = HashMap()
    private var mRequestId = ""
    private var mUserId = ""
    private var customer_id = ""

    companion object {

        fun startActivity(activity: Activity, bundle: Bundle?, isClear: Boolean) {
            val intent = Intent(activity, ChatActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            if (isClear) intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.startActivity(intent)
        }
    }

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(ChatsListViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat)
        binding!!.lifecycleOwner = this
    }


    override fun createActivityObject() {
        mActivity = this
        mUserId = App.sessionManager!!.getUserID()
        mRequestId = intent.getBundleExtra("bundle").getString("request_id")!!
        customer_id = intent.getBundleExtra("bundle").getString("customer_id")!!
        mUserId = App.sessionManager!!.getUserID()
    }

    override protected fun onResume() {
        super.onResume()
    }

    override protected fun onPause() {
        super.onPause()
    }

    override fun initializeObject() {
        if (UtilityMethod.hasInternet(mActivity!!)) {
            mDatabase!!.keepSynced(true)
            fetchMessage()
        } else {
            UtilityMethod.showSnackBar(binding!!.imgSend, getString(R.string.network_error), false)
        }
    }


    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })

        viewModel!!.responseSendMessageData.observe(this, Observer {
            handleSendMessageResult(it)
        })

        /*if (request_status.equals("Completed") || request_status.equals("Cancelled")) {
            binding!!.rlChat.visibility = View.GONE
            binding!!.viewtop.visibility = View.GONE
        }*/
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.toolbarLayout.toolbarTitle.text = "Chat"
        binding!!.toolbarLayout.imgBack.visibility = View.VISIBLE
        binding!!.imgSend.setOnClickListener(this)
        binding!!.imgAttachment.setOnClickListener(this)



        binding!!.recyclerView.setOnScrollChangeListener(object : View.OnScrollChangeListener {
            override fun onScrollChange(view: View?, p1: Int, p2: Int, p3: Int, p4: Int) {
                mDatabase!!.child(MESSAGES).child(mRequestId).orderByChild("timestamp")
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(tasksSnapshot: DataSnapshot) {
                            for (snapshot in tasksSnapshot.children) {
                                if (!snapshot.child(SENDER_ID).getValue()!!
                                        .equals(App.sessionManager!!.getUserID())
                                )
                                    snapshot.ref.child(IS_READ).setValue(true)
                            }
                        }

                        override fun onCancelled(error: DatabaseError) {
                            TODO("Not yet implemented")
                        }
                    })
            }

        })

    }

    private fun handleResult(result: ApiResponse<UploadImageResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    sendAMessage(response.data.image_url, "image")
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()

                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }


    private fun handleSendMessageResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                //ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                //ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                //ProgressDialog.hideProgressDialog()
                val response = result.data!!
            }
        }
    }

    override fun onClick(view: View?) {

        when (view!!.id) {
            R.id.imgAttachment -> {
                ImagePicker.with(mActivity!!)
                    .crop()                    //Crop image(Optional), Check Customization for more option
                    .compress(1024)            //Final image size will be less than 1 MB(Optional)
                    .maxResultSize(
                        1080,
                        1080
                    )    //Final image resolution will be less than 1080 x 1080(Optional)
                    .start()
            }
            R.id.imgBack -> finish()
            R.id.imgSend -> {
                val message = binding!!.edtMessage.text.toString().trim()
                if (isValidFormData(message)) {
                    if (UtilityMethod.hasInternet(mActivity!!)) {
                        sendAMessage(message, "text")
                    } else {
                        UtilityMethod.showSnackBar(
                            binding!!.imgSend,
                            getString(R.string.network_error),
                            false
                        )
                    }
                }
            }
        }
    }

    /*"{
    ""user_id"":""511"",
    ""msg_type"":""text"",
    ""message"":""hello art""
}"*/

    /*"sender_id":"517","receiver_id":"564"*/
    private fun sendAMessage(message: String, type: String) {
        var jsonObject = JsonObject()
        jsonObject.addProperty("sender_id", App.sessionManager!!.getUserID())
        jsonObject.addProperty("receiver_id", customer_id)
        jsonObject.addProperty("msg_type", type)
        jsonObject.addProperty("request_id", mRequestId)
        jsonObject.addProperty("message", message)
        viewModel!!.sent_msg_notif(jsonObject)
        binding!!.edtMessage.setText("")
        try {
            val chat_user_ref = "$MESSAGES/$mRequestId" + ""

            val user_message_push = mDatabase!!.child(MESSAGES)
                .child(mRequestId).push()

            val push_id = user_message_push.key
            val req = HashMap<String, Any>()
            req[IS_READ] = false
            req[KEY_ID] = push_id.toString()
            req[Message] = message
            req[TIMESTAMP] = ServerValue.TIMESTAMP
            req[SENDER_ID] = App.sessionManager!!.getUserID()
            req[MESSAGE_TYPE] = type
            req[RECEIVER_ID] = customer_id

            val messageUserMap = HashMap<String, Any>()
            messageUserMap.put("$chat_user_ref/$push_id", req)


            mDatabase!!.child(MESSAGES).child(mRequestId)
                .child(Message)

            mDatabase!!.updateChildren(messageUserMap) { databaseError, databaseReference ->
                if (databaseError != null) {
                    Log.d("CHAT_LOG", databaseError.message)
                } else {

                    mDatabase!!.child(MESSAGES).child(mRequestId)
                        .addListenerForSingleValueEvent(object :
                            ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {

                            }

                            override fun onCancelled(databaseError: DatabaseError) {

                            }
                        })
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun fetchMessage() {
        ProgressDialog.showProgressDialog(mActivity!!)

        try {
            adapter = object : FirebaseRecyclerAdapter<MessageResponse, ChatViewHolder>(
                MessageResponse::class.java,
                R.layout.item_row_chat,
                ChatViewHolder::class.java,
                mDatabase!!.child(MESSAGES).child(mRequestId).orderByChild(
                    "timestamp"
                )
            ) {
                @SuppressLint("SimpleDateFormat")
                override fun populateViewHolder(
                    viewHolder: ChatViewHolder,
                    model: MessageResponse,
                    position: Int
                ) {
                    ProgressDialog.hideProgressDialog()
                    try {
                        selectedDate = UtilityMethod.getDate(model.timestamp!!.toLong())
                        dateMap!![position] = selectedDate
                        if (position > 0) {
                            if (selectedDate != dateMap!![position - 1]) {
                                viewHolder.txtDate.text = UtilityMethod.getFormattedDate(
                                    mActivity!!,
                                    model.timestamp!!.toLong()
                                )
                                viewHolder.txtDate.visibility = View.VISIBLE
                            } else {
                                viewHolder.txtDate.visibility = View.GONE
                            }
                        } else {
                            viewHolder.txtDate.text = UtilityMethod.getFormattedDate(
                                mActivity!!,
                                model.timestamp!!.toLong()
                            )
                            viewHolder.txtDate.visibility = View.VISIBLE
                        }
                        if (model.sender_id == App.sessionManager!!.getUserID()) {
                            if (model.message_type.equals("image")) {
                                viewHolder.ivSenderImage.visibility = View.VISIBLE
                                viewHolder.txtSenderMessage.visibility = View.GONE
                                Glide.with(mActivity!!)
                                    .load(model.message)
                                    .into(viewHolder.ivSenderImage)
                                viewHolder.ivSenderImage.setOnClickListener {
                                    val uri = Uri.parse(model.message)
                                    showPhoto(uri)
                                }
                            } else {
                                viewHolder.ivSenderImage.visibility = View.GONE
                                viewHolder.txtSenderMessage.visibility = View.VISIBLE
                                viewHolder.txtSenderMessage.text = model.message
                            }
                            viewHolder.lLReceiver.visibility = View.GONE
                            viewHolder.lLSender.visibility = View.VISIBLE
                            viewHolder.txtSenderTime.text = UtilityMethod.getTime(model.timestamp!!.toLong())

                            var isRead : Boolean? = null
                            mDatabase!!.child(MESSAGES).child(mRequestId).child(model.keyID).addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                    isRead = dataSnapshot.child("isRead").getValue() as Boolean?
                                    if (isRead!!){
                                        viewHolder.ivIsRead.setImageDrawable(resources.getDrawable(R.drawable.double_tick))
                                    }else{
                                        viewHolder.ivIsRead.setImageDrawable(resources.getDrawable(R.drawable.single_tick))
                                    }
                                }
                                override fun onCancelled(databaseError: DatabaseError) {

                                }
                            })

                        } else {
                            if (model.message_type.equals("image")) {
                                viewHolder.ivReceiverImage.visibility = View.VISIBLE
                                viewHolder.txtReceiverMessage.visibility = View.GONE
                                Glide.with(mActivity!!)
                                    .load(model.message)
                                    .into(viewHolder.ivReceiverImage)
                                viewHolder.ivReceiverImage.setOnClickListener {
                                    val uri = Uri.parse(model.message)
                                    showPhoto(uri)
                                }
                            } else {
                                viewHolder.ivReceiverImage.visibility = View.GONE
                                viewHolder.txtReceiverMessage.visibility = View.VISIBLE
                                viewHolder.txtReceiverMessage.text = model.message
                            }
                            viewHolder.lLSender.visibility = View.GONE
                            viewHolder.lLReceiver.visibility = View.VISIBLE
                            viewHolder.txtReceiverTime.text = UtilityMethod.getTime(model.timestamp!!.toLong())
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } finally {
            ProgressDialog.hideProgressDialog()
        }
        moveToBottom()
        binding!!.recyclerView.adapter = adapter
    }

    private fun moveToBottom() {
        try {
            (adapter as FirebaseRecyclerAdapter<MessageResponse, ChatViewHolder>).registerAdapterDataObserver(
                object :
                    RecyclerView.AdapterDataObserver() {
                    override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                        binding!!.recyclerView.smoothScrollToPosition((adapter as FirebaseRecyclerAdapter<MessageResponse, ChatViewHolder>).itemCount - 1)

                    }
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showPhoto(photoUri: Uri) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.setDataAndType(photoUri, "image/*")
        startActivity(intent)
    }

    class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val lLReceiver = itemView.findViewById<LinearLayout>(R.id.lLReceiver)!!
        val txtDate = itemView.findViewById<TextView>(R.id.txtDate)!!
        val txtReceiverMessage = itemView.findViewById<TextView>(R.id.txtReceiverMessage)!!
        val txtReceiverTime = itemView.findViewById<TextView>(R.id.txtReceiverTime)!!

        val lLSender = itemView.findViewById<LinearLayout>(R.id.lLSender)!!
        val txtSenderMessage = itemView.findViewById<TextView>(R.id.txtSenderMessage)!!
        val txtSenderTime = itemView.findViewById<TextView>(R.id.txtSenderTime)!!
        val ivSenderImage = itemView.findViewById<AppCompatImageView>(R.id.ivSenderImage)!!
        val ivReceiverImage = itemView.findViewById<AppCompatImageView>(R.id.ivReceiverImage)!!
        val ivIsRead = itemView.findViewById<AppCompatImageView>(R.id.ivIsRead)!!
    }

    private fun isValidFormData(address: String): Boolean {
        if (TextUtils.isEmpty(address)) {
            UtilityMethod.showSnackBar(binding!!.imgSend, getString(R.string.enter_message), false)
            return false
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data

            //You can get File object from intent
            val file: File? = ImagePicker.getFile(data)

            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(data)

            viewModel!!.upload_image(file, mRequestId)

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
}