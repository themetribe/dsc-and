package com.os.runner.ui.activity.dashboard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DashboardViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<AvailabilityStatusResponse>>()
    var responseHomeData = MutableLiveData<ApiResponse<HomeCountDataResponse>>()
    var apiResponse: ApiResponse<AvailabilityStatusResponse>? = null
    var apiHomeResponse: ApiResponse<HomeCountDataResponse>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiHomeResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun update_location(jsonObject: JsonObject) {
        subscription = App.apiService!!.update_location(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                //responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    //responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    //responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}