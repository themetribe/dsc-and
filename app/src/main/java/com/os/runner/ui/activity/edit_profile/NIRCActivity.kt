package com.os.runner.ui.activity.edit_profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.maps.model.LatLng
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityBankInformationBinding
import com.app.dstarrunner.databinding.ActivityChangePasswordBinding
import com.app.dstarrunner.databinding.ActivityNircBinding
import com.os.runner.model.Data
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import java.io.File

class NIRCActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: EditProfileViewModel? = null
    private var binding: ActivityNircBinding? = null
    private var frontCardFile: File? = null
    private var backCardFile: File? = null
    private var profileFile: File? = null
    private var pick_type: String? = null
    private var userPojo:Data? =null

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
    }

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_nirc)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        if (intent.extras != null) {
            userPojo = intent.getBundleExtra("bundle")!!.getSerializable("data") as Data
        }
        initControls()
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }

    private fun initControls() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.nirc))
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.frontCardUpload.setOnClickListener(this)
        binding!!.backCardUpload.setOnClickListener(this)
        binding!!.btnSave.setOnClickListener(this)
        binding!!.edtNIRCNumber.setText(userPojo!!.nric_info.nric_number)
        Glide.with(mActivity!!)
            .load(userPojo!!.nric_info.nric_front_image)
            .placeholder(R.drawable.samplefront)
            .into(binding!!.imvFrontCard)

        Glide.with(mActivity!!)
            .load(userPojo!!.nric_info.nric_back_image)
            .placeholder(R.drawable.sampleback)
            .into(binding!!.imvBackCard)


    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.btnSave->{
                var nirc_number = binding!!.edtNIRCNumber.text.toString().trim()
                if(TextUtils.isEmpty(nirc_number)){
                    UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter nric number")
                }else if(frontCardFile==null){
                    UiHelper.showErrorToastMsgShort(mActivity!!, "Please select front card")
                }else if(backCardFile==null){
                    UiHelper.showErrorToastMsgShort(mActivity!!, "Please select back card")
                }else{
                        val reqData: HashMap<String, Any> = HashMap()
                        reqData["user_id"] = App.sessionManager!!.getUserID()
                        reqData["first_name"] = ""
                        reqData["last_name"] = ""
                        reqData["email"] = ""
                        reqData["country_code"] = ""
                        reqData["mobile"] = ""
                        reqData["address"] = ""
                        reqData["bank_name"] = ""
                        reqData["account_number"] = ""
                        reqData["account_holder_name"] = ""
                        reqData["nric_number"] = nirc_number
                        reqData["declaration"] = ""
                        reqData["branch_code"] = ""
                        reqData["bank_code"] = ""
                        reqData["postal_code"] = ""
                        viewModel!!.runner_edit_profile(
                            reqData,
                            profileFile,
                            frontCardFile,
                            backCardFile
                        )
                }
            }
            R.id.frontCardUpload -> {
                pick_type = "front"
                ImagePicker.with(mActivity!!)
                    .crop()                    //Crop image(Optional), Check Customization for more option
                    .compress(1024)            //Final image size will be less than 1 MB(Optional)
                    .maxResultSize(
                        1080,
                        1080
                    )    //Final image resolution will be less than 1080 x 1080(Optional)
                    .start()
            }
            R.id.backCardUpload -> {
                pick_type = "back"
                ImagePicker.with(mActivity!!)
                    .crop()                    //Crop image(Optional), Check Customization for more option
                    .compress(1024)            //Final image size will be less than 1 MB(Optional)
                    .maxResultSize(
                        1080,
                        1080
                    )    //Final image resolution will be less than 1080 x 1080(Optional)
                    .start()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data

            if(pick_type.equals("front")){
                //You can get File object from intent
                frontCardFile = ImagePicker.getFile(data)
                Glide.with(this)
                    .load(frontCardFile)
                    .into(binding!!.imvFrontCard)
            }else{
                //You can get File object from intent
                backCardFile = ImagePicker.getFile(data)
                Glide.with(this)
                    .load(backCardFile)
                    .into(binding!!.imvBackCard)
            }


            //You can get File object from intent
            val file: File? = ImagePicker.getFile(data)

            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(data)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, NIRCActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent,333)
        }
    }

    private fun handleResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    App.sessionManager!!.setUserData(response.data)
                    setResult(111)
                    finish()
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()

                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgLong(mActivity!!, response.message)
                }
            }
        }
    }

}
