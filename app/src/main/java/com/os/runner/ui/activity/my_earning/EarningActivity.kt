package com.os.runner.ui.activity.my_earning

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.dstarrunner.R
import com.app.dstarrunner.databinding.ActivityEarningBinding
import com.app.dstarrunner.databinding.ActivityPendingRequestBinding
import com.os.runner.model.*
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.adapter.EarningAdapter
import kotlinx.android.synthetic.main.custom_toolbar.view.*

class EarningActivity : BaseBindingActivity(), View.OnClickListener {


    private var binding: ActivityEarningBinding? = null
    private var viewModel: MyEarningViewModel? = null

    /*Pending Requests Adapter*/
    private var earningDataList: ArrayList<EarningData>? = ArrayList()
    private var earningAdapter: EarningAdapter? = null
    private var onClickListener: View.OnClickListener? = null
    var dataMyEarningResponse: DataMyEarning? = null


    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(MyEarningViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_earning)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        if (intent.extras != null) {
            dataMyEarningResponse = intent.getBundleExtra("bundle")!!.getSerializable("data") as DataMyEarning
        }
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, EarningActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    override fun setListeners() {

    }

    private fun initControl() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.earning))
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerView.setLayoutManager(mLayoutManager)

        earningDataList!!.addAll(dataMyEarningResponse!!.earning_data)
        earningAdapter = EarningAdapter(this, earningDataList!!, onClickListener)
        binding!!.recyclerView.adapter = earningAdapter
        earningAdapter!!.notifyDataSetChanged()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }
}