package com.os.runner.ui.fragment.home

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.FragmentHomeBinding
import com.os.runner.model.AvailabilityStatusResponse
import com.os.runner.model.HomeCountDataResponse
import com.os.runner.ui.activity.notification_list.NotificationActivity
import com.os.runner.ui.activity.pending_requests.PendingRequestsActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.ui.fragment.BaseFragment
import com.os.runner.utility.Constants
import com.os.runner.utility.LocationProvider
import com.os.runner.utility.UiHelper

class HomeFragment : BaseFragment(), View.OnClickListener, OnMapReadyCallback, LocationListener {

    private var binding: FragmentHomeBinding? = null
    var viewModel: HomeViewModel? = null

    private var mGoogleMap: GoogleMap? = null
    private var locationProvider: LocationProvider? = null
    private var coordinate: LatLng? = null
    private val errorCallback = LocationProvider.ErrorCallback { }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding!!.lifecycleOwner = this
        return binding!!
    }

    override fun createActivityObject() {
        mActivity = activity
    }

    override fun initializeObject() {
        initControl()
        initilizeMap()
        locationProvider = LocationProvider(mActivity!!, this, errorCallback)
    }


    private fun initControl() {
        binding!!.imvNotification.setOnClickListener(this)
        binding!!.btnOnlineOffline.setOnClickListener(this)
    }

    private fun initilizeMap() {
        try {
            val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
            mapFragment?.getMapAsync(this)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun setListeners() {
        binding!!.btPendingRequest.setOnClickListener {
            PendingRequestsActivity.startActivity(mActivity!!, null)
        }
        viewModel!!.responseLiveData.observe(this, Observer {
             handleResult(it)
         })

        viewModel!!.responseHomeData.observe(this, Observer {
            handleHomeResult(it)
        })
    }

    private fun handleResult(result: ApiResponse<AvailabilityStatusResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    App.sessionManager!!.setAvailabilityStatus(response.data.status)
                    if(App.sessionManager!!.getAvailabilityStatus().equals("online")){
                        binding!!.btnOnlineOffline.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_button_red))
                        binding!!.btnOnlineOffline.setText("Go Offline")
                    }else{
                        binding!!.btnOnlineOffline.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_button_green))
                        binding!!.btnOnlineOffline.setText("Go Online")
                    }
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun handleHomeResult(result: ApiResponse<HomeCountDataResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if(response.data.unread_notiification.toInt()>0){
                        binding!!.rlNotificationCount.visibility = View.VISIBLE
                        binding!!.txtNotificationCount.setText(response.data.unread_notiification)
                    }
                    if(response.data.pending_request.toInt()>0){
                        binding!!.txtRequestCount.setText(response.data.pending_request)
                        binding!!.rlRequestCount.visibility = View.VISIBLE
                    }
                    App.sessionManager!!.setAvailabilityStatus(response.data.view_status)
                    App.sessionManager!!.setRunnerPosition(response.data.current_position)

                    if(App.sessionManager!!.getAvailabilityStatus().equals("online")){
                        binding!!.btnOnlineOffline.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_button_red))
                        binding!!.btnOnlineOffline.setText("Go Offline")
                    }else{
                        binding!!.btnOnlineOffline.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_button_green))
                        binding!!.btnOnlineOffline.setText("Go Online")
                    }
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imvNotification -> {
                NotificationActivity.startActivity(context as Activity, null)
            }
            R.id.btnOnlineOffline->{
                var status =""
                if(App.sessionManager!!.getAvailabilityStatus().equals("online")){
                    status = "offline"
                }else{
                    status = "online"
                }
                availabilityStatusCall(status)
            }
        }
    }

    private fun availabilityStatusCall(status: String) {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty("status", status)
        viewModel!!.availability_status(jsonObject)
    }


    private fun homeDataCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        viewModel!!.home_data_count(jsonObject)
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        mGoogleMap = googleMap
        if (ActivityCompat.checkSelfPermission(
                mActivity!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mActivity!!, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        val style = MapStyleOptions.loadRawResourceStyle(mActivity!!, R.raw.style_json)
        mGoogleMap!!.setMapStyle(style)
        mGoogleMap!!.uiSettings.isMyLocationButtonEnabled = false
        //mGoogleMap!!.isMyLocationEnabled = true
        mGoogleMap!!.setPadding(20, 200, 20, 20)

        if(coordinate!=null){
            val markerOptions = MarkerOptions().position(coordinate!!) .icon(
                BitmapDescriptorFactory.fromResource(R.drawable.logoruner))
            mGoogleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 12f))
            mGoogleMap!!.addMarker(markerOptions)
        }
    }

    override fun onLocationChanged(location: Location?) {
        var latitude = 0.00
        var longitude = 0.00
        if(location!!.latitude==null&&location.longitude==null){
            latitude  =  App.sessionManager!!.getLatitute()!!.toDouble()
            longitude  =  App.sessionManager!!.getLongitute()!!.toDouble()
            coordinate = LatLng(latitude, longitude)
        }else{
            latitude  =  location.latitude
            longitude  =  location.longitude
            coordinate = LatLng(latitude, longitude)
        }
    }



    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }


    override fun onResume() {
        homeDataCall()
        super.onResume()
        if (locationProvider != null) {
            locationProvider!!.onResume()
        }
    }

}