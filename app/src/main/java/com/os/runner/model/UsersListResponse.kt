package com.os.runner.model

import java.io.Serializable

data class UsersListResponse(
    val response: ResponseUsersList
)

data class ResponseUsersList(
    val data: List<UsersData>,
    val message: String,
    val status: Int
)

data class UsersData(
    var id: Int=0,
    val type: String="",
    val name: String="",
    val mobile: String="",
    val email: String="",
    val email_verified_at: String="",
    val isOtpVerified: String="",
    val snapchat_id: String="",
    val gender: String="",
    val age: String="",
    val dob: String="",
    val country: String="",
    val bio: String="",
    val show_dob: Int=0,
    val search_gender: String="",
    val search_country: String="",
    val want_to_meet: String="",
    val image: String="",
    val device_id: String,
    val device_type: String="",
    val latitude: String="",
    val longitude: String="",
    val gems: Int=0,
    val status: String="",
    val created_at: String="",
    val distance: Int=0,
    val `all_images`: List<String>,
    val updated_at: String=""
): Serializable