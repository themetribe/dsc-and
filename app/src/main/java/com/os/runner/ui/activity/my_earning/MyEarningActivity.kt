package com.os.runner.ui.activity.my_earning

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityMyEarningBinding
import com.os.runner.model.DataMyEarning
import com.os.runner.model.DefaultPojo
import com.os.runner.model.MyEarningResponse
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.MyEarningAdapter
import com.os.runner.ui.dialogs.AlertMessageDialog
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_view.view.*
import android.widget.EditText


class MyEarningActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: MyEarningViewModel? = null
    private var binding: ActivityMyEarningBinding? = null
    /*My Earning Adapter*/
    private var dataMyEarning: ArrayList<DataMyEarning>? = ArrayList()
    private var myEarningAdapter: MyEarningAdapter? = null
    private var onClickListener: View.OnClickListener? = null
    var myEarningResponse: MyEarningResponse? = null


    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
    }

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(MyEarningViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_earning)
        onClickListener=this
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControls()
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
        viewModel!!.responseLivePayoutData.observe(this, Observer {
            handlePayoutResult(it)
        })
    }

    override fun onResume() {
        super.onResume()
        binding!!.txtMyBalance.setText(App.sessionManager!!.getWalletAMOUNT())
        wallet_historyCall()
    }

    private fun initControls() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.my_earning))
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.btnWithdrawRequest.setOnClickListener(this)

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerView.setLayoutManager(mLayoutManager)

        myEarningAdapter = MyEarningAdapter(this, dataMyEarning!!, onClickListener)
        binding!!.recyclerView.adapter = myEarningAdapter

    }

    private fun wallet_historyCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.RUNNER_ID, App.sessionManager!!.getUserID())
        viewModel!!.my_earning(jsonObject)
    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.cvEarningView->{
                val position = v.tag as Int
                var bundle= Bundle()
                bundle.putSerializable("data", myEarningResponse!!.data!![position])
                EarningActivity.startActivity(this,bundle)

            }
            R.id.btnWithdrawRequest->{

                val withdrawAmount: EditText = findViewById(R.id.withdrawAmount)
                if(withdrawAmount.text.toString().trim().isEmpty()) {
                    UiHelper.showErrorToastMsgShort(mActivity!!, "Enter amount you want to withdraw")

                }else{
                    AlertMessageDialog.showDialog(this,
                    "Withdraw Request",
                    getString(R.string.do_you_really_want_to_withdraw),
                    "Yes",
                    getString(R.string.cancel_alert),
                    object : AlertMessageDialog.AlertMessageCallback {
                        override fun onSuccessClick() {
                            var jsonObject = JsonObject()
                            jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
                            viewModel!!.payout(jsonObject)
                        }

                        override fun onCancelClick() {

                        }
                    })
                }


            }
        }
    }



    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, MyEarningActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    private fun handlePayoutResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun handleResult(result: ApiResponse<MyEarningResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    myEarningResponse = response
                    if (response.data.size==0) {
                        binding!!.txtNoDataFound.visibility = View.VISIBLE
                        binding!!.txtNoDataFound.txtMessage.text = response.message
                        binding!!.recyclerView.visibility = View.GONE
                    } else {
                        binding!!.txtNoDataFound.visibility = View.GONE
                        binding!!.recyclerView.visibility = View.VISIBLE
                        setAdapters(response.data)
                    }
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setAdapters(data: List<DataMyEarning>) {
        if(data!=null){
            dataMyEarning!!.clear()
            dataMyEarning!!.addAll(data)
            myEarningAdapter!!.notifyDataSetChanged()
        }
    }

}
