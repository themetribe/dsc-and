package com.os.runner.ui.activity.verify_mobile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.os.drunnercustomer.ui.activity.verify_mobile.VerifyMobileViewModel
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.ResendOtpResponse
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.edit_profile.EditProfileActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.activity_verify_mobile.*
import java.util.concurrent.TimeUnit

class VerifyMobileActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: VerifyMobileViewModel? = null
    private var otp = ""
    private var mobile = ""
    private var country_code = ""
    private var latitude = 0.0
    private var longitude = 0.0
    private var fromActivity: String? = null

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
        if (latLng != null) {
            latitude = latLng.latitude
            longitude = latLng.longitude
        }
    }


    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(VerifyMobileViewModel::class.java)
        setContentView(R.layout.activity_verify_mobile)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        otp = intent.getBundleExtra("bundle").getString("otp")!!
        mobile = intent.getBundleExtra("bundle").getString("mobile")!!
        country_code = intent.getBundleExtra("bundle").getString("country_code")!!
        fromActivity = intent.getBundleExtra("bundle").getString("fromActivity")!!
        initControls()
    }

    override fun setListeners() {
       /* val yourArray: String? = otp
        val one = yourArray!!.substring(0, 1)
        val two = yourArray!!.substring(1, 2)
        val three = yourArray!!.substring(2, 3)
        val four= yourArray!!.substring(3, 4)
        edtOne.setText(one)
        edtTwo.setText(two)
        edtThree.setText(three)
        edtFour.setText(four)*/
        //Toast.makeText(this,otp, Toast.LENGTH_LONG).show()
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })

        viewModel!!.responseResendOtpData.observe(this, Observer {
            handleResendResult(it)
        })
    }
    private fun initControls() {
        imvBack!!.visibility = View.VISIBLE
        imvBack!!.setOnClickListener(this)
        btnVerify!!.setOnClickListener(this)
        txtMobileNumber!!.setText("Please enter verification code sent to\n"+country_code+"-"+mobile)
        txtResendOtp!!.setOnClickListener(this)
        onTextChange(edtOne, edtOne, edtTwo)
        onTextChange(edtOne, edtTwo, edtThree)
        onTextChange(edtTwo, edtThree, edtFour)
        onTextChange(edtThree, edtFour, edtFour)

        val countDownTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.d("tekloon", "millisUntilFinished $millisUntilFinished")
                val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                txtResendOtp.setText(getString(R.string.resend_otp)+"("+seconds+"s)")
                txtResendOtp.isClickable=false
            }
            override fun onFinish() {
                Log.d("tekloon", "onFinish")
                txtResendOtp.setText(getString(R.string.resend_otp))
                txtResendOtp.isClickable=true
            }
        }
        countDownTimer.start()
    }


    private fun onTextChange(
        edtFirst: EditText,
        edtCurrent: EditText,
        edtLast: EditText
    ) {
        edtCurrent.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action !== KeyEvent.ACTION_DOWN) return@OnKeyListener false
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                if (TextUtils.isEmpty(edtCurrent.text.toString())) {
                    edtFirst.setText("")
                    edtFirst.requestFocus()
                }
            }
            false
        })
        edtCurrent.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                Handler().postDelayed(Runnable {
                    if (edtCurrent.length() > 0) {
                        edtLast.requestFocus()
                    } else {
                        edtCurrent.requestFocus()
                    }
                }, 100)
            }
            override fun afterTextChanged(s: Editable) {}
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imvBack -> {
                onBackPressed()
            }
            R.id.txtResendOtp->{
                resend_otpAPI()
            }
            R.id.btnVerify -> {
                UiHelper.hideKeyboard(this)
                val one = edtOne.text.toString().trim()
                val two = edtTwo.text.toString().trim()
                val three = edtThree.text.toString().trim()
                val four = edtFour.text.toString().trim()
                val otp = one + two + three + four
                if (isValidFormData(one,two,three,four)) {
                    var jsonObject = JsonObject()
                    jsonObject.addProperty("user_id", App.sessionManager!!.getUserID())
                    jsonObject.addProperty(Constants.MOBILE, mobile)
                    jsonObject.addProperty(Constants.LATITUDE_NAME, latitude)
                    jsonObject.addProperty(Constants.LONGITUDE_NAME, longitude)
                    jsonObject.addProperty(Constants.DEVICE_ID, App.sessionManager!!.getDeviceToken())
                    jsonObject.addProperty(Constants.DEVICE_TYPE, "android")
                    jsonObject.addProperty("otp", otp)
                    viewModel!!.verify_otp(jsonObject)
                }
            }
        }
    }


    private fun isValidFormData(
        one: String,
        two: String,
        three: String,
        four: String
    ): Boolean {

        if (TextUtils.isEmpty(one) || TextUtils.isEmpty(two) || TextUtils.isEmpty(three) || TextUtils.isEmpty(four)) {
            UiHelper.showErrorToastMsgShort(mActivity!!,getString(R.string.otp_not_empty))
            return false
        }
        return true
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, VerifyMobileActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent,123)
        }
    }

    private fun handleResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!,getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    App.sessionManager!!.setLogin(true)
                    if(fromActivity.equals("account")){
                        setResult(222)
                        finish()
                    }else{
                        val bundle = Bundle()
                        bundle.putSerializable("fromActivity", "splash")
                        EditProfileActivity.startActivity(mActivity!!, bundle,true)
                    }
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!,response.message)
                }
            }
            }
        }

    private fun resend_otpAPI() {
        var jsonObject = JsonObject()
        jsonObject.addProperty("user_id", App.sessionManager!!.getUserID())
        jsonObject.addProperty(Constants.COUNTRY_CODE, country_code)
        jsonObject.addProperty(Constants.MOBILE, mobile)
        viewModel!!.resend_otp(jsonObject)
    }

    private fun handleResendResult(result: ApiResponse<UserPojo>?) {

        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!,getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    //Toast.makeText(this, "" + response.data.otp, Toast.LENGTH_LONG).show()
                    val countDownTimer = object : CountDownTimer(60000, 1000) {
                        override fun onTick(millisUntilFinished: Long) {
                            Log.d("tekloon", "millisUntilFinished $millisUntilFinished")
                            val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                            txtResendOtp.setText("Resend OTP("+seconds+"s)")
                            txtResendOtp.isClickable=false
                        }
                        override fun onFinish() {
                            Log.d("tekloon", "onFinish")
                            txtResendOtp.setText("Resend OTP")
                            txtResendOtp.isClickable=true
                        }
                    }
                    countDownTimer.start()
                    UiHelper.showSucessToastMsgShort(mActivity!!,response.message)
                } else {
                    UiHelper.showSucessToastMsgShort(mActivity!!,response.message)
                }
            }
        }
    }

}
