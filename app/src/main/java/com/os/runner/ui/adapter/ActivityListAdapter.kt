package com.os.runner.ui.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.app.dstarrunner.R
import com.os.runner.model.DataRequest
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.row_inprogress.view.*
import kotlinx.android.synthetic.main.row_inprogress.view.cvInProgress
import kotlinx.android.synthetic.main.row_inprogress.view.imvCustomerImage
import kotlinx.android.synthetic.main.row_inprogress.view.txtBuyingLocationAddress
import kotlinx.android.synthetic.main.row_inprogress.view.txtBuyingLocationName
import kotlinx.android.synthetic.main.row_inprogress.view.txtCreatedDate
import kotlinx.android.synthetic.main.row_inprogress.view.txtCustomerName
import kotlinx.android.synthetic.main.row_inprogress.view.txtDropOffLocationAddress
import kotlinx.android.synthetic.main.row_inprogress.view.txtServiceName

class ActivityListAdapter(
    private val context: Activity?,
    private val list: ArrayList<DataRequest>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_inprogress

    override fun getViewModel(position: Int): Any? = list[position]

    @SuppressLint("SetTextI18n")
    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        if(list[position].service_name.equals("Help Me Queue")){
            viewDataBinding.root.llHelpMeSendBuy.visibility = View.GONE
            viewDataBinding.root.rlHelpMeQueue.visibility = View.VISIBLE
            viewDataBinding.root.txtCustomerAddress.text = list[position].pickup_info.address
            if(list[position].status.equals("Completed")&&list[position].is_rating.equals("0")){
                viewDataBinding.root.txtHelpMeQueueRating.visibility = View.VISIBLE
                viewDataBinding.root.txtHelpMeQueueRating.setText("Rate")
            }else{
                if(list[position].status.equals("Completed")&&list[position].is_rating.equals("1")){
                    viewDataBinding.root.txtHelpMeQueueRating.visibility = View.VISIBLE
                    viewDataBinding.root.txtHelpMeQueueRating.setText("Rated")
                    viewDataBinding.root.txtHelpMeQueueRating.isEnabled = false
                }
            }
        }else{
            if(list[position].service_name.equals("Help Me Buy")){
                viewDataBinding.root.txtBuyingLocationName.text = "Buying Location"
            }else{
                viewDataBinding.root.txtBuyingLocationAddress.text = "Pickup Location"
            }
            viewDataBinding.root.txtDropOffLocationName.text = "Drop Off Location"+"("+list[position].drop_info.size+" Stop)"

            if(list[position].status.equals("Completed")&&list[position].is_rating.equals("0")){
                viewDataBinding.root.txtHelpMeSendBuyRating.visibility = View.VISIBLE
                viewDataBinding.root.txtHelpMeSendBuyRating.setText("Rate")
            }else{
                if(list[position].status.equals("Completed")&&list[position].is_rating.equals("1")){
                    viewDataBinding.root.txtHelpMeSendBuyRating.visibility = View.VISIBLE
                    viewDataBinding.root.txtHelpMeSendBuyRating.setText("Rated")
                    viewDataBinding.root.txtHelpMeSendBuyRating.isEnabled = false
                }
            }
            viewDataBinding.root.llHelpMeSendBuy.visibility = View.VISIBLE
            viewDataBinding.root.rlHelpMeQueue.visibility = View.GONE
            viewDataBinding.root.txtBuyingLocationAddress.text = list[position].pickup_info.address
            if(list[position].drop_info.size!=0){
                viewDataBinding.root.txtDropOffLocationAddress.text = list[position].drop_info[0].address
            }
        }
        Glide.with(context!!).load(list[position].user_image).placeholder(R.drawable.default_user_pic).into(viewDataBinding.root.imvCustomerImage)
        viewDataBinding.root.txtServiceName.text = list[position].service_name
        viewDataBinding.root.txtCustomerName.text = list[position].user_name
        viewDataBinding.root.txtStatus.text = list[position].status
        viewDataBinding.root.txtCreatedDate.text = UtilityMethod.parseDateFormated(list[position].schedule_date_time)
        viewDataBinding.root.txtHelpMeQueueCancel.tag = position
        viewDataBinding.root.txtHelpMeSendBuyCancel.tag = position
        viewDataBinding.root.cvInProgress.tag = position
        viewDataBinding.root.txtHelpMeQueueRating.tag = position
        viewDataBinding.root.txtHelpMeSendBuyRating.tag = position
        viewDataBinding.root.cvInProgress.setOnClickListener(onClickListener)
        viewDataBinding.root.txtHelpMeQueueRating.setOnClickListener(onClickListener)
        viewDataBinding.root.txtHelpMeSendBuyRating.setOnClickListener(onClickListener)
    }

    override fun getItemCount(): Int = list.size
}