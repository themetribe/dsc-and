package com.os.runner.ui.activity.pending_requests

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import com.os.runner.model.RequestResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PendingRequestsViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<RequestResponse>>()
    var responseAcceptRejectData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiResponse: ApiResponse<RequestResponse>? = null
    var apiAcceptRejectResponse: ApiResponse<DefaultPojo>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiAcceptRejectResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun requests_for_runner(jsonObject: JsonObject) {
        subscription = App.apiService!!.requests_for_runner(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }

    fun accept_request(jsonObject: JsonObject) {
        subscription = App.apiService!!.accept_request(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAcceptRejectData.postValue(apiAcceptRejectResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseAcceptRejectData.postValue(apiAcceptRejectResponse!!.success(result))
                },
                { throwable ->
                    responseAcceptRejectData.postValue(apiAcceptRejectResponse!!.error(throwable))
                }
            )
    }

    fun decline_request(jsonObject: JsonObject) {
        subscription = App.apiService!!.decline_request(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAcceptRejectData.postValue(apiAcceptRejectResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseAcceptRejectData.postValue(apiAcceptRejectResponse!!.success(result))
                },
                { throwable ->
                    responseAcceptRejectData.postValue(apiAcceptRejectResponse!!.error(throwable))
                }
            )
    }

    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}