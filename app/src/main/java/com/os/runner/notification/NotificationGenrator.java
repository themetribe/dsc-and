package com.os.runner.notification;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.core.app.NotificationCompat;

import com.app.dstarrunner.R;
import com.os.runner.ui.activity.dashboard.DashboardActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Random;

public class NotificationGenrator {


    private NotificationUtils mNotificationUtils;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint("NewApi")
    public void generateNotification(Context context, @NotNull Map<String, String> body) {
        String title = "";
        if (TextUtils.isEmpty(body.get("title"))) {
            title = context.getResources().getString(R.string.app_name);
        }else{
            title = body.get("title");
        }
        Intent notificationIntent;
        notificationIntent = new Intent(context, DashboardActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("notification_type", body.get("notification_type"));
        bundle.putString("service_type", body.get("service_type"));
        bundle.putString("receiver_id", body.get("receiver_id"));
        bundle.putString("request_id", body.get("request_id"));
        bundle.putString("sender_id", body.get("sender_id"));
        notificationIntent.putExtra("bundle", bundle);
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        PendingIntent intent = PendingIntent.getActivity(context, m, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap largeIcon = BitmapFactory.decodeResource((context).getResources(), R.mipmap.ic_launcher_round);
        Log.d("generateNotification", "11111");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.d("generateNotification", "2222");
            mNotificationUtils = new NotificationUtils(context);
            Notification.Builder nb = mNotificationUtils.
                    getAndroidChannelNotification(context, title, body.get("message"), intent);
            mNotificationUtils.getManager().notify(m, nb.build());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.d("generateNotification", "3333");
            Resources res = context.getResources();
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setContentTitle(title)
                    .setContentText(body.get("message")).setContentIntent(intent)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    // .setPriority(Notification.PRIORITY_MAX)
                    .setAutoCancel(true)
                    .setWhen(System.currentTimeMillis())
                    .setTicker(context.getString(R.string.app_name))
                    .setLargeIcon(largeIcon);
            Notification n = mBuilder.build();
            n.flags |= Notification.FLAG_AUTO_CANCEL;
            n.defaults |= Notification.DEFAULT_VIBRATE;
            NotificationManager notifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notifyMgr.notify(m, mBuilder.build());
            // notificationManager.notify(m,n);

        } else {
            Log.d("generateNotification", "4444");
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notif = new Notification.Builder(context).setContentTitle(title).setContentText(body.get("message")).setContentIntent(intent).setSmallIcon(R.mipmap.ic_launcher_round).setLargeIcon(largeIcon).setStyle(new Notification.BigTextStyle().bigText(body.get("message")))
                    ///  .setStyle(new Notification.BigPictureStyle().bigPicture(largeIcon)) /
                    .build();
            int smallIconId = context.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());
            Log.d("smallIconId", "" + smallIconId);
            if (smallIconId != 0) {
                notif.contentView.setViewVisibility(smallIconId, View.INVISIBLE);
                notif.bigContentView.setViewVisibility(smallIconId, View.INVISIBLE);
            }
            notif.flags |= Notification.FLAG_AUTO_CANCEL;
            // Play default notification sound
            notif.defaults |= Notification.DEFAULT_SOUND;
            notif.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
            // Vibrate if vibrate is enabled
            notif.defaults |= Notification.DEFAULT_VIBRATE;
            notificationManager.notify(m, notif);
        }

    }
}
