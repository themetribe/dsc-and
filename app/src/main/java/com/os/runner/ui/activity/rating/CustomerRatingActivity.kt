package com.os.runner.ui.activity.rating

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityCustomerRatingBinding
import com.os.runner.model.DefaultPojo
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.dashboard.DashboardActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*

class CustomerRatingActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: CustomerRatingViewModel? = null
    private var binding: ActivityCustomerRatingBinding? = null
    private var mRequestId = ""
    private var mUserId = ""

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
    }

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(CustomerRatingViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_customer_rating)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControls()
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }

    private fun initControls() {
        binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.cutomer_rating)
        binding!!.btnSubmit.setOnClickListener(this)
        mRequestId = intent.getBundleExtra("bundle").getString("request_id")!!
        mUserId = intent.getBundleExtra("bundle").getString("user_id")!!
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.btnSubmit -> {
                var review = binding!!.edtComment.text.toString().trim()

                if(isValidFormData(review)){
                    add_rating()
                }
            }
        }
    }


    private fun isValidFormData(
        review: String
    ): Boolean {
        if (TextUtils.isEmpty(review)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.please_enter_your_review))
            return false
        }
        return true
    }

    private fun add_rating() {
        var jsonObject = JsonObject()
        jsonObject.addProperty("given_by", App.sessionManager!!.getUserID())
        jsonObject.addProperty("accept_by", mUserId)
        jsonObject.addProperty("request_id", mRequestId)
        jsonObject.addProperty("tip", "")
        jsonObject.addProperty("rating", binding!!.ratingBar.rating.toString())
        jsonObject.addProperty("review_title", "")
        jsonObject.addProperty("review", binding!!.edtComment.text.toString().trim())
        viewModel!!.add_rating(jsonObject)
    }


    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, CustomerRatingActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    private fun handleResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!,getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    DashboardActivity.startActivity(mActivity!!, null, true)
                } else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                }else {
                    UiHelper.showErrorToastMsgShort(mActivity!!,response.message)
                }
            }
        }
    }

}
