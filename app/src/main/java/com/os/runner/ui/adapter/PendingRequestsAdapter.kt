package com.os.runner.ui.adapter

import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.app.dstarrunner.R
import com.os.runner.model.DataRequest
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.row_inprogress.view.*
import kotlinx.android.synthetic.main.row_pending_requests.view.*
import kotlinx.android.synthetic.main.row_pending_requests.view.cvInProgress
import kotlinx.android.synthetic.main.row_pending_requests.view.imvCustomerImage
import kotlinx.android.synthetic.main.row_pending_requests.view.txtBuyingLocationAddress
import kotlinx.android.synthetic.main.row_pending_requests.view.txtBuyingLocationName
import kotlinx.android.synthetic.main.row_pending_requests.view.txtCreatedDate
import kotlinx.android.synthetic.main.row_pending_requests.view.txtCustomerName
import kotlinx.android.synthetic.main.row_pending_requests.view.txtDropOffLocationAddress
import kotlinx.android.synthetic.main.row_pending_requests.view.txtDropOffLocationName
import kotlinx.android.synthetic.main.row_pending_requests.view.txtServiceName

class PendingRequestsAdapter(
    private val context: Activity?,
    private val list: ArrayList<DataRequest>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_pending_requests

    override fun getViewModel(position: Int): Any? = list[position]
    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {

        Glide.with(context!!).load(list[position].user_image).placeholder(R.drawable.default_user_pic).into(viewDataBinding.root.imvCustomerImage)
        viewDataBinding.root.txtCustomerName.text = list[position].user_name
        viewDataBinding.root.txtServiceName.text = list[position].service_name
        viewDataBinding.root.txtRequestAmount.text = context.getString(R.string.singapur_doller) + " " +list[position].amount
        if(!list[position].tip.equals("0.00")){
            viewDataBinding.root.txtTipAmount.text = "Tip : "+context.getString(R.string.singapur_doller) + " " +list[position].tip
            viewDataBinding.root.txtTipAmount.visibility=View.VISIBLE
        }else{
            viewDataBinding.root.txtTipAmount.visibility=View.GONE
        }
        viewDataBinding.root.txtCreatedDate.text = UtilityMethod.parseDateFormated(list[position].schedule_date_time)


        if(list[position].service_name.equals("Help Me Queue")){
            viewDataBinding.root.rlPickupLocation.visibility = View.GONE
            viewDataBinding.root.rlDropOffLocation.visibility = View.GONE
            viewDataBinding.root.rlQueueLocation.visibility = View.VISIBLE
            viewDataBinding.root.txtQueueLocationAddress.setText(list[position].pickup_info.address)
        }else{
            if(list[position].service_name.equals("Help Me Buy")){
                viewDataBinding.root.txtBuyingLocationName.text = "Buying Location"
            }else{
                viewDataBinding.root.txtBuyingLocationName.text = "Pick up Location"
            }

            viewDataBinding.root.txtDropOffLocationName.text = "Drop Off Location"+"("+list[position].drop_info.size+" Stops)"

            viewDataBinding.root.rlPickupLocation.visibility = View.VISIBLE
            viewDataBinding.root.rlDropOffLocation.visibility = View.VISIBLE
            viewDataBinding.root.rlQueueLocation.visibility = View.GONE
            viewDataBinding.root.txtBuyingLocationAddress.text = list[position].pickup_info.address
            viewDataBinding.root.txtDropOffLocationAddress.text = list[position].drop_info[0].address
        }
        viewDataBinding.root.btnAccept.tag = position
        viewDataBinding.root.cvInProgress.tag = position
        viewDataBinding.root.btnReject.tag = position
        viewDataBinding.root.btnAccept.setOnClickListener(onClickListener)
        viewDataBinding.root.btnReject.setOnClickListener(onClickListener)
        viewDataBinding.root.cvInProgress.setOnClickListener(onClickListener)
    }
    override fun getItemCount(): Int = list.size
}