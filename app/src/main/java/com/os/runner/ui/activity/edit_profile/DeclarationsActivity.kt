package com.os.runner.ui.activity.edit_profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityDeclarationsBinding
import com.os.runner.model.Data
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import java.io.File

class DeclarationsActivity : BaseBindingActivity(), View.OnClickListener {


    var viewModel: EditProfileViewModel? = null
    private var binding: ActivityDeclarationsBinding? = null
    private var userPojo: Data? = null
    private var profileFile: File? = null
    private var nricFrontImageFile: File? = null
    private var nricBackImageFile: File? = null

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_declarations)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, DeclarationsActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent, 222)
        }
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }


    private fun initControl() {
        if (intent.extras != null) {
            userPojo = intent.getBundleExtra("bundle")!!.getSerializable("data") as Data
        }
        binding!!.toolbarLayout.imgBack.visibility = View.VISIBLE
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.btnSave.setOnClickListener(this)
        binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.declarations)
        setData()
    }

    private fun setData() {
        if(userPojo!!.declaration!= null){
            binding!!.cbType1.isChecked=userPojo!!.declaration.type_1
            binding!!.cbType2.isChecked=userPojo!!.declaration.type_2
            binding!!.cbType3.isChecked=userPojo!!.declaration.type_3
        }
    }

    /*{"type_1": false,"type_2": true,"type_3": false}*/
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.btnSave -> {
                if (binding!!.cbType1.isChecked && binding!!.cbType2.isChecked && binding!!.cbType3.isChecked) {
                    val reqData: HashMap<String, Any> = HashMap()
                    reqData["user_id"] = App.sessionManager!!.getUserID()
                    reqData["first_name"] = ""
                    reqData["last_name"] = ""
                    reqData["email"] =""
                    reqData["country_code"] = ""
                    reqData["mobile"] = ""
                    reqData["address"] = ""
                    reqData["bank_name"] = ""
                    reqData["account_number"] = ""
                    reqData["account_holder_name"] = ""
                    reqData["nric_number"] = ""
                    reqData["declaration"] = "{\"type_1\": true,\"type_2\": true,\"type_3\": true}"
                    reqData["branch_code"] = ""
                    reqData["bank_code"] = ""
                    reqData["postal_code"] = ""
                    viewModel!!.runner_edit_profile(
                        reqData,
                        profileFile,
                        nricFrontImageFile,
                        nricBackImageFile
                    )
                } else {
                    UiHelper.showErrorToastMsgShort(
                        mActivity!!,
                        "Please select all declarations to save."
                    )
                }
            }
        }
    }

    private fun handleResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    App.sessionManager!!.setUserData(response.data)
                    setResult(111)
                    finish()
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()

                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgLong(mActivity!!, response.message)
                }
            }
        }
    }


}