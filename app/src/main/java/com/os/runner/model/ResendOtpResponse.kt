package com.os.runner.model

data class ResendOtpResponse(
    val response: Response
)

data class Response(
    val `data`: DataResendOtp,
    val message: String,
    val status: Int
)

data class DataResendOtp(
    val country_code: String,
    val mobile: String,
    val otp: String
)