package com.os.runner.ui.activity.change_password

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityChangePasswordBinding
import com.os.runner.model.DefaultPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.custom_toolbar.view.*

class ChangePasswordActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: ChangePasswordViewModel? = null
    private var binding: ActivityChangePasswordBinding? = null

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
    }

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(ChangePasswordViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControls()
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }

    private fun initControls() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.change_password))
        binding!!.toolbarLayout.txtEdit.setOnClickListener(this)
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.btnSubmit.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.btnSubmit -> {
                if(isValidFormData(binding!!.edtOldPassword.text.toString().trim(),binding!!.edtNewPassword.text.toString().trim(),binding!!.edtConfirmPassword.text.toString().trim())){
                    var jsonObject = JsonObject()
                    jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
                    jsonObject.addProperty("old_password", binding!!.edtOldPassword.text.toString().trim())
                    jsonObject.addProperty(Constants.PASSWORD, binding!!.edtNewPassword.text.toString().trim())
                    viewModel!!.changePassword(jsonObject)
                }
            }
        }
    }

    private fun isValidFormData(
        old_password: String,
        new_password: String,
        confirm_password: String
    ): Boolean {


        if (TextUtils.isEmpty(old_password)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter old password")
            return false
        }

        if (TextUtils.isEmpty(new_password)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter new password")
            return false
        }

        if (new_password.length < 6 || new_password.length > 30) {
            UiHelper.showErrorToastMsgShort(
                mActivity!!,
                getString(R.string.err_msg_new_password_valid)
            )
            return false
        }
        if (!UtilityMethod.isValidPassword(new_password)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.password_validation))
            return false
        }

        if (TextUtils.isEmpty(confirm_password)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter confirm password")
            return false
        }
        if (!new_password.equals(confirm_password)) {
            UiHelper.showErrorToastMsgShort(mActivity!!,getString(R.string.password_not_match))
            return false
        }
        return true
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, ChangePasswordActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    private fun handleResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!,getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!,response.message)
                    finish()
                } else if(response.status == -1){
                    App.sessionManager!!.setLogout()

                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                }else {
                    UiHelper.showErrorToastMsgShort(mActivity!!,response.message)
                }
            }
        }
    }

}
