package com.os.runner.utility;

public class ChatConstants {
    //Main Child Name
    public static String RECENT = "Recent";
    public static String USERS = "Users";
    public static String MESSAGES = "Message";
    public static String LOCATION = "Location";
    public static String RUNNERS = "Runners";



    public static String CUSTOMER_ID = "customer_id";
    public static String IS_READ = "isRead";
    public static String KEY_ID = "keyID";
    public static String Message = "message";
    public static String MESSAGE_TYPE = "message_type";
    public static String RUNNER_ID = "runner_id";
    public static String TIMESTAMP = "timestamp";
    public static String SENDER_ID = "sender_id";
    public static String RECEIVER_ID = "receiver_id";


    /*Sub Child Name*/
    /*RECENT && MESSAGE SUB CHILD*/
    public static String IS_ONINE = "isOnline";   /*Extra field in Recent*/
    public static String FROM = "from";
    public static String IMAGE_ID = "image_id";
    /*Users SubChild*/
    public static String USER_ID = "user_id";
    public static String DEVICE_ID = "device_id";
    public static String  FIRST_NAME = "first_name";
    public static String  LAST_NAME = "last_name";
    public static String USER_IMAGE = "user_image";
    public static String EMAIL_ID = "email_id";
    public static String MOBILE_NO = "mobile_no";
    public static String COUNTRY_CODE = "country_code";
    public static String USER_TYPE = "user_type";
    /*Location SubChild*/
    public static String LATITUDE = "latitude";
    public static String  LONGITUDE = "longitude";

}
