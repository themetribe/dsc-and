package com.os.runner.ui.activity.bank_name

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.dstarrunner.R
import com.app.dstarrunner.databinding.ActivityBankNameBinding
import com.os.runner.model.CancelData
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.adapter.BankNameAdapter
import kotlinx.android.synthetic.main.custom_toolbar.view.*

class BankNameListActivity : BaseBindingActivity(), View.OnClickListener{

    var viewModel: BankNameViewModel? = null
    private var binding: ActivityBankNameBinding? = null
    private var bankNameAdapter: BankNameAdapter? = null
    private val bankNamelist: ArrayList<CancelData> = ArrayList()
    private var onClickListener: View.OnClickListener? = null
    private var bank_name =""
    private val tempBankNamelist: Array<String> = arrayOf(
        "AUSTRALIA & NZ BANKING GRP LTD",
        "BANGKOK BANK PUBLIC CO LTD",
        "BANK OF AMERICA, NA",
        "BANK OF CHINA",
        "BANK OF INDIA",
        "BNP PARIBAS",
        "CIMB BANK BERHAD",
        "CITIBANK NA",
        "CITIBANK SINGAPORE LIMITED",
        "COMMERZBANK AKTIENGESELLCHAFT",
        "CREDIT AGRICOLE CORPORATE AND INVESTMENT BANK",
        "CTBC BANK CO. LTD",
        "DBS/POSB",
        "DEUTSCHE BANK AG",
        "FIRST COMMERCIAL BANK, LTD",
        "HL BANK",
        "HSBC (CORPORATE)",
        "HSBC (PERSONAL)",
        "ICICI BANK LIMITED",
        "INDIAN BANK",
        "INDIAN OVERSEAS BANK",
        "INDUSTRIAL AND COMMERCIAL BANK OF CHINA",
        "INTESA SANPAOLO S.P.A",
        "JPMORGAN CHASE BANK",
        "KOREA EXCHANGE BANK",
        "MALAYAN BANKING BERHAD",
        "MAYBANK SINGAPORE LIMITED",
        "MIZUHO BANK LTD",
        "NATIONAL AUSTRALIA BANK LTD",
        "NORDEA BANK AB SINGAPORE BRANCH",
        "OVERSEA-CHINESE BANKING CORPORATION LTD",
        "PT BANK NEGARA INDONESIA (PERSERO) TBK",
        "QATAR NATIONAL BANK SAQ",
        "RHB BANK BERHAD",
        "SING INVESTMENTS & FINANCE LIMITED",
        "SINGAPURA FINANCE LIMITED",
        "SKANDINAVISKA ENSKILDA BANKEN AB (PUBL)",
        "SOCIETE GENERALE",
        "STANDARD CHARTERED BANK (SINGAPORE) LIMITED",
        "STATE BANK OF INDIA",
        "SUMITOMO MITSUI BANKING CORPORATION ",
        "THE BANK OF EAST ASIA LTD",
        "THE BANK OF TOKYO – MITSUBISHI UFJ LTD",
        "UBS AG",
        "UCO BANK",
        "UNICREDIT BANK AG",
        "UNITED OVERSEAS BANK LTD")

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(BankNameViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bank_name)
        onClickListener=this
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, BankNameListActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent,109)
        }
    }

    override fun setListeners() {
    }

    private fun initControl() {
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.bank_name)

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding!!.recycleView.setLayoutManager(mLayoutManager)

        for (item in  0  until tempBankNamelist.size) {
            val cancelData = CancelData()
            cancelData.name= tempBankNamelist[item]
            bankNamelist!!.add(cancelData)
        }

        bankNameAdapter = BankNameAdapter(this, bankNamelist!!, onClickListener)
        binding!!.recycleView.adapter = bankNameAdapter

    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.rlMainView -> {
                val position = v.tag as Int
                bank_name = bankNamelist!![position].name
                val intent = Intent()
                intent.putExtra("bank_name", bank_name!!)
                setResult(201, intent)
                finish()
            }
        }
    }

}