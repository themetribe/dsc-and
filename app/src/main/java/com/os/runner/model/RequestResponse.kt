package com.os.runner.model

import java.io.Serializable

data class RequestResponse(
    val `data`: List<DataRequest>,
    val message: String,
    val status: Int
)

data class DataRequest(
    val accept_time: String,
    val category_name: String,
    val completion_time: String,
    val created_at: String,
    val distance: String,
    val drop_info: List<DropInfo>,
    val is_rating: String,
    val item_detail: String,
    val pickup_info: PickupInfo,
    val rating: String,
    val request_id: String,
    val request_type: String,
    val runner_detail: RunnerDetail,
    val schedule_date_time: String,
    val service_name: String,
    val start_time: String,
    val status: String,
    val transport_category: String,
    val user_image: String,
    val user_id: String,
    val user_mobile: String,
    val user_name: String,
    val tip: String,
    val amount: String,
    val weight: String
):Serializable

data class DropInfo(
    val address: String,
    val contact_number: String,
    val landmark: String,
    val latitude: String,
    val longitude: String,
    val name: String,
    val status: String
)

data class PickupInfo(
    val address: String,
    val contact_number: String,
    val landmark: String,
    val latitude: String,
    val longitude: String,
    val name: String,
    val status: String
):Serializable
class RunnerDetail(
    val runner_id: String,
    val runner_name: String,
    val runner_mobile: String,
    val is_favourite: String,
    val runner_image: String,
    val runner_vehicle: String
):Serializable