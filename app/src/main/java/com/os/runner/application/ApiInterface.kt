package com.os.runner.application

import com.google.gson.JsonObject
import com.os.runner.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


//  fun signIn(@Header("x-auth-token") token: String,@Body jsonObject: JsonObject): Observable<JsonObject>
interface ApiInterface {
    @Headers("Content-Type: application/json")
    @POST("signup")
    fun signup(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("social-signup")
    fun social_signup(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("login")
    fun login(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("logout")
    fun logout(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("verify-otp")
    fun verify_otp(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("resend-otp")
    fun resend_otp(@Body jsonObject: JsonObject): Observable<UserPojo>

    @POST("forgot-password")
    fun forgotPassword(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("requests-for-runner")
    fun requests_for_runner(@Body jsonObject: JsonObject): Observable<RequestResponse>

    @Headers("Content-Type: application/json")
    @POST("accept-request")
    fun accept_request(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("decline-request")
    fun decline_request(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("notification-list")
    fun notification_list(@Body jsonObject: JsonObject): Observable<NotifiationResponse>

    @Headers("Content-Type: application/json")
    @POST("notification-read-unread")
    fun notification_read_unread(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("notification-delete")
    fun notification_delete(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("availability-status")
    fun availability_status(@Body jsonObject: JsonObject): Observable<AvailabilityStatusResponse>

    @Headers("Content-Type: application/json")
    @POST("home-data-count")
    fun home_data_count(@Body jsonObject: JsonObject): Observable<HomeCountDataResponse>

    @Headers("Content-Type: application/json")
    @POST("runner-profile")
    fun profile_api(@Body jsonObject: JsonObject): Observable<UserPojo>

   /* @Headers("Content-Type: application/json")
    @POST("edit-profile")
    fun edit_profile(@Body jsonObject: JsonObject): Observable<UserPojo>*/


    @Headers("Content-Type: application/json")
    @POST("change-password")
    fun changePassword(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("top-ten-runners")
    fun top_ten_runners(@Body jsonObject: JsonObject): Observable<TopTenRunnerResponse>

    @Headers("Content-Type: application/json")
    @POST("my-earning")
    fun my_earning(@Body jsonObject: JsonObject): Observable<MyEarningResponse>

    @Headers("Content-Type: application/json")
    @POST("payout")
    fun payout(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("transactions-history")
    fun transactions_history(@Body jsonObject: JsonObject): Observable<TransactionsResponse>

    @Headers("Content-Type: application/json")
    @POST("runner-request-detail")
    fun runner_request_detail(@Body jsonObject: JsonObject): Observable<RequestDetailResponse>

    @Multipart
    @POST("update-images")
    fun update_images(
        @Part customer_image: MultipartBody.Part?
    ): Observable<PhotosResponse>

    /*{
        "user_id": "503",
        "first_name": "Ram",
        "last_name": "Kumar",
        "email": "1993nareshj@gmail.com",
        "country_code": "+91",
        "mobile": "31313153",
        "address": "",
        "bank_name":"",
        "account_number":"000123456"
        "account_holder_name":""
        "nric_number":"",
        "nric_front_image":""
        "nric_back_image":"","transport_category_id":""
        "declaration": {"type_1": false,"type_2": true,"type_3": false} ,
        "branch_code":"1100"
        "bank_code":"000"
        "postal_code":"218745"
    }*/

    @Multipart
    @POST("runner-edit-profile")
    fun runner_edit_profile(
        @Part("user_id") user_id: RequestBody
        , @Part("first_name") first_name: RequestBody
        , @Part("last_name") last_name: RequestBody
        , @Part("email") email: RequestBody
        , @Part("country_code") country_code: RequestBody
        , @Part("mobile") mobile: RequestBody
        , @Part("address") address: RequestBody
        , @Part("bank_name") bank_name: RequestBody
        , @Part("transport_category_id") transport_category_id: RequestBody
        , @Part("account_number") account_number: RequestBody
        , @Part("account_holder_name") account_holder_name: RequestBody
        , @Part("nric_number") nric_number: RequestBody
        , @Part("declaration") declaration: RequestBody
        , @Part("branch_code") branch_code: RequestBody
        , @Part("bank_code") bank_code: RequestBody
        , @Part("postal_code") postal_code: RequestBody
        , @Part image: MultipartBody.Part?
        , @Part nric_front_image: MultipartBody.Part?
        , @Part nric_back_image: MultipartBody.Part?
    ): Observable<UserPojo>


    @Multipart
    @POST("upload-image")
    fun upload_image(
        @Part image: MultipartBody.Part?
        , @Part("request_id") request_id: RequestBody
    ): Observable<UploadImageResponse>

    @Headers("Content-Type: application/json")
    @POST("sent-msg-notif")
    fun sent_msg_notif(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("start-job")
    fun start_job(@Body jsonObject: JsonObject): Observable<DefaultPojo>


    @Headers("Content-Type: application/json")
    @POST("action-start-job")
    fun action_start_job(@Body jsonObject: HashMap<String, Any>): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("twilio-token")
    fun twilio_token(@Body jsonObject: JsonObject): Observable<CallResponse>

    @Headers("Content-Type: application/json")
    @POST("runner-cancel-request")
    fun runner_cancel_request(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("addrating")
    fun add_rating(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @POST("my-rating")
    fun my_rating(@Body jsonObject: JsonObject): Observable<RatingListResponse>


    @Headers("Content-Type: application/json")
    @POST("update-location")
    fun update_location(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    /*Other Api */
    @POST("users/update_devicetoken")
    fun updateDeviceToken(@Body jsonObject: JsonObject): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @GET("getuser")
    fun getProfile(): Observable<UserPojo>

    @Multipart
    @POST("register")
    fun singup(
        @Part("name") full_name: RequestBody
        , @Part("snapchat_id") snapchat_id: RequestBody
        , @Part("mobile") phone_number: RequestBody
        , @Part("age") age: RequestBody
        , @Part("gender") gender: RequestBody
        , @Part("want_to_meet") want_to_meet: RequestBody
        , @Part("password") password: RequestBody
        , @Part("latitude") latitute: RequestBody
        , @Part("longitude") longiture: RequestBody
        , @Part("device_id") device_id: RequestBody
        , @Part("device_type") device_type: RequestBody
        , @Part customer_image: MultipartBody.Part?
    ): Observable<UserPojo>



    /*@Headers("Content-Type: application/json")
    @GET("nearest-users-list")
    fun get_near_by_users_list(): Observable<UsersListResponse>*/

    @Headers("Content-Type: application/json")
    @GET("users-list")
    fun get_near_by_users_list(): Observable<UsersListResponse>

    @Headers("Content-Type: application/json")
    @GET("getgems")
    fun getgems(): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @GET("settings")
    fun settings(): Observable<SettingsDataResponse>

    @Headers("Content-Type: application/json")
    @POST("send-request")
    fun request_send(@Body jsonObject: JsonObject): Observable<DefaultPojo>


    @Headers("Content-Type: application/json")
    @POST("update-dob")
    fun update_dob(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("update-gender")
    fun update_gender(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("update-search-preference")
    fun update_search_preference(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("update-gems")
    fun update_gems(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("update-bio")
    fun update_bio(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("update-country")
    fun update_country(@Body jsonObject: JsonObject): Observable<UserPojo>

    @Headers("Content-Type: application/json")
    @POST("delete-image")
    fun delete_image(@Body jsonObject: JsonObject): Observable<DefaultPojo>


    @Headers("Content-Type: application/json")
    @GET("list-images")
    fun getPhotos(): Observable<PhotosResponse>


    @Headers("Content-Type: application/json")
    @GET("logout")
    fun logout(): Observable<DefaultPojo>

    @Headers("Content-Type: application/json")
    @GET("account-delete")
    fun account_delete(): Observable<DefaultPojo>



    @Headers("Content-Type: application/json")
    @GET("list-request-for-me")
    fun request_list_notifications(): Observable<SnapRequestListResponse>

    @Headers("Content-Type: application/json")
    @POST("action-request")
    fun action_request(@Body jsonObject: HashMap<String, Any>): Observable<DefaultPojo>


    @Headers("Content-Type: application/json")
    @POST("unblock-friend")
    fun unblock_friend(@Body jsonObject: JsonObject): Observable<DefaultPojo>


    @Headers("Content-Type: application/json")
    @GET("block-friends")
    fun block_friends(): Observable<BlockedUserResponse>


    @Headers("Content-Type: application/json")
    @POST("login")
    fun login_api(@Body jsonObject: JsonObject): Observable<UserPojo>


    @Headers("Content-Type: application/json")
    @POST("static-page")
    fun static_pages(@Body jsonObject: JsonObject): Observable<StaticPagesResponse>

}
