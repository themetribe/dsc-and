package com.os.runner.ui.adapter

import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.app.dstarrunner.R
import com.os.runner.model.DataRatingList
import kotlinx.android.synthetic.main.row_earning.view.imvCustomerProfile
import kotlinx.android.synthetic.main.row_rating_list.view.*

class RatingListAdapter(
    private val context: Activity?,
    private val list: ArrayList<DataRatingList>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_rating_list

    override fun getViewModel(position: Int): Any? = list[position]

    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        Glide.with(context!!).load(list[position].image).placeholder(R.drawable.default_user_pic).into(viewDataBinding.root.imvCustomerProfile)
        viewDataBinding.root.txtUserName.text = list[position].username
        viewDataBinding.root.txtBookingID.text = "Booking ID: "+list[position].request_id
        if(list[position].review.isNullOrBlank()){
            viewDataBinding.root.txtComment.text = "No comment"
        }else{
            viewDataBinding.root.txtComment.text = list[position].review
        }

        viewDataBinding.root.ratingBar.rating = list[position].rating.toFloat()

    }

    override fun getItemCount(): Int = list.size
}