package com.os.runner.model

import java.io.Serializable
data class UserPojo(
    val `data`: Data,
    val message: String,
    val status: Int
):Serializable

data class Data(
    val address: String,
    val auth_token: String,
    val bank_info: BankInfo,
    val country_code: String,
    val declaration: Declaration,
    val device_id: String,
    val device_type: String,
    val email: String,
    val first_name: String,
    val is_approved: String,
    val is_email_verify: String,
    val is_mobile_verify: String,
    val is_notification: String,
    val last_name: String,
    val latitude: String,
    val login_type: String,
    val longitude: String,
    val mobile: String,
    val complete_trip: String,
    val notification_count: String,
    val rating: String,
    val cancelled_trip: String,
    val nric_info: NricInfo,
    val otp: String,
    val profile_pic: String,
    val ranking: String,
    val referral_code: String,
    val reward_points: String,
    val transport_category: String,
    val user_id: String,
    val view_status: String,
    val wallet_amount: String,
    val completed_trip: String
):Serializable

data class BankInfo(
    val account_holder_name: String,
    val account_number: String,
    val bank_code: String,
    val bank_name: String,
    val branch_code: String,
    val postal_code: String
):Serializable

data class Declaration(
    val type_1: Boolean,
    val type_2: Boolean,
    val type_3: Boolean
):Serializable

data class NricInfo(
    val nric_back_image: String,
    val nric_front_image: String,
    val nric_number: String
):Serializable