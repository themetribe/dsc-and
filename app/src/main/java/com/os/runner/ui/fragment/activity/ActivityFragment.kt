package com.os.runner.ui.fragment.activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.os.drunnercustomer.ui.fragment.completed.CompletedFragment
import com.app.dstarrunner.R
import com.app.dstarrunner.databinding.FragmentActivityBinding
import com.os.runner.ui.fragment.BaseFragment
import com.os.runner.ui.fragment.upcoming.UpcomingFragment

class ActivityFragment : BaseFragment(),View.OnClickListener{

    private var binding: FragmentActivityBinding? = null
    var viewModel: ActivityViewModel? = null
    private var mFragmentManager: FragmentManager? = null
    private var mFragmentTransaction: FragmentTransaction? = null

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        viewModel = ViewModelProvider(this).get(ActivityViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_activity, container, false)
        binding!!.lifecycleOwner = this
        return binding!!
    }

    override fun createActivityObject() {
        mActivity = activity
    }

    override fun initializeObject() {
        initControl()
    }

    private fun initControl() {
        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */
        mFragmentManager = childFragmentManager
        mFragmentTransaction = mFragmentManager!!.beginTransaction()
        mFragmentTransaction!!.replace(R.id.containerView, UpcomingFragment()).commit()

        binding!!.llUpcoming.setOnClickListener(this)
        binding!!.llCompleted.setOnClickListener(this)

    }

    override fun setListeners() {

       /* viewModel!!.responseLiveData.observe(this, androidx.lifecycle.Observer {
            handleResult(it)
        })*/
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.llUpcoming->{
                manageTabs(0,true,false)
            }
            R.id.llCompleted->{
                manageTabs(1,false,true)
            }
        }
    }

    private fun manageTabs(
        position: Int,
        upcoming: Boolean,
        completed: Boolean
    ) {
        try {
            when (position) {
                0 -> {
                    mFragmentManager = childFragmentManager
                    mFragmentTransaction = mFragmentManager!!.beginTransaction()
                    mFragmentTransaction!!.replace(R.id.containerView, UpcomingFragment()).commit()
                }
                1 -> {
                    mFragmentManager = childFragmentManager
                    mFragmentTransaction = mFragmentManager!!.beginTransaction()
                    mFragmentTransaction!!.replace(R.id.containerView, CompletedFragment()).commit()
                }
            }
            if (upcoming) {
                binding!!.llUpcoming!!.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_default_button))
                binding!!.txtUpcoming!!.setTextColor(resources.getColor(R.color.black))
            } else {
                binding!!.llUpcoming!!.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_grey_button))
                binding!!.txtUpcoming!!.setTextColor(resources.getColor(R.color.black))
            }
            if (completed) {
                binding!!.llCompleted!!.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_default_button))
                binding!!.txtCompleted!!.setTextColor(resources.getColor(R.color.black))
            } else {
                binding!!.llCompleted!!.setBackgroundDrawable(resources.getDrawable(R.drawable.bg_grey_button))
                binding!!.txtCompleted!!.setTextColor(resources.getColor(R.color.black))
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

}