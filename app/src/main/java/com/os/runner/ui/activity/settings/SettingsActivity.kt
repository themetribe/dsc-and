package com.os.runner.ui.activity.settings

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivitySettingsBinding
import com.os.runner.model.DefaultPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.activity.static_page.StatisContentPagesActivity
import com.os.runner.ui.dialogs.AlertMessageDialog
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*

class SettingsActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: SettingsViewModel? = null
    private var binding: ActivitySettingsBinding? = null

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
    }

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(SettingsViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControls()
    }

    override fun setListeners() {
        viewModel!!.responseLogoutData.observe(this, Observer {
            handleResult(it)
        })
    }

    private fun initControls() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.settings))
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.cardViewTermsOfService.setOnClickListener(this)
        binding!!.cardViewLogout.setOnClickListener(this)
        binding!!.cardViewTermsOfService.setOnClickListener(this)
        binding!!.txtUpdate.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.txtUpdate -> {
                val appPackageName = packageName // getPackageName() from Context or Activity object
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                } catch (anfe: ActivityNotFoundException) {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                }
            }
            R.id.cardViewTermsOfService->{
                val bundle = Bundle()
                bundle.putString("slug", "terms_condition")
                StatisContentPagesActivity.startActivity(this, bundle)
            }
            R.id.cardViewLogout -> {
                AlertMessageDialog.showDialog(this,
                    getString(R.string.logout_alert),
                    getString(R.string.do_you_really_want_to_logout),
                    getString(R.string.logout_alert),
                    getString(R.string.cancel_alert),
                    object : AlertMessageDialog.AlertMessageCallback {
                        override fun onSuccessClick() {
                            var jsonObject = JsonObject()
                            jsonObject.addProperty("user_id", App.sessionManager!!.getUserID())
                            viewModel!!.logout(jsonObject)
                        }

                        override fun onCancelClick() {

                        }
                    })
            }
        }
    }


    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, SettingsActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    private fun handleResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + result.data!!)
                    UiHelper.showSucessToastMsgShort(mActivity, response.message)
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(this, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

}
