package com.os.runner.model

data class TransactionsResponse(
    val `data`: List<DataTransactions>,
    val message: String,
    val status: Int
)

data class DataTransactions(
    val amount: String,
    val status: String,
    val transaction_date: String,
    val transaction_id: String
)