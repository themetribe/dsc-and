package com.os.runner.custom

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

class MediumFontEditText : AppCompatEditText {
    //String font;
    internal var font_type: Typeface? = null
    internal var context: Context


    constructor(context: Context, fontName: String) : super(context) {
        this.context = context
        // this.font = fontName;
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.context = context
        init()
        // TODO Auto-generated constructor stub
    }

    constructor(context: Context) : super(context) {
        this.context = context
        init()
        // TODO Auto-generated constructor stub
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        this.context = context
        init()
        // TODO Auto-generated constructor stub
    }

    private fun init() {
        try {
            val tf = Typeface.createFromAsset(resources.assets, "VARELAROUND-REGULAR.TTF")

            typeface = tf
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}