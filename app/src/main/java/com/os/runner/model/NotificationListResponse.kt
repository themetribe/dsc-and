package com.os.runner.model

import java.io.Serializable

data class NotificationListResponse(
    val response: ResponseNotificationList
)

data class ResponseNotificationList(
    val `data`: DataNotificationList,
    val message: String,
    val status: Int
)

data class DataNotificationList(
    val notif_data: List<NotificationList>,
    val notification_count: Int,
    val unread_notification_count: Int
)

data class NotificationList(
    val created_at: String="",
    val id: Int=0,
    val is_read: Int=0,
    val noti_type: String="",
    val notif: String="",
    val user_id: String=""
):Serializable