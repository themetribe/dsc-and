package com.os.runner.ui.activity.static_page

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.StaticPagesResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class StaticContentPagesViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<StaticPagesResponse>>()
    var apiResponse: ApiResponse<StaticPagesResponse>? = null
    private var subscription: Disposable? = null

    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun static_pages(jsonObject: JsonObject) {
        subscription = App.apiService!!.static_pages(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}