package com.os.runner.model

data class RatingListResponse(
    val `data`: List<DataRatingList>,
    val message: String,
    val status: Int
)

data class DataRatingList(
    val created_at: String,
    val given_by: String,
    val id: String,
    val image: String,
    val rating: String,
    val request_id: String,
    val review: String,
    val username: String
)