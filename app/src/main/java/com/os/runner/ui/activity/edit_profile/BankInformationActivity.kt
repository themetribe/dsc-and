package com.os.runner.ui.activity.edit_profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityBankInformationBinding
import com.os.runner.model.Data
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.bank_name.BankNameListActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.activity_bank_information.*
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import java.io.File

class BankInformationActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: EditProfileViewModel? = null
    private var binding: ActivityBankInformationBinding? = null
    private var userPojo: Data? = null
    private var profileFile: File? = null
    private var nricFrontImageFile: File? = null
    private var nricBackImageFile: File? = null

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bank_information)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        if (intent.extras != null) {
            userPojo = intent.getBundleExtra("bundle")!!.getSerializable("data") as Data
        }
        initControls()
        setData()
    }

    private fun setData() {
        binding!!.edtSelectBankName.setText(userPojo!!.bank_info.bank_name)
        binding!!.edtAccountNumber.setText(userPojo!!.bank_info.account_number)
        binding!!.edtAccountHolderName.setText(userPojo!!.bank_info.account_holder_name)
        binding!!.edtBankCode.setText(userPojo!!.bank_info.bank_code)
        binding!!.edtBranchCode.setText(userPojo!!.bank_info.branch_code)
        binding!!.edtPostalCode.setText(userPojo!!.bank_info.postal_code)
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }

    private fun initControls() {
        binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.add_back_detail)
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.edtSelectBankName.setOnClickListener(this)
        binding!!.btnProceed.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.edtSelectBankName -> {
                BankNameListActivity.startActivity(mActivity!!, null)
            }
            R.id.btnProceed -> {
                var bankname = edtSelectBankName.text.toString().trim()
                var account_number = edtAccountNumber.text.toString().trim()
                var holder_name = edtAccountHolderName.text.toString().trim()
                var bank_code = edtBankCode.text.toString().trim()
                var branch_code = edtBranchCode.text.toString().trim()
                var postal_code = edtPostalCode.text.toString().trim()

                if (isValidFormData(
                        bankname,
                        account_number,
                        holder_name,
                        bank_code,
                        branch_code,
                        postal_code
                    )
                ) {
                    val reqData: HashMap<String, Any> = HashMap()
                    reqData["user_id"] = App.sessionManager!!.getUserID()
                    reqData["first_name"] = ""
                    reqData["last_name"] = ""
                    reqData["email"] = ""
                    reqData["country_code"] = ""
                    reqData["mobile"] = ""
                    reqData["address"] = ""
                    reqData["bank_name"] = bankname
                    reqData["account_number"] = account_number
                    reqData["account_holder_name"] = holder_name
                    reqData["nric_number"] = ""
                    reqData["declaration"] = ""
                    reqData["branch_code"] = branch_code
                    reqData["bank_code"] = bank_code
                    reqData["postal_code"] = postal_code
                    viewModel!!.runner_edit_profile(
                        reqData,
                        profileFile,
                        nricFrontImageFile,
                        nricBackImageFile
                    )
                }
            }
        }
    }

    private fun isValidFormData(
        bankname: String,
        account_number: String,
        holder_name: String,
        bank_code: String,
        branch_code: String,
        postal_code: String
    ): Boolean {

        if (TextUtils.isEmpty(bankname)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please select bank name")
            return false
        }
        if (TextUtils.isEmpty(account_number)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter account number")
            return false
        }
        if (TextUtils.isEmpty(holder_name)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter account holder name")
            return false
        }
        if (TextUtils.isEmpty(bank_code)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter bank code")
            return false
        }
        if (TextUtils.isEmpty(branch_code)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter branch code")
            return false
        }
        if (TextUtils.isEmpty(postal_code)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter postal code")
            return false
        }
        return true
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, BankInformationActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent, 111)
        }
    }

    private fun handleResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    App.sessionManager!!.setUserData(response.data)
                    setResult(111)
                    finish()
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()

                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgLong(mActivity!!, response.message)
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 201) {
            var bank_name = data!!.getStringExtra("bank_name")
            binding!!.edtSelectBankName.setText(bank_name)
        }
    }
}
