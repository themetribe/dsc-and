package com.os.runner.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import com.app.dstarrunner.R;
import com.os.runner.activity.BaseActivity;


public class ProgressBarDialog extends Dialog {
    boolean outSideCancelable;
    boolean cancelable;
   // AVLoadingIndicatorView loadingIndicatorView;
    public ProgressBarDialog(BaseActivity context) {
        super(context);
    }
    public ProgressBarDialog(Context context, boolean cancelable, boolean outSideCancelable) {
        super(context);
        this.outSideCancelable=outSideCancelable;
        this.cancelable=cancelable;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.progress_layout);
       // loadingIndicatorView=findViewById(R.id.avLoading);
        closeOptionsMenu();
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
   /* public void smoothShow(){
        //show();
        loadingIndicatorView.smoothToShow();
    }
    public void smoothHide(){
        loadingIndicatorView.smoothToHide();
        //dismiss();
    }*/
}

