package com.os.runner.model

import java.io.Serializable

data class MyEarningResponse(
    val `data`: List<DataMyEarning>,
    val message: String,
    val status: Int
):Serializable

data class DataMyEarning(
    val earning_data: List<EarningData>,
    val month: String,
    val total_amount: String
):Serializable

data class EarningData(
    val created_at: String,
    val drop_info: List<DropInfoEarning>,
    val earning: String,
    val pickup_info: PickupInfoEarning,
    val schedule_date_time: String,
    val service_name: String,
    val tip: String,
    val user_image: String,
    val user_name: String
):Serializable

data class DropInfoEarning(
    val address: String,
    val contact_number: String,
    val landmark: String,
    val latitude: String,
    val longitude: String,
    val name: String,
    val status: String
):Serializable

data class PickupInfoEarning(
    val address: String,
    val contact_number: String,
    val landmark: String,
    val latitude: String,
    val longitude: String,
    val name: String,
    val status: String
):Serializable