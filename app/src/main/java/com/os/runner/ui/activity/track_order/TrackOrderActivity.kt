package com.os.runner.ui.activity.track_order

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.app.dstarrunner.R
import com.app.dstarrunner.databinding.ActivityTrackOrderBinding
import com.os.runner.map.DrawMarker
import com.os.runner.map.DrawRouteMaps
import com.os.runner.model.RequestDetailResponse
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.utility.LocationProvider

@Suppress("DEPRECATION")
class TrackOrderActivity : BaseBindingActivity(), OnMapReadyCallback,
    LocationListener,View.OnClickListener {

    private var mGoogleMap: GoogleMap? = null
    private var locationProvider: LocationProvider? = null
    private var coordinate: LatLng? = null
    private val errorCallback = LocationProvider.ErrorCallback { }
    private var mLastClickTime: Long = 0
    var binding: ActivityTrackOrderBinding? = null
    var requestDetailResponse : RequestDetailResponse?=null

    companion object {

        fun startActivity(activity: Activity, bundle: Bundle?, isClear: Boolean) {
            val intent = Intent(activity, TrackOrderActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            if (isClear) intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.startActivityForResult(intent, 15)
        }
    }

    override fun createContentView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_track_order)
        binding!!.lifecycleOwner = this
    }


    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        requestDetailResponse = intent.getBundleExtra("bundle")!!.getSerializable("data") as RequestDetailResponse
        initilizeMap()
        locationProvider = LocationProvider(mActivity!!, this, errorCallback)
    }

    private fun initilizeMap() {

        try {
            val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
            mapFragment?.getMapAsync(this)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    override fun setListeners() {
        binding!!.imgBack.setOnClickListener(this)
        if(requestDetailResponse!!.data.runner_detail.runner_id!=null){

        }
        /*mDatabase!!.child(ChatConstants.RUNNERS).child(requestDetailResponse!!.data.runner_detail.runner_id)
            .addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.hasChild(ChatConstants.LATITUDE) && dataSnapshot.hasChild(ChatConstants.LONGITUDE)) {
                        val latitude =
                            dataSnapshot.child(ChatConstants.LATITUDE)
                                .getValue(String::class.java)!!
                        val longitude =
                            dataSnapshot.child(ChatConstants.LONGITUDE)
                                .getValue(String::class.java)!!
                        val coordinate = LatLng(latitude.toDouble(), longitude.toDouble())

                        if (mGoogleMap != null) {
                            *//*pickup Maker list *//*
                            val pickup_coordinate = LatLng(
                                requestDetailResponse!!.data.pickup_info.latitude.toDouble(),
                                requestDetailResponse!!.data.pickup_info.longitude.toDouble()
                            )
                            if(requestDetailResponse!!.data.service_name.equals("Help Me Queue")){
                                DrawRouteMaps.getInstance(mActivity!!).draw(coordinate, pickup_coordinate, mGoogleMap)
                                DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, coordinate, R.drawable.logoruner, requestDetailResponse!!.data.runner_detail.runner_name)
                                DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, pickup_coordinate, R.drawable.pickup,  requestDetailResponse!!.data.pickup_info.address)

                                val bounds = LatLngBounds.Builder().include(coordinate).include(pickup_coordinate).build()
                                val displaySize = Point()
                                windowManager.defaultDisplay.getSize(displaySize)
                                mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 350, 50))
                            }else{

                                *//*runner Maker list *//*
                                mGoogleMap!!.addMarker(
                                    MarkerOptions().position(coordinate).icon(
                                        BitmapDescriptorFactory.fromResource(
                                            R.drawable.logoruner
                                        )
                                    ).title(requestDetailResponse!!.data.runner_detail.runner_name)
                                )

                                var temp_coordinate : LatLng?= null
                                var address = ""

                                temp_coordinate = pickup_coordinate
                                address = requestDetailResponse!!.data.pickup_info.address

                                *//*Dropoff Maker list *//*
                                if(requestDetailResponse!!.data.drop_info.size>0){

                                    for (i in 0 until requestDetailResponse!!.data.drop_info.size) {
                                        val dropinfo_coordinate = LatLng(
                                            requestDetailResponse!!.data.drop_info!![i].latitude.toDouble(),
                                            requestDetailResponse!!.data.drop_info!![i].longitude.toDouble()
                                        )
                                        DrawRouteMaps.getInstance(mActivity!!).draw(temp_coordinate, dropinfo_coordinate, mGoogleMap)
                                        if(temp_coordinate!!.equals(pickup_coordinate)){
                                            DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, temp_coordinate, R.drawable.pickup, address)
                                        }else{
                                            DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, temp_coordinate, R.drawable.drop, address)
                                        }
                                        DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, dropinfo_coordinate, R.drawable.drop,  requestDetailResponse!!.data.drop_info!![i].address)

                                        val bounds = LatLngBounds.Builder().include(temp_coordinate).include(dropinfo_coordinate).build()
                                        val displaySize = Point()
                                        windowManager.defaultDisplay.getSize(displaySize)
                                        mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 350, 50))
                                        temp_coordinate = dropinfo_coordinate
                                        address = requestDetailResponse!!.data.drop_info!![i].address
                                    }
                                }
                            }



                            mGoogleMap!!.moveCamera(
                                CameraUpdateFactory.newLatLng(
                                    coordinate
                                )
                            )
                            // Zoom out to zoom level 10, animating with a duration of 2 seconds.
                            mGoogleMap!!.animateCamera(
                                CameraUpdateFactory.zoomTo(12f),
                                2000,
                                null
                            )
                        }
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })*/
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.imgBack -> {
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
        if (locationProvider != null) {
            locationProvider!!.onResume()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        mGoogleMap = googleMap
        if (ActivityCompat.checkSelfPermission(
                mActivity!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mActivity!!, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        val style = MapStyleOptions.loadRawResourceStyle(mActivity!!, R.raw.style_json)
        mGoogleMap!!.setMapStyle(style)
        mGoogleMap!!.uiSettings.isMyLocationButtonEnabled = true
        mGoogleMap!!.isMyLocationEnabled = true

    }

    override fun onLocationChanged(location: Location?) {

        val latitude = location!!.latitude
        val longitude = location.longitude
        coordinate = LatLng(latitude, longitude)

        if (mGoogleMap != null) {
            /*pickup Maker list */
            val pickup_coordinate = LatLng(
                requestDetailResponse!!.data.pickup_info.latitude.toDouble(),
                requestDetailResponse!!.data.pickup_info.longitude.toDouble()
            )
            if(requestDetailResponse!!.data.service_name.equals("Help Me Queue")){
                DrawRouteMaps.getInstance(mActivity!!).draw(coordinate, pickup_coordinate, mGoogleMap)
                DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, coordinate, R.drawable.logoruner, requestDetailResponse!!.data.runner_detail.runner_name)
                DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, pickup_coordinate, R.drawable.pickup,  requestDetailResponse!!.data.pickup_info.address)

                val bounds = LatLngBounds.Builder().include(coordinate).include(pickup_coordinate).build()
                val displaySize = Point()
                windowManager.defaultDisplay.getSize(displaySize)
                mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 350, 50))
            }else{

                /*runner Maker list */
                mGoogleMap!!.addMarker(
                    MarkerOptions().position(coordinate!!).icon(
                        BitmapDescriptorFactory.fromResource(
                            R.drawable.logoruner
                        )
                    ).title(requestDetailResponse!!.data.runner_detail.runner_name)
                )

                var temp_coordinate : LatLng?= null
                var address = ""

                temp_coordinate = pickup_coordinate
                address = requestDetailResponse!!.data.pickup_info.address

                /*Dropoff Maker list */
                if(requestDetailResponse!!.data.drop_info.size>0){

                    for (i in 0 until requestDetailResponse!!.data.drop_info.size) {
                        val dropinfo_coordinate = LatLng(
                            requestDetailResponse!!.data.drop_info!![i].latitude.toDouble(),
                            requestDetailResponse!!.data.drop_info!![i].longitude.toDouble()
                        )
                        DrawRouteMaps.getInstance(mActivity!!).draw(temp_coordinate, dropinfo_coordinate, mGoogleMap)
                        if(temp_coordinate!!.equals(pickup_coordinate)){
                            DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, temp_coordinate, R.drawable.pickup, address)
                        }else{
                            DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, temp_coordinate, R.drawable.drop, address)
                        }
                        DrawMarker.getInstance(mActivity!!).draw(mGoogleMap, dropinfo_coordinate, R.drawable.drop,  requestDetailResponse!!.data.drop_info!![i].address)

                        val bounds = LatLngBounds.Builder().include(temp_coordinate).include(dropinfo_coordinate).build()
                        val displaySize = Point()
                        windowManager.defaultDisplay.getSize(displaySize)
                        mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 350, 50))
                        temp_coordinate = dropinfo_coordinate
                        address = requestDetailResponse!!.data.drop_info!![i].address
                    }
                }
            }



            mGoogleMap!!.moveCamera(
                CameraUpdateFactory.newLatLng(
                    coordinate
                )
            )
            // Zoom out to zoom level 10, animating with a duration of 2 seconds.
            mGoogleMap!!.animateCamera(
                CameraUpdateFactory.zoomTo(12f),
                2000,
                null
            )
        }

    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }

}