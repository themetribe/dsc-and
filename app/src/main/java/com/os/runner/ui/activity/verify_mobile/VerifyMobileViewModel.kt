package com.os.drunnercustomer.ui.activity.verify_mobile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.ResendOtpResponse
import com.os.runner.model.UserPojo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class VerifyMobileViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<UserPojo>>()
    var responseResendOtpData = MutableLiveData<ApiResponse<UserPojo>>()
    var apiResponse: ApiResponse<UserPojo>? = null
    var apiResendOtpResponse: ApiResponse<UserPojo>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiResendOtpResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun verify_otp(jsonObject: JsonObject) {
        subscription = App.apiService!!.verify_otp(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }


    fun resend_otp(jsonObject: JsonObject) {
        subscription = App.apiService!!.resend_otp(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseResendOtpData.postValue(apiResendOtpResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseResendOtpData.postValue(apiResendOtpResponse!!.success(result))
                },
                { throwable ->
                    responseResendOtpData.postValue(apiResendOtpResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}