package com.os.runner.ui.activity.my_earning

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import com.os.runner.model.MyEarningResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MyEarningViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<MyEarningResponse>>()
    var responseLivePayoutData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiResponse: ApiResponse<MyEarningResponse>? = null
    var apiPayoutResponse: ApiResponse<DefaultPojo>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiPayoutResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun my_earning(jsonObject: JsonObject) {
        subscription = App.apiService!!.my_earning(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }

    fun payout(jsonObject: JsonObject) {
        subscription = App.apiService!!.payout(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLivePayoutData.postValue(apiPayoutResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLivePayoutData.postValue(apiPayoutResponse!!.success(result))
                },
                { throwable ->
                    responseLivePayoutData.postValue(apiPayoutResponse!!.error(throwable))
                }
            )
    }

    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}