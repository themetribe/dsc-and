package com.os.runner.ui.adapter

import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.app.dstarrunner.R
import com.os.runner.model.DataTransactions
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.row_transaction_list.view.*

class TransactionsAdapter(
    private val context: Activity?,
    private val list: ArrayList<DataTransactions>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_transaction_list

    override fun getViewModel(position: Int): Any? = list[position]

    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        viewDataBinding.root.txtTransactionId.text = list[position].transaction_id
        viewDataBinding.root.txtTransactionAmount.text = context!!.getString(R.string.singapur_doller) + " "+list[position].amount
        viewDataBinding.root.txtTransactionCreatedDate.text = UtilityMethod.parseDateFormated(list[position].transaction_date)
        viewDataBinding.root.txtTransactionStatus.text = list[position].status
    }

    override fun getItemCount(): Int = list.size
}