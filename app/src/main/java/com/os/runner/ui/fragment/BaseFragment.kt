package com.os.runner.ui.fragment

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment() {

    protected var mActivity: Activity? = null


    protected abstract fun setBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): ViewDataBinding

    protected abstract fun createActivityObject()

    protected abstract fun initializeObject()

    protected abstract fun setListeners()

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
        if (mActivity == null) throw NullPointerException("must create activty object")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = setBinding(inflater, container)
        val view = binding.root
        createActivityObject()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeObject()
        setListeners()
    }


}
