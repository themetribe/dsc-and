package com.os.runner.utility

class MessageResponse() {
    var isRead:Boolean?=null
    var keyID = ""
    var message = ""
    var sender_id = ""
    var receiver_id = ""
    var message_type = ""
    var timestamp:Long? = null
}