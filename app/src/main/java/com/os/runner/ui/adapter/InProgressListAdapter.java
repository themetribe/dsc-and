package com.os.runner.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dstarrunner.R;
import com.os.runner.utility.CircleImageView;

import java.util.List;


public class InProgressListAdapter extends RecyclerView.Adapter<InProgressListAdapter.MyViewHolder> {
    private Activity activity;
    private Context context;
    private List<String> movieList;
    private View.OnClickListener mOnClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvStoreName, tvProducts, tvActualPrice;
        CardView cvInProgress;
        //public CardView card_view;
        LinearLayout ll_view_single_post;
        RelativeLayout rlclosestoreview;
        CircleImageView ivProductImg;

        public MyViewHolder(View view) {
            super(view);
            cvInProgress = (CardView) view.findViewById(R.id.cvInProgress);
           /* tvStoreName = (TextView) view.findViewById(R.id.tvStoreName);
            tvProducts = (TextView) view.findViewById(R.id.tvProducts);
            //card_view = (CardView) view.findViewById(R.id.card_view);
            ivMark = (ImageView) view.findViewById(R.id.ivMark);
            ivProductImg = (CircleImageView) view.findViewById(R.id.ivProductImg);
            ll_view_single_post = (LinearLayout) view.findViewById(R.id.ll_view_single_post);*/
        }
    }


    public InProgressListAdapter(Activity activity, Context context, List<String> movieList, View.OnClickListener mOnClickListener) {
        this.activity = activity;
        this.context = context;
        this.movieList = movieList;
        this.mOnClickListener = mOnClickListener;
    }

    @Override
    public InProgressListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_inprogress, parent, false);

        return new InProgressListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final InProgressListAdapter.MyViewHolder holder, final int position) {
        holder.cvInProgress.setOnClickListener(mOnClickListener);
        //final HomeListData.BusinessCategoriesBean productData = movieList.get(position);
        //holder.tvStoreName.setText(movieList.get(position).getCategory_name());

        /*Glide.with(context)
                .load(movieList.get(position).getCategory_logo())
                .into(holder.ivProductImg);*/
    }

    @Override
    public int getItemCount() {
        //return movieList.size();
        return 10;
    }
}
