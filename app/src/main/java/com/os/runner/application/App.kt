package com.os.runner.application

import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.os.runner.utility.SessionManager


class App : Application(), OnCompleteListener<InstanceIdResult> {

    override fun onComplete(task: Task<InstanceIdResult>) {
        if (!task.isSuccessful) {
            Log.e("TAG", "getInstanceId failed", task.getException())
        } else {
            if (task.getResult() != null) {
                val deviceToken = task.getResult()!!.getToken()
                sessionManager!!.setDeviceToken(deviceToken)
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        //Places.initialize(getApplicationContext(), "AIzaSyCGarjo-SMyBu79kObNnO9Bw9oroR7jX7s");
        init()
        getDeviceToken()
    }

    private fun getDeviceToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(this)
    }

    private fun init() {
        if (singleton == null)
            singleton = this
        if (sessionManager == null)
            sessionManager = SessionManager(singleton!!)
        if (apiService == null)
            apiService = ApiClient.retrofitClient!!.create(ApiInterface::class.java)
        isAppRunning = true

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    companion object {
        var singleton: App? = null
        var sessionManager: SessionManager? = null
        var apiService: ApiInterface? = null
        var isAppRunning = false
        var isAppInForeground = false

    }
}