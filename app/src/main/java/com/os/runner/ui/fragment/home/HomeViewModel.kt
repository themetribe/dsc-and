package com.os.runner.ui.fragment.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.AvailabilityStatusResponse
import com.os.runner.model.HomeCountDataResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class HomeViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<AvailabilityStatusResponse>>()
    var responseHomeData = MutableLiveData<ApiResponse<HomeCountDataResponse>>()
    var apiResponse: ApiResponse<AvailabilityStatusResponse>? = null
    var apiHomeResponse: ApiResponse<HomeCountDataResponse>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiHomeResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun availability_status(jsonObject: JsonObject) {
        subscription = App.apiService!!.availability_status(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }





    fun home_data_count(jsonObject: JsonObject) {
        subscription = App.apiService!!.home_data_count(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseHomeData.postValue(apiHomeResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseHomeData.postValue(apiHomeResponse!!.success(result))
                },
                { throwable ->
                    responseHomeData.postValue(apiHomeResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}