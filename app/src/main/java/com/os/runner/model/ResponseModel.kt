package com.os.runner.model

data class ResponseModel(
    val response: ResponseData
)

data class ResponseData(
    val message: String,
    val status: Int
)