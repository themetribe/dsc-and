package com.os.runner.map;

import com.google.android.gms.maps.model.LatLng;

public class FetchUrl {
    public static String getUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=true";
        String mode = "mode=driving";
        String key = "key=AIzaSyDrILrLtYyy2sVdR1GjnRgEG_eIMsU2jcc";
        String parameters = str_origin + "&" + str_dest + "&" + sensor+"&" + mode+"&" + key;
        String output = "json";
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }
}
