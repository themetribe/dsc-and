package com.os.runner.firebase

import android.text.TextUtils
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.App
import com.os.runner.notification.NotificationGenrator
import com.os.runner.utility.SessionManager


class MyFirebaseMessagingService : FirebaseMessagingService() {

    companion object {
        var NOTIFICATION_INTENT_ACTION = "com.os.snapyo.service.notification"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("onMessageReceived", remoteMessage.toString())
        if (remoteMessage != null) {
            if (remoteMessage.data != null) {
                /*{"notification_type":"chat","service_type":"","receiver_id":"520","request_id":"775","sender_id":"512"}*/
                val body = remoteMessage.data!!

                NotificationGenrator().also {
                    it.generateNotification(this,  body)
                }
            }
        }
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        var sessionManager: SessionManager = App.sessionManager!!
        sessionManager.setDeviceToken(s)
        Log.d("fcmToken", "==" + s)
        val jsonObject = JsonObject()
        jsonObject.addProperty("device_type", "android")
        jsonObject.addProperty("device_id", sessionManager.getDeviceToken())
        jsonObject.addProperty("user_id", sessionManager.getUserID())
        App.apiService!!.updateDeviceToken(jsonObject)
            .doOnSubscribe { d ->

            }             /*Error got here https://github.com/ReactiveX/RxJava/wiki/Error-Handling*/
            .subscribe(
                { result ->

                },
                { throwable ->

                }
            )
    }



}
