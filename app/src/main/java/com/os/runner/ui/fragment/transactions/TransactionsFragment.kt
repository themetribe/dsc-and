package com.os.runner.ui.fragment.transactions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.FragmentTransactionsBinding
import com.os.runner.model.DataTransactions
import com.os.runner.model.TransactionsResponse
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.TransactionsAdapter
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.ui.fragment.BaseFragment
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.no_data_view.view.*

class TransactionsFragment : BaseFragment(), View.OnClickListener {

    private var binding: FragmentTransactionsBinding? = null
    var viewModel: TransactionsViewModel? = null

    /*Transaction Adapter*/
    private var dataTransactionsList: ArrayList<DataTransactions>? = ArrayList()
    private var transactionsAdapter: TransactionsAdapter? = null
    private var onClickListener: View.OnClickListener? = null

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        viewModel = ViewModelProvider(this).get(TransactionsViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_transactions, container, false)
        binding!!.lifecycleOwner = this
        onClickListener = this
        return binding!!
    }

    override fun createActivityObject() {
        mActivity = activity
    }

    override fun initializeObject() {
        initControl()
    }

    private fun initControl() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerView.setLayoutManager(layoutManager)

        transactionsAdapter = TransactionsAdapter(mActivity!!, dataTransactionsList!!, onClickListener)
        binding!!.recyclerView.adapter = transactionsAdapter
    }

    override fun onResume() {
        super.onResume()
        transactionApiCall()
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }

    private fun transactionApiCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.RUNNER_ID, App.sessionManager!!.getUserID())
        viewModel!!.transactions_history(jsonObject)
    }


    private fun handleResult(result: ApiResponse<TransactionsResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if (response.data.size==0) {
                        binding!!.txtNoDataFound.visibility = View.VISIBLE
                        binding!!.txtNoDataFound.txtMessage.text = "You currently have no transactions"
                        binding!!.recyclerView.visibility = View.GONE
                    } else {
                        binding!!.txtNoDataFound.visibility = View.GONE
                        binding!!.recyclerView.visibility = View.VISIBLE
                        setAdapters(response.data!!)
                    }

                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setAdapters(data: List<DataTransactions>) {
        if (data != null) {
            dataTransactionsList!!.clear()
            dataTransactionsList!!.addAll(data)
            transactionsAdapter!!.notifyDataSetChanged()
        }
    }



    override fun onClick(p0: View?) {
        TODO("Not yet implemented")
    }
}