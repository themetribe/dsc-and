package com.os.runner.model

import java.io.Serializable

data class CancelRequestResponse(
    val data: List<CancelData>
):Serializable

data class CancelData(
    var name: String=""
):Serializable