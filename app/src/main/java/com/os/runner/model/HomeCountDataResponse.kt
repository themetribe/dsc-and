package com.os.runner.model

data class HomeCountDataResponse(
    val `data`: DataHomeCount,
    val message: String,
    val status: Int
)

data class DataHomeCount(
    val pending_request: String,
    val unread_notiification: String,
    val view_status: String,
    val current_position: String
)