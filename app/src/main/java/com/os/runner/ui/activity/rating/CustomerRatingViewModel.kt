package com.os.runner.ui.activity.rating

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import com.os.runner.model.RatingListResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CustomerRatingViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var responseRatingLiveData = MutableLiveData<ApiResponse<RatingListResponse>>()
    var apiResponse: ApiResponse<DefaultPojo>? = null
    var apiRatingResponse: ApiResponse<RatingListResponse>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiRatingResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun add_rating(jsonObject: JsonObject) {
        subscription = App.apiService!!.add_rating(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }

    fun my_rating(jsonObject: JsonObject) {
        subscription = App.apiService!!.my_rating(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseRatingLiveData.postValue(apiRatingResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseRatingLiveData.postValue(apiRatingResponse!!.success(result))
                },
                { throwable ->
                    responseRatingLiveData.postValue(apiRatingResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}