package com.os.runner.utility

class ChatConversationResponse() {
    var country_code = ""
    var email_id = ""
    var first_name = ""
    var last_name = ""
    var mobile_no = ""
    var user_id = ""
    var user_image = ""
    var message = ""
    var is_online = false
    var time:Long? = null
}