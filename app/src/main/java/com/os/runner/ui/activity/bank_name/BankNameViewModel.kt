package com.os.runner.ui.activity.bank_name

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.os.runner.application.ApiResponse
import com.os.runner.model.DefaultPojo
import io.reactivex.disposables.Disposable

class BankNameViewModel : ViewModel() {

    var responseCancelData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiResponse: ApiResponse<DefaultPojo>? = null


    private var subscription: Disposable? = null

    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }

   /* fun customer_cancel_request(jsonObject: JsonObject) {

        subscription = App.apiService!!.customer_cancel_request(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseCancelData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseCancelData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseCancelData.postValue(apiResponse!!.error(throwable))
                }
            )
    }
*/

    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}