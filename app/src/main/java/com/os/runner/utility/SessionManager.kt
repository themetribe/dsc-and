package com.os.runner.utility

import android.content.Context
import android.content.SharedPreferences
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.os.runner.model.Data

class SessionManager(var context: Context) : BaseObservable() {

    internal var sharedPref: SharedPreferences? = null

    internal var editor: SharedPreferences.Editor? = null

    init {
        sharedPref = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
        editor = sharedPref!!.edit()
    }

    fun setUserData(userData: Data) {
        editor!!.putString(Constants.FULL_NAME, userData.first_name+" "+userData.last_name)
        editor!!.putString(Constants.FIRST_NAME, userData.first_name)
        editor!!.putString(Constants.LAST_NAME, userData.last_name)
        editor!!.putString(Constants.EMAIL, userData.email)
        editor!!.putString(Constants.IMAGE, userData.profile_pic)
        editor!!.putString(Constants.MOBILE, userData.mobile)
        editor!!.putString(Constants.USERID, userData.user_id)
        editor!!.putString(Constants.COUNTRY_CODE, userData.country_code)
        editor!!.commit()
        notifyPropertyChanged(BR.fullName)
    }

    fun setLogout() {
        editor!!.putString(Constants.FULL_NAME, "")
        editor!!.putString(Constants.FIRST_NAME, "")
        editor!!.putString(Constants.LAST_NAME, "")
        editor!!.putString(Constants.EMAIL, "")
        editor!!.putString(Constants.IMAGE, "")
        editor!!.putString(Constants.MOBILE, "")
        editor!!.putString(Constants.USERID, "")
        editor!!.putString(Constants.USER_TYPE, "")
        editor!!.putString(Constants.COUNTRY_CODE, "")
        editor!!.putBoolean(Constants.IS_LOGIN, false)
        editor!!.commit()
        notifyPropertyChanged(BR.fullName)
    }


    @Bindable("fullName")
    fun getFullName(): String {
        return sharedPref!!.getString(Constants.FULL_NAME, "")!!
    }

    fun getUserID(): String {
        return sharedPref!!.getString(Constants.USERID, "")!!
    }

    fun getEmail(): String {
        return sharedPref!!.getString(Constants.EMAIL, "")!!
    }

    fun getImage(): String {
        return sharedPref!!.getString(Constants.IMAGE, "")!!
    }

    fun getRanking(): String {
        return sharedPref!!.getString(Constants.RANKING, "")!!
    }


    fun setRanking(ranking: String) {
        editor!!.putString(Constants.RANKING, ranking)
        editor!!.commit()
    }

    fun getRunnerPosition(): String {
        return sharedPref!!.getString(Constants.RUNNER_POSITION, "")!!
    }


    fun setRunnerPosition(position: String) {
        editor!!.putString(Constants.RUNNER_POSITION, position)
        editor!!.commit()
    }

    fun getPoints(): String {
        return sharedPref!!.getString(Constants.POINTS, "")!!
    }


    fun setPoints(points: String) {
        editor!!.putString(Constants.POINTS, points)
        editor!!.commit()
    }

    fun getWalletAMOUNT(): String {
        return sharedPref!!.getString(Constants.WALLET_AMOUNT, "")!!
    }


    fun setWalletAMOUNT(wallet_amount: String) {
        editor!!.putString(Constants.WALLET_AMOUNT, wallet_amount)
        editor!!.commit()
    }

    fun getAuthToken(): String {
        return sharedPref!!.getString(Constants.AUTHTOKEN, "")!!
    }


    fun setAuthToken(authToken: String) {
        editor!!.putString(Constants.AUTHTOKEN, authToken)
        editor!!.commit()
    }


    fun getFirstName(): String? {
        return sharedPref!!.getString(Constants.FIRST_NAME, "")
    }

    fun getLastName(): String? {
        return sharedPref!!.getString(Constants.LAST_NAME, "")
    }



    fun getMobile(): String? {
        return sharedPref!!.getString(Constants.MOBILE, "")
    }

    fun isLogin(): Boolean {
        return sharedPref!!.getBoolean(Constants.IS_LOGIN, false)
    }

    fun setLogin(value: Boolean) {
        editor!!.putBoolean(Constants.IS_LOGIN, value)
        editor!!.commit()
    }

    fun isApproved(): Boolean {
        return sharedPref!!.getBoolean(Constants.IS_APPROVED, false)
    }

    fun setApproved(value: Boolean) {
        editor!!.putBoolean(Constants.IS_APPROVED, value)
        editor!!.commit()
    }

    fun getDeviceToken(): String? {
        return sharedPref!!.getString(Constants.DEVICE_ID, "")
    }

    fun setDeviceToken(deviceToken: String) {
        editor!!.putString(Constants.DEVICE_ID, deviceToken)
        editor!!.commit()
    }

    fun getLatitute(): String? {
        return sharedPref!!.getString(Constants.LATITUDE_NAME, "0.0")
    }

    fun setLatitute(address: String) {
        editor!!.putString(Constants.LATITUDE_NAME, address)
        editor!!.commit()
    }

    fun getLongitute(): String? {
        return sharedPref!!.getString(Constants.LONGITUDE_NAME, "0.0")
    }

    fun setLongitute(address: String) {
        editor!!.putString(Constants.LONGITUDE_NAME, address)
        editor!!.commit()
    }


    fun getAddress(): String? {
        return sharedPref!!.getString(Constants.ADDRESS, "Select address")
    }

    fun setAddress(address: String) {
        editor!!.putString(Constants.ADDRESS, address)
        editor!!.commit()
    }


    fun getAvailabilityStatus(): String? {
        return sharedPref!!.getString(Constants.STATUS, "status")
    }

    fun setAvailabilityStatus(status: String) {
        editor!!.putString(Constants.STATUS, status)
        editor!!.commit()
    }

    fun setUserType(type: String) {
        editor!!.putString(Constants.USER_TYPE, type)
        editor!!.commit()
    }

    fun getUserType(): String? {
        return sharedPref!!.getString(Constants.USER_TYPE, "0")
    }

    fun setLanguage(type: String) {
        editor!!.putString(Constants.LANGUAGE, type)
        editor!!.commit()
    }

    fun getLanguage(): String? {
        return sharedPref!!.getString(Constants.LANGUAGE, "en")
    }

    fun getCountryCode(): Int? {
        return sharedPref!!.getInt(Constants.COUNTRY_CODE, 0)
    }


    fun clearUserData() {
        editor!!.clear()
        editor!!.commit()
    }


}