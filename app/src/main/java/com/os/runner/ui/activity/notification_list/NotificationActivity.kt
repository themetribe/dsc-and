package com.os.runner.ui.activity.notification_list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.NotificationActivityBinding
import com.os.runner.model.*
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.NotificationAdapter
import com.os.runner.ui.dialogs.AlertMessageDialog
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_view.view.*
import java.util.*

class NotificationActivity : BaseBindingActivity(), View.OnClickListener{


    var viewModel: NotificationsViewModel? = null
    private var binding: NotificationActivityBinding? = null
    private var onClickListener: View.OnClickListener? = null
    private var notificationType: String=""
    private var serviceName: String=""
    private var requestId: String = ""

    /*Notification Adapter*/
    private var notifDataList: ArrayList<NotifData>? = ArrayList()
    private var notificationAdapter: NotificationAdapter? = null


    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(NotificationsViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.notification_activity)
        binding!!.session = App.sessionManager!!
        onClickListener=this
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, NotificationActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, androidx.lifecycle.Observer {
            handleResult(it)
        })
        viewModel!!.responseReadData.observe(this, androidx.lifecycle.Observer {
            handleReadUnreadResult(it)
        })
    }

    private fun initControl() {
        binding!!.toolbarLayout.txtClearAll.setOnClickListener(onClickListener)
        binding!!.toolbarLayout.imgBack.setOnClickListener(onClickListener)
        binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.notification)
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity!!, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerView.layoutManager = layoutManager

        notificationAdapter = NotificationAdapter(mActivity!!, notifDataList!!, onClickListener)
        binding!!.recyclerView.adapter = notificationAdapter
        notificationApiCall()
    }

    private fun notificationApiCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        viewModel!!.notification_list(jsonObject)
    }

    private fun handleResult(result: ApiResponse<NotifiationResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if (response.data.notif_data.size==0) {
                        binding!!.txtNoDataFound.visibility = View.VISIBLE
                        binding!!.txtNoDataFound.txtMessage.text = "You currently have no notification"
                        binding!!.txtNoDataFound.imvNoData.setImageDrawable(resources.getDrawable(R.drawable.notification_no_data))
                        binding!!.recyclerView.visibility = View.GONE
                        binding!!.toolbarLayout.txtClearAll.visibility = View.GONE
                    } else {
                        binding!!.txtNoDataFound.visibility = View.GONE
                        binding!!.recyclerView.visibility = View.VISIBLE
                        binding!!.toolbarLayout.txtClearAll.visibility = View.VISIBLE
                        setAdapters(response.data.notif_data)

                    }
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setAdapters(notifData: List<NotifData>) {
        if (notifData != null) {
            notifDataList!!.clear()
            notifDataList!!.addAll(notifData)
            notificationAdapter!!.notifyDataSetChanged()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack->{
                finish()
            }
            R.id.rlMainNotificationView -> {
                val position = v.tag as Int
                notificationType = ""
                requestId = ""
                serviceName = ""
                if(notifDataList!![position].is_read.equals("0")) {
                    readUnreadApiCall(notifDataList!![position].notification_id)
                    if (notifDataList!![position].notification_type.equals("request")){
                        notificationType = notifDataList!![position].notification_type
                        requestId = notifDataList!![position].request_id
                        serviceName = notifDataList!![position].service_name
                    }
                }else{
                    /*if(notifDataList!![position].notification_type.equals("request")){
                        var bundle = Bundle()
                        bundle.putString("request_id",notifDataList!![position].request_id)
                        bundle.putString("help_type",notifDataList!![position].service_name)
                        RequestDetailActivity.startActivity(mActivity!!,bundle)
                    }*/
                }
            }
            R.id.txtClearAll -> {
                AlertMessageDialog.showDialog(mActivity!!,
                    "Alert",
                    "Are you sure, you want to clear all notification?",
                    "yes",
                    getString(R.string.cancel_alert),
                    object : AlertMessageDialog.AlertMessageCallback {
                        override fun onSuccessClick() {
                            var jsonObject = JsonObject()
                            jsonObject.addProperty("user_id", App.sessionManager!!.getUserID())
                            jsonObject.addProperty("type", "all")
                            jsonObject.addProperty("notification_id", "")
                            viewModel!!.notification_delete(jsonObject)
                        }

                        override fun onCancelClick() {

                        }
                    })
            }
            R.id.imvDeleteNotification->{
                val position = v.tag as Int
                var jsonObject = JsonObject()
                jsonObject.addProperty("user_id", App.sessionManager!!.getUserID())
                jsonObject.addProperty("type", "manual")
                jsonObject.addProperty("notification_id", notifDataList!![position].notification_id)
                viewModel!!.notification_delete(jsonObject)
            }
        }
    }

    private fun readUnreadApiCall(notificationId: String) {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty("is_read", "1")
        jsonObject.addProperty("notification_id", notificationId)
        viewModel!!.notification_read_unread(jsonObject)
    }


    private fun handleReadUnreadResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    notificationApiCall()
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }






}