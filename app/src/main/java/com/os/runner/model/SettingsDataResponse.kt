package com.os.runner.model

import java.io.Serializable

data class SettingsDataResponse(
    val response: ResponseSettingData
)

data class ResponseSettingData(
    val `data`: DataSettings,
    val message: String,
    val status: Int
)

data class DataSettings(
    val id: Int,
    val register_gems: String,
    val send_request_gems: String,
    val daily_gems: String,
    val video_gems: String,
    val insta_gems: String,
    val copy_link: String,
    val install_ads: String,
    val created_at: String,
    val updated_at: String
): Serializable