package com.os.runner.model

data class AvailabilityStatusResponse(
    val `data`: DataAvailabilityStatus,
    val message: String,
    val status: Int
)

data class DataAvailabilityStatus(
    val id: String,
    val status: String
)