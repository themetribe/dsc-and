package com.os.runner.model

data class UploadImageResponse(
    val `data`: DataUploadImage,
    val message: String,
    val status: Int
)

data class DataUploadImage(
    val created_at: String,
    val id: Int,
    val image_url: String,
    val request_id: String,
    val updated_at: String
)