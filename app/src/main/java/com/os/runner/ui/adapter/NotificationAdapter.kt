package com.os.runner.ui.adapter

import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.app.dstarrunner.R
import com.os.runner.model.NotifData
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.row_notifications.view.*

class NotificationAdapter(
    private val context: Activity?,
    private val list: ArrayList<NotifData>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_notifications

    override fun getViewModel(position: Int): Any? = list[position]
    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        viewDataBinding.root.txtTitle.text = list[position].title
        viewDataBinding.root.txtDescription.text = list[position].notification
        viewDataBinding.root.txtCreatedDate.text = UtilityMethod.parseDateFormated(list[position].created_at)
        if(list[position].is_read.equals("1")){
            viewDataBinding.root.imvLeftNotificationIcon.setImageDrawable(context!!.getDrawable(R.drawable.circleunselect))
        }else{
            viewDataBinding.root.imvLeftNotificationIcon.setImageDrawable(context!!.getDrawable(R.drawable.circleselect))
        }
        viewDataBinding.root.rlMainNotificationView.tag = position
        viewDataBinding.root.imvDeleteNotification.tag = position
        viewDataBinding.root.rlMainNotificationView.setOnClickListener(onClickListener)
        viewDataBinding.root.imvDeleteNotification.setOnClickListener(onClickListener)
    }
    override fun getItemCount(): Int = list.size
}