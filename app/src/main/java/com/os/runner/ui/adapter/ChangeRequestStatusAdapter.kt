package com.os.runner.ui.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.app.dstarrunner.R
import com.os.runner.model.DropInfoRequestDetail
import kotlinx.android.synthetic.main.row_change_request_status.view.*
import kotlinx.android.synthetic.main.row_drop_info_item_location.view.*
import kotlinx.android.synthetic.main.row_drop_info_item_location.view.txtLocationName
import kotlinx.android.synthetic.main.row_drop_info_item_location.view.txtMobile

class ChangeRequestStatusAdapter(
    private val context: Activity?,
    private val list: ArrayList<DropInfoRequestDetail>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_change_request_status

    override fun getViewModel(position: Int): Any? = list[position]

    @SuppressLint("SetTextI18n")
    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        if(list[position].request_name.equals("Help Me Queue")){
            viewDataBinding.root.txtLocationName.text = "Queue Location"
            viewDataBinding.root.btnTapToChange.text = "Tap to Queue"
        }else if(list[position].request_name.equals("Help Me Buy")){
            viewDataBinding.root.txtLocationName.text = "Buying Location"
            viewDataBinding.root.btnTapToChange.text = "Tap to Buy"
        } else if(list[position].request_name.equals("Help Me Send")){
            viewDataBinding.root.txtLocationName.text = "Pickup Location"
            viewDataBinding.root.btnTapToChange.text = "Tap to Pick"
        }else{
            viewDataBinding.root.btnTapToChange.text = "Tap to Deliver"
        }

        if(list[position].status.equals("submit")&&list[position].request_name.equals("Help Me Queue")) {
            viewDataBinding.root.btnTapToChange.text = "Finished"
            viewDataBinding.root.btnTapToChange.isEnabled = false
        }else if(list[position].status.equals("submit")&&!list[position].request_name.equals("Help Me Queue")){
            viewDataBinding.root.btnTapToChange.text = "Completed"
            viewDataBinding.root.btnTapToChange.isEnabled = false
        }

        viewDataBinding.root.txtLocationAdress.text = list[position].address

        viewDataBinding.root.btnTapToChange.tag = position
        viewDataBinding.root.btnTapToChange.setOnClickListener(onClickListener)
    }

    override fun getItemCount(): Int = list.size
}