package com.os.drunnercustomer.ui.fragment.completed

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.RequestResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CompletedViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<RequestResponse>>()
    var apiResponse: ApiResponse<RequestResponse>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun requests_for_runner(jsonObject: JsonObject) {
        subscription = App.apiService!!.requests_for_runner(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}