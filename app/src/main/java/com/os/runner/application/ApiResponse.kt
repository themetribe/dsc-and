package com.os.runner.application

import io.reactivex.annotations.NonNull
import io.reactivex.annotations.Nullable
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by ${Saquib} on 03-05-2018.
 */

class ApiResponse<T> public constructor(
    val status: Status, @param:Nullable @field:Nullable
    val data: T?,
    val error: String?
) {

    fun loading(): ApiResponse<T> {
        return ApiResponse(Status.LOADING, null, null)
    }

    fun success(@NonNull data: T): ApiResponse<T> {
        return ApiResponse(Status.SUCCESS, data, null)
    }

    fun error(@NonNull error: Throwable): ApiResponse<T> {
        var message = "Something is went wrong, Please try again!!"
        if (error is SocketTimeoutException) message = "It seems you are not connected to internet!!"
        else if (error is UnknownHostException) message =
            "It seems you are not connected to internet!!"
        else if (error is HttpException) message = "Invalid Request!!"
        return ApiResponse(Status.ERROR, null, message)
    }

    enum class Status {
        LOADING,
        SUCCESS,
        ERROR
    }

}
