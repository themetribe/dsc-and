package com.os.runner.model

import java.io.Serializable

data class RequestDetailResponse(
    val `data`: DataRequestDetail,
    val message: String,
    val status: Int
):Serializable

data class DataRequestDetail(
    val accept_time: String,
    val admin_commission_amount: String,
    val amount: String,
    val at_job_location: String,
    val booking_id: String,
    val user_image: String,
    val category_id: String,
    val category_name: String,
    val completion_time: String,
    val created_at: String,
    val distance: String,
    val drop_info: List<DropInfoRequestDetail>,
    val duration: String,
    val item_detail: String,
    val multistop_charge: String,
    val net_amount: String,
    val offer_amount: String,
    val paid_amount: String,
    val pickup_info: PickupInfoRequestDetail,
    val request_id: String,
    val request_type: String,
    val runner_detail: RunnerRequestDetail,
    val schedule_date_time: String,
    val service_id: String,
    val service_name: String,
    val start_job: String,
    val start_time: String,
    val status: String,
    val surcharge: String,
    val total_distance: String,
    val transport_category: String,
    val user_id: String,
    val user_mobile: String,
    val user_name: String,
    val waiting_charges: String,
    val weight: String,
    val zone_id: String
):Serializable

data class DropInfoRequestDetail(
    var address: String="",
    var contact_number: String="",
    var landmark: String="",
    var latitude: String="",
    var longitude: String="",
    var name: String="",
    var status: String="",
    var request_name: String=""
):Serializable

data class PickupInfoRequestDetail(
    val address: String,
    val contact_number: String,
    val landmark: String,
    val latitude: String,
    val longitude: String,
    val name: String,
    val status: String
):Serializable

data class RunnerRequestDetail(
    val is_favourite: String,
    val runner_id: String,
    val runner_image: String,
    val runner_mobile: String,
    val runner_name: String,
    val runner_vehicle: String
):Serializable