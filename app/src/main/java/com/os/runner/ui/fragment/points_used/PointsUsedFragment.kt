package com.os.runner.ui.fragment.points_used

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.app.dstarrunner.R
import com.app.dstarrunner.databinding.FragmentPointsReceivedBinding
import com.os.runner.ui.adapter.ItemHeader
import com.os.runner.ui.adapter.ItemHeaderViewBinder
import com.os.runner.ui.adapter.Result
import com.os.runner.ui.adapter.User
import com.os.runner.ui.adapter.UserItemViewBinder
import com.os.runner.ui.fragment.BaseFragment
import tellh.com.stickyheaderview_rv.adapter.DataBean
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter
import java.util.*


class PointsUsedFragment : BaseFragment() {

    private var binding: FragmentPointsReceivedBinding? = null
    var viewModel: PointsUsedViewModel? = null
    private var adapter: StickyHeaderViewAdapter? = null

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        viewModel = ViewModelProvider(this).get(PointsUsedViewModel::class.java)
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_points_received,
            container,
            false
        )
        binding!!.lifecycleOwner = this
        return binding!!
    }

    override fun createActivityObject() {
        mActivity = activity
    }

    override fun initializeObject() {
        initControl()
    }

    private fun initControl() {
        binding!!.recyclerView.setLayoutManager(LinearLayoutManager(context))
        initData()
       /* val userList: List<DataBean> = ArrayList()*/
        /*adapter = StickyHeaderViewAdapter(userList)
            .RegisterItemType(UserItemViewBinder())
            .RegisterItemType(ItemHeaderViewBinder())
        binding!!.recyclerView.setAdapter(adapter)*/
    }

    private fun initData() {
        val gson = Gson()
        val result: Result = gson.fromJson(User.dataSource, Result::class.java)
        val userList: List<User> = result.getItems()
        Collections.sort<User>(
            userList
        ) { o1, o2 -> o1.getLogin().compareTo(o2.getLogin()) }
        val userListBak: MutableList<DataBean> = java.util.ArrayList()
        var currentPrefix: String = userList[0].getLogin().substring(0, 1).toUpperCase()
        userListBak.add(ItemHeader(currentPrefix))
        for (user in userList) {
            if (currentPrefix.compareTo(
                    user.getLogin().substring(0, 1),
                    ignoreCase = true
                ) == 0
            ) userListBak.add(user) else {
                currentPrefix = user.getLogin().substring(0, 1).toUpperCase()
                userListBak.add(ItemHeader(currentPrefix))
                userListBak.add(user)
            }
        }
        adapter = StickyHeaderViewAdapter(userListBak)
            .RegisterItemType(UserItemViewBinder())
            .RegisterItemType(ItemHeaderViewBinder())
        binding!!.recyclerView.setAdapter(adapter)
    }

    override fun setListeners() {
       /* viewModel!!.responseLiveData.observe(this, androidx.lifecycle.Observer {
            handleResult(it)
        })*/
    }
}