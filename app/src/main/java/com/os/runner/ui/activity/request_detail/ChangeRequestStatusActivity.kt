package com.os.runner.ui.activity.request_detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.os.drunnercustomer.ui.activity.request_detail.RequestDetailViewModel
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityChangeRequestStatusBinding
import com.os.runner.model.DefaultPojo
import com.os.runner.model.DropInfoRequestDetail
import com.os.runner.model.RequestDetailResponse
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.ChangeRequestStatusAdapter
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import java.util.*
import kotlin.collections.ArrayList

class ChangeRequestStatusActivity : BaseBindingActivity(), View.OnClickListener {


    private var binding: ActivityChangeRequestStatusBinding? = null
    private var viewModel: RequestDetailViewModel? = null

    /*Pending Requests Adapter*/
    private var dataPickupInfoList: ArrayList<DropInfoRequestDetail>? = ArrayList()
    private var tempDataPickupInfoList: ArrayList<DropInfoRequestDetail>? = ArrayList()
    private var changeRequestStatusAdapter: ChangeRequestStatusAdapter? = null
    private var onClickListener: View.OnClickListener? = null
    private var request_id = ""
    private var requestDetailResponse: RequestDetailResponse? = null


    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(RequestDetailViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_request_status)
        onClickListener = this
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        if (intent.extras != null) {
            request_id = intent.extras!!.getBundle("bundle")!!.getString("request_id") as String
        }
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, ChangeRequestStatusActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent,111)
        }
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
        viewModel!!.responseStartJobData.observe(this, Observer {
            handleActionResult(it)
        })
    }

    private fun initControl() {
        binding!!.toolbarLayout.toolbarTitle.text = "Change Request Status"
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding!!.recyclerView.layoutManager = mLayoutManager

        changeRequestStatusAdapter =
            ChangeRequestStatusAdapter(this, dataPickupInfoList!!, onClickListener)
        binding!!.recyclerView.adapter = changeRequestStatusAdapter
        request_detailCall()
    }


    private fun request_detailCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty("request_id", request_id)
        viewModel!!.runner_request_detail(jsonObject)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(301)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.btnTapToChange -> {
                val position = v.tag as Int
                if(position==0&&tempDataPickupInfoList!![0].status.equals("pending")){
                    changeStatusCall(position)
                }else if(position>0&&tempDataPickupInfoList!![0].status.equals("submit")) {
                    changeStatusCall(position)
                }else{
                    if(requestDetailResponse!!.data.service_name.equals("Help Me Buy")){
                        UiHelper.showErrorToastMsgShort(mActivity!!,"Please select first tap to buy")
                    }
                    if(requestDetailResponse!!.data.service_name.equals("Help Me Send")){
                        UiHelper.showErrorToastMsgShort(mActivity!!,"Please select first tap to Pick up")
                    }

                }
            }
        }
    }


    private fun changeStatusCall(position: Int) {
        var json_dropoff_location = JsonArray()
        val pickupObject = JsonObject()
        for (i in 0 until requestDetailResponse!!.data.drop_info.size) {
            var jsonObject_delivery = JsonObject()
            jsonObject_delivery.addProperty("address", requestDetailResponse!!.data.drop_info[i].address)
            jsonObject_delivery.addProperty("contact_number", requestDetailResponse!!.data.drop_info[i].contact_number)
            jsonObject_delivery.addProperty("landmark", requestDetailResponse!!.data.drop_info[i].landmark)
            jsonObject_delivery.addProperty("latitude", requestDetailResponse!!.data.drop_info[i].latitude)
            jsonObject_delivery.addProperty("longitude", requestDetailResponse!!.data.drop_info[i].longitude)
            jsonObject_delivery.addProperty("name", requestDetailResponse!!.data.drop_info[i].name)
            if(i==position-1){
                jsonObject_delivery.addProperty("status", "submit")
            }else{
                jsonObject_delivery.addProperty("status", requestDetailResponse!!.data.drop_info[i].status)
            }
            json_dropoff_location.add(jsonObject_delivery)
        }

        pickupObject.addProperty("address", requestDetailResponse!!.data.pickup_info.address)
        pickupObject.addProperty(
            "contact_number",
            requestDetailResponse!!.data.pickup_info.contact_number
        )
        pickupObject.addProperty("landmark", requestDetailResponse!!.data.pickup_info.landmark)
        pickupObject.addProperty("latitude", requestDetailResponse!!.data.pickup_info.latitude)
        pickupObject.addProperty("longitude", requestDetailResponse!!.data.pickup_info.longitude)
        pickupObject.addProperty("name", requestDetailResponse!!.data.pickup_info.name)
        if(position==0 && requestDetailResponse!!.data.pickup_info.status.equals("pending")){
            pickupObject.addProperty("status", "submit")
        }else{
            pickupObject.addProperty("status", requestDetailResponse!!.data.pickup_info.status)
        }


        val reqData: HashMap<String, Any> = HashMap()
        reqData.put(Constants.RUNNER_ID, App.sessionManager!!.getUserID())
        reqData.put("request_id", request_id)
        reqData.put("pickup_info", pickupObject)
        reqData.put("drop_info", json_dropoff_location)
        viewModel!!.action_start_job(reqData)

    }

    private fun handleActionResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    request_detailCall()
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun handleResult(result: ApiResponse<RequestDetailResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    requestDetailResponse = result.data
                    var address = response.data.pickup_info.address
                    var contact_number = ""
                    var landmark = ""
                    var latitude = ""
                    var longitude = ""
                    var name = ""
                    var status = response.data.pickup_info.status
                    var service_name = response.data.service_name
                    var datapickupInfo = DropInfoRequestDetail(
                        address,
                        contact_number,
                        landmark,
                        latitude,
                        longitude,
                        name,
                        status,
                        service_name
                    )
                    tempDataPickupInfoList!!.clear()
                    tempDataPickupInfoList!!.add(datapickupInfo)
                    tempDataPickupInfoList!!.addAll(response.data.drop_info)
                    setAdapters(tempDataPickupInfoList!!)
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }


    private fun setAdapters(data: List<DropInfoRequestDetail>) {
        if (data != null) {
            dataPickupInfoList!!.clear()
            dataPickupInfoList!!.addAll(data)
            changeRequestStatusAdapter!!.notifyDataSetChanged()
        }
    }

}