package com.os.runner.ui.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.app.dstarrunner.R
import com.os.runner.model.CancelData
import kotlinx.android.synthetic.main.row_cancel_request.view.*

class BankNameAdapter(
    private val context: Activity?,
    private val list: ArrayList<CancelData>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_cancel_request

    override fun getViewModel(position: Int): Any? = list[position]

    @SuppressLint("SetTextI18n")
    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        viewDataBinding.root.txtCancelContent.text = list[position].name
        viewDataBinding.root.rlMainView.tag = position
        viewDataBinding.root.rlMainView.setOnClickListener(onClickListener)
    }

    override fun getItemCount(): Int = list.size
}