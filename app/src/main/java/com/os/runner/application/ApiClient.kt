package com.os.runner.application

import android.util.Log
import com.google.gson.GsonBuilder
import com.app.dstarrunner.BuildConfig
import com.os.runner.utility.SessionManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import java.util.concurrent.TimeUnit

object ApiClient {
    val LIVE_URL = "http://13.235.72.118:1234/"
   // val LOCAL_URL = "https://72.octalsecure.com/dstarrunner/api/"  /*Live*/
    //val LOCAL_URL = "https://72.octaldevs.com/dstarrunner/api/"  /*Live*/

    val LOCAL_URL = "http://dev.dstar-runner.com/api/"  /*Live*/

    private var retrofit: Retrofit? = null
    private var retrofitImage: Retrofit? = null
    private val timeOutTime: Long = 240
    val retrofitClient: Retrofit?
        get() {
            if (retrofit == null) {
                val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
                val builder = OkHttpClient.Builder()
                builder.connectTimeout(timeOutTime, TimeUnit.SECONDS)
                builder.readTimeout(timeOutTime, TimeUnit.SECONDS)
                builder.writeTimeout(timeOutTime, TimeUnit.SECONDS)
                var sessionManager: SessionManager = SessionManager(App.singleton!!)
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor { chain ->
                    val request = chain.request().newBuilder()
                        .addHeader("auth", sessionManager.getAuthToken()).build()
                    if (BuildConfig.DEBUG)
                        Log.e("authToken", sessionManager.getAuthToken())
                    chain.proceed(request)
                }.addInterceptor(httpLoggingInterceptor)
                builder.networkInterceptors().add(httpLoggingInterceptor)
                val okHttpClient = builder.build()
                retrofit = Retrofit.Builder()
                    .baseUrl(LOCAL_URL)
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }
            return retrofit
        }


    val retrofitClientForImage: Retrofit?
        get() {
            if (retrofitImage == null) {
                val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
                val builder = OkHttpClient.Builder()
                builder.connectTimeout(timeOutTime, TimeUnit.SECONDS)
                builder.readTimeout(timeOutTime, TimeUnit.SECONDS)
                builder.writeTimeout(timeOutTime, TimeUnit.SECONDS)

                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(httpLoggingInterceptor)
                builder.networkInterceptors().add(httpLoggingInterceptor)
                val okHttpClient = builder.build()
                retrofitImage = Retrofit.Builder()
                    .baseUrl(LOCAL_URL)
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }
            return retrofitImage
        }

}