package com.os.runner.ui.fragment.account

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.FragmentAccountBinding
import com.os.runner.model.Data
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.change_password.ChangePasswordActivity
import com.os.runner.ui.activity.edit_profile.EditProfileActivity
import com.os.runner.ui.activity.my_earning.MyEarningActivity
import com.os.runner.ui.activity.rating.RatingListActivity
import com.os.runner.ui.activity.settings.SettingsActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.activity.top_runner.TopRunnerActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.ui.fragment.BaseFragment
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import zendesk.chat.Chat
import zendesk.chat.ChatConfiguration
import zendesk.chat.ChatEngine
import zendesk.chat.VisitorInfo
import zendesk.messaging.MessagingActivity

class AccountFragment : BaseFragment(), View.OnClickListener{

    private var binding: FragmentAccountBinding? = null
    var viewModel: AccountViewModel? = null
    var userPojo: Data? = null

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        viewModel = ViewModelProvider(this).get(AccountViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false)
        binding!!.lifecycleOwner = this
        return binding!!
    }

    override fun createActivityObject() {
        mActivity = activity
    }

    override fun initializeObject() {
        //ZopimChat.init(getString(R.string.zendesk_chat_id))
        Chat.INSTANCE.init(mActivity!!.applicationContext, getString(R.string.zendesk_chat_id), mActivity!!.packageName)
        initControl()
    }

    private fun initControl() {
        binding!!.txtEditProfile.setOnClickListener(this)
        binding!!.rlSettings.setOnClickListener(this)
        binding!!.rlChangePassword.setOnClickListener(this)
        binding!!.rlHelpCenter.setOnClickListener(this)
        binding!!.rlInviteFriends.setOnClickListener(this)
        binding!!.rlTop10Runner.setOnClickListener(this)
        binding!!.rlMyEarning.setOnClickListener(this)
        binding!!.rlProfileView.setOnClickListener(this)
        binding!!.rlRatings.setOnClickListener(this)
        binding!!.rlRatingView.setOnClickListener(this)

    }

    override fun onResume() {
        super.onResume()
        profileApiCall()
    }


    private fun profileApiCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        viewModel!!.profile_api(jsonObject)
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtEditProfile -> {
                val bundle = Bundle()
                bundle.putSerializable("fromActivity", "account")
                EditProfileActivity.startActivity(context as Activity, bundle,false)
            }
            R.id.rlProfileView -> {
                val bundle = Bundle()
                bundle.putSerializable("fromActivity", "account")
                EditProfileActivity.startActivity(context as Activity, bundle,false)
            }
            R.id.rlRatings -> {
                RatingListActivity.startActivity(context as Activity, null)
            }
            R.id.rlRatingView -> {
                RatingListActivity.startActivity(context as Activity, null)
            }
            R.id.rlMyEarning -> {
                MyEarningActivity.startActivity(context as Activity, null)
            }
            R.id.rlSettings -> {
                SettingsActivity.startActivity(context as Activity, null)
            }
            R.id.rlTop10Runner -> {
                TopRunnerActivity.startActivity(context as Activity, null)
            }
            R.id.rlChangePassword -> {
                ChangePasswordActivity.startActivity(context as Activity, null)
            }
            R.id.rlHelpCenter->{
                val chatConfiguration = ChatConfiguration.builder()
                    .withAgentAvailabilityEnabled(false)
                    .build()

                val profileProvider = Chat.INSTANCE.providers()!!.profileProvider()
                val chatProvider = Chat.INSTANCE.providers()!!.chatProvider()

                val visitorInfo = VisitorInfo.builder()
                    .withName(App.sessionManager!!.getFirstName())
                    .withEmail(App.sessionManager!!.getEmail())
                    .withPhoneNumber(App.sessionManager!!.getMobile()) // numeric string
                    .build()
                profileProvider.setVisitorInfo(visitorInfo, null)
                chatProvider.setDepartment("A department", null)

                MessagingActivity.builder()
                    .withEngines(ChatEngine.engine())
                    .show(v!!.context, chatConfiguration);
            }
            R.id.rlInviteFriends -> {
                try {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/plain"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "DStar Runner")
                    var strShareMessage = "\nLet me recommend you this application\n\n"
                    strShareMessage = strShareMessage + "https://play.google.com/store/apps/details?id=" + mActivity!!.getPackageName()
                    strShareMessage =strShareMessage + "\nPlease use my referal code "+userPojo!!.referral_code+" to earn."
                    shareIntent.putExtra(Intent.EXTRA_TEXT, strShareMessage)
                    startActivity(Intent.createChooser(shareIntent, "Share via"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun handleResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    userPojo = response.data!!
                    App.sessionManager!!.setUserData(response.data)
                    App.sessionManager!!.setRanking(response.data.ranking)
                    App.sessionManager!!.setPoints(response.data.reward_points)
                    App.sessionManager!!.setWalletAMOUNT(response.data.wallet_amount)
                    setData(response.data!!)
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setData(data: Data) {
        binding!!.txtUserName.text = data.first_name+" "+data.last_name
        if(data.complete_trip.isNullOrBlank()||data.complete_trip.equals("0")){
            binding!!.txtCompletedTrip.text = "0"
        }else{
            binding!!.txtCompletedTrip.text = data.complete_trip
        }

        if(data.rating.isNullOrBlank()||data.rating.equals("0")){
            binding!!.txtRating.text = "0"
        }else{
            binding!!.txtRating.text = data.rating
        }

        if(data.cancelled_trip.isNullOrBlank()||data.cancelled_trip.equals("0")){
            binding!!.txtCancelledTrip.text = "0"
        }else{
            binding!!.txtCancelledTrip.text = data.cancelled_trip
        }

        Glide.with(mActivity!!).load(data.profile_pic).placeholder(R.drawable.default_user_pic).placeholder(R.drawable.default_user_pic).into(
            binding!!.imvUserProfile
        )
    }
}