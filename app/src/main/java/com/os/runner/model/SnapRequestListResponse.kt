package com.os.runner.model

import com.os.runner.utility.UtilityMethod
import java.io.Serializable

data class SnapRequestListResponse(
    val response: ResponseSnapRequestList
)

data class ResponseSnapRequestList(
    val `data`: List<DataSnapRequestList>,
    val message: String,
    val status: Int
)
data class DataSnapRequestList(
    var id: Int? =null,
    val user_id: Int? =null,
    val friend_id: Int? =null,
    val report_msg: String="",
    val status: String="",
    val created_at: String="",
    val country: String="",
    val updated_at: String="",
    val want_to_meet: String="",
    val dob: String="",
    val bio: String="",
    val snapchat_id: String="",
    val gender: String="",
    val age: String="",
    val mobile: String="",
    val name: String="",
    val image: String=""
):Serializable{
    fun createdDate():String{
        return UtilityMethod.parseRequestDateFormated(created_at).toString()
    }
}