package com.os.runner.ui.activity


import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toolbar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.App
import com.os.runner.utility.ChatConstants
import com.os.runner.utility.LocationManagerClient
import com.os.runner.utility.UiHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


abstract class BaseBindingActivity : AppCompatActivity(), LocationManagerClient.LocationListener {


    protected var mActivity: AppCompatActivity? = null

    protected var toolBar: Toolbar? = null

    protected var fragment: Fragment? = null

    protected abstract fun createContentView()

    protected abstract fun createActivityObject()

    protected abstract fun initializeObject()


    protected abstract fun setListeners()

    protected var mDatabase: DatabaseReference? = null

    protected var locationManagerClient: LocationManagerClient? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.isAppRunning = true
        createActivityObject()
        mDatabase = FirebaseDatabase.getInstance().reference
        createContentView()
        initializeObject()
        setListeners()
        locationManagerClient = LocationManagerClient.getInstance()
            .setIsFreshLocation(true)
            .init(applicationContext, this)
            .setIsRequireToUpdateLocation(true)
            .build()
        locationManagerClient!!.fetchCurrentLocation(this)

    }

    override fun onLocationAvailable(latLng: LatLng?) {

        Log.d(
            "onLocationAvailable",
            "latitude=" + latLng!!.latitude + " longitude=" + latLng!!.longitude
        )
        App.sessionManager!!.setLatitute(latLng!!.latitude.toString())
        App.sessionManager!!.setLongitute(latLng!!.longitude.toString())

        doAsync {
            uiThread {
                /*Update location on firebase*/
                if (App.sessionManager!!.isLogin()) {
                    val mUserDbReference =
                        mDatabase!!.child(ChatConstants.RUNNERS).child(App.sessionManager!!.getUserID())
                    val userMap = java.util.HashMap<String, String>()
                    userMap[ChatConstants.LATITUDE] = latLng?.latitude.toString()
                    userMap[ChatConstants.LONGITUDE] = latLng?.longitude.toString()
                    userMap[ChatConstants.RUNNER_ID] = App.sessionManager!!.getUserID()
                    mUserDbReference.setValue(userMap).addOnCompleteListener { task1 ->
                    }
                }
            }
        }
    }

    override fun onFail(status: LocationManagerClient.LocationListener.Status?) {

    }

    public fun showUpIconVisibility(isVisible: Boolean) {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(isVisible)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val view = currentFocus
        val ret = super.dispatchTouchEvent(event)
        if (view is EditText) {
            val w = currentFocus
            val location = IntArray(2)
            w!!.getLocationOnScreen(location)
            val x = event.rawX + w.left - location[0]
            val y = event.rawY + w.top - location[1]
            if (event.action == MotionEvent.ACTION_DOWN
                && (x < w.left || x >= w.right || y < w.top || y > w.bottom)
            ) {
                val imm =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(window.currentFocus!!.windowToken, 0)
            }
        }
        return ret
    }

    override fun onResume() {
        super.onResume()
        if (mActivity == null) throw NullPointerException("must create activty object")
        App.isAppInForeground = true
        //val filter = IntentFilter(NOTIFICATION_INTENT_ACTION)
        //LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter)
    }

    protected override fun onPause() {
        UiHelper.hideKeyboard(this)
        App.isAppInForeground = false
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        super.onPause()
    }

    fun changeFragment(fragment: Fragment, isAddToBack: Boolean) {
        this.fragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, fragment.javaClass.name)
        if (isAddToBack) transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }


    /*private val mReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            if (!App.sessionManager!!.isLogin()) return
            var extraData = intent.getStringExtra(FIELD_DATA)
            val transaction = supportFragmentManager.beginTransaction()
            val prev = supportFragmentManager.findFragmentByTag("dialog")
            if (prev != null) {
                transaction.remove(prev)
            }
            transaction.addToBackStack(null)
            val acceptOrderFragment = AcceptOrderFragment.getInstance()
            val bundle = Bundle()
            bundle.putString("extraData", extraData)
            acceptOrderFragment.arguments = bundle
            acceptOrderFragment.show(transaction, "dialog")
        }
    }*/

}

