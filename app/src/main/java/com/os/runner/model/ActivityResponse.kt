package com.os.runner.model

import java.io.Serializable

data class ActivityResponse(
    val `data`: List<DataActivity>,
    val message: String,
    val status: Int
)

data class DataActivity(
    val accept_time: String,
    val category_name: String,
    val completion_time: String,
    val created_at: String,
    val distance: String,
    val drop_info: List<DropInfo>,
    val is_rating: String,
    val item_detail: String,
    val pickup_info: PickupInfo,
    val rating: String,
    val request_id: String,
    val request_type: String,
    val runner_detail: RunnerDetail,
    val schedule_date_time: String,
    val service_name: String,
    val start_time: String,
    val status: String,
    val tip: String,
    val transport_category: String,
    val user_mobile: String,
    val user_name: String,
    val weight: String
)

data class DropInfoActivity(
    val address: String,
    val contact_number: String,
    val landmark: String,
    val latitude: String,
    val longitude: String,
    val name: String
)

data class PickupInfoActivity(
    val address: String,
    val contact_number: String,
    val landmark: String,
    val latitude: String,
    val longitude: String,
    val name: String
)

data class RunnerDetailActivity(
    val runner_id: String,
    val runner_image: String,
    val runner_mobile: String,
    val runner_name: String,
    val runner_vehicle: String
):Serializable