package com.os.runner.ui.activity.splash


import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.model.LatLng
import com.app.dstarrunner.R
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivitySplashBinding
import com.app.dstarrunner.databinding.ActivitySplashBindingImpl
import com.app.dstarrunner.databinding.SigninSignupLayoutBinding
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.dashboard.DashboardActivity
import com.os.runner.ui.activity.edit_profile.EditProfileActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpViewModel
import com.os.runner.ui.dialogs.AlertMessageDialog
import com.os.runner.utility.ChatConstants
import com.os.runner.utility.LocationManagerClient
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class SplashActivity : BaseBindingActivity(){

    private var binding: ActivitySplashBinding? = null

    override fun onLocationAvailable(latLng: LatLng?) {
        Log.d(
            "onLocationAvailable",
            "latitude=" + latLng!!.latitude + " longitude=" + latLng!!.longitude
        )
        App.sessionManager!!.setLatitute(latLng!!.latitude.toString())
        App.sessionManager!!.setLongitute(latLng!!.longitude.toString())
        if (App.sessionManager!!.isLogin()){
            val mUserDbReference =
                mDatabase!!.child(ChatConstants.RUNNERS).child(App.sessionManager!!.getUserID())
            val userMap = java.util.HashMap<String, String>()
            userMap[ChatConstants.LATITUDE] = latLng!!.latitude.toString()
            userMap[ChatConstants.LONGITUDE] = latLng!!.longitude.toString()
            userMap[ChatConstants.RUNNER_ID] = App.sessionManager!!.getUserID()
            mUserDbReference.setValue(userMap).addOnCompleteListener { task1 ->
            }
        }
    }

    override fun onFail(status: LocationManagerClient.LocationListener.Status?) {
        if (status == LocationManagerClient.LocationListener.Status.PERMISSION_DENIED) {
            AlertMessageDialog.showDialog(this,
                getString(R.string.need_permission),
                getString(R.string.app_need_location_permissin),
                getString(R.string.continue_),
                getString(R.string.cancel).toUpperCase(),
                object : AlertMessageDialog.AlertMessageCallback {
                    override fun onSuccessClick() {
                        locationManagerClient!!.checkLocationPermission()
                    }

                    override fun onCancelClick() {
                        finish()
                    }
                })
        } else if (status == LocationManagerClient.LocationListener.Status.GPS_DISABLED) {
            AlertMessageDialog.showDialog(this,
                getString(R.string.gps_seetings),
                getString(R.string.enable_gps),
                getString(R.string.ok).toUpperCase(),
                getString(R.string.no_thanks),
                object : AlertMessageDialog.AlertMessageCallback {
                    override fun onSuccessClick() {
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivity(intent)
                    }

                    override fun onCancelClick() {
                        finish()
                    }
                })
        }
    }

    override fun createContentView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        getKeyHash()
    }

    override fun setListeners() {
        Executors.newSingleThreadScheduledExecutor().schedule({
            goNext()
       }, 3, TimeUnit.SECONDS)
    }

    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        *//*locationManagerClient = LocationManagerClient.getInstance()
            .setIsFreshLocation(true)
            .init(applicationContext, this)
            .setIsRequireToUpdateLocation(false)
            .build()*//*
    }*/

    override fun onResume() {
        super.onResume()
        /*Executors.newSingleThreadScheduledExecutor().schedule({
            locationManagerClient!!.fetchCurrentLocation(this@SplashActivity)
        }, 3, TimeUnit.SECONDS)*/
    }

    private fun getKeyHash() {
        try {
            val info =
                packageManager.getPackageInfo("com.app.dstarrunner", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
    }

    private fun goNext() {
        Log.e("tag", "isLogin" + App.sessionManager!!.isLogin())
        if (App.sessionManager!!.isLogin()&&App.sessionManager!!.isApproved()) {
            DashboardActivity.startActivity(this, null, true)
        }else if(App.sessionManager!!.isLogin()&&!App.sessionManager!!.isApproved()){
            val bundle = Bundle()
            bundle.putSerializable("fromActivity", "splash")
            EditProfileActivity.startActivity(this, bundle,true)
            finish()
        }else{
            SignInSignUpActivity.startActivity(this, null, true)
        }
    }

   /* override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationManagerClient!!.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationManagerClient!!.onActivityResult(requestCode, resultCode, data)
    }*/
}

