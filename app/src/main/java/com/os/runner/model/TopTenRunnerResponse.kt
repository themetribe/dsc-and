package com.os.runner.model

data class TopTenRunnerResponse(
    val `data`: List<DataTopTenRunner>,
    val message: String,
    val status: Int
)

data class DataTopTenRunner(
    val cancelled_orders: String,
    val complete_orders: String,
    val current_vehicle: String,
    val id: String,
    val image: String,
    val is_favourite: String,
    val name: String,
    val rating: String,
    val sort: String,
    val view_status: String
)