package com.os.runner.ui.adapter;

import android.view.View;

import com.app.dstarrunner.R;

import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;
import tellh.com.stickyheaderview_rv.adapter.ViewBinder;

/**
 * Created by tlh on 2017/1/22 :)
 */

public class UserItemViewBinder extends ViewBinder<User, UserItemViewBinder.ViewHolder> {
    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(StickyHeaderViewAdapter adapter, ViewHolder holder, int position, User entity) {
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.item_user;
    }

    static class ViewHolder extends ViewBinder.ViewHolder {

        public ViewHolder(View rootView) {
            super(rootView);
        }

    }
}
