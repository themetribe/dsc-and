package com.os.runner.ui.activity.dashboard


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.App
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.chat.ChatActivity
import com.os.runner.ui.activity.request_detail.RequestDetailActivity
import com.os.runner.ui.dialogs.AlertMessageDialog
import com.os.runner.ui.fragment.account.AccountFragment
import com.os.runner.ui.fragment.activity.ActivityFragment
import com.os.runner.ui.fragment.home.HomeFragment
import com.os.runner.ui.fragment.transactions.TransactionsFragment
import com.os.runner.utility.ChatConstants
import com.os.runner.utility.Constants
import com.os.runner.utility.LocationManagerClient
import com.os.runner.utility.UiHelper


class DashboardActivity : BaseBindingActivity(), View.OnClickListener {

    private var mContext: Context? = null
    private var viewModel: DashboardViewModel? = null
    private var mLastClickTime: Long = 0
    private var mFragmentManager: FragmentManager? = null
    private var mFragmentTransaction: FragmentTransaction? = null
    private var nav_img: ImageView? = null
    private val TAG: String = DashboardActivity::class.java.getSimpleName()
    private var ibHome: ImageButton? = null
    private  var ibActivity:ImageButton? = null
    private  var ibOffers:ImageView? = null
    private  var ibNotification:ImageButton? = null
    private  var ibSettings:ImageButton? = null

    //Bottom Navtivation XML
    private var rlBottomTabsView: RelativeLayout? = null
    private var tvHome: TextView? = null
    private var tvActivity: TextView? = null
    private var tvBook: TextView? = null
    private var tvNotification: TextView? = null
    private var tvSettings: TextView? = null
    private var llHomeNavigation: LinearLayout? = null
    private  var llActivityNavigation:LinearLayout? = null
    private  var llNotificationNavigation:LinearLayout? = null
    private  var llSettingsNavigation:LinearLayout? = null

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)
        setContentView(R.layout.activity_dashboard)
    }

    override fun createActivityObject() {
        mActivity = this
    }


    override fun initializeObject() {
        initControl()
        fetchArgumentsData(intent)
    }


    private fun fetchArgumentsData(intent: Intent) {
        val bundle = intent.getBundleExtra("bundle")
        if (bundle != null) {
            val notificationType = bundle.getString("notification_type")
            if (notificationType != null) {
                if (notificationType.equals("chat")) {
                    /*{"notification_type":"chat","service_type":"","receiver_id":"520","request_id":"775","sender_id":"512"}*/
                    val b = Bundle()
                    b.putString("request_id", bundle.getString("request_id"))
                    b.putString("customer_id", bundle.getString("sender_id"))
                    ChatActivity.startActivity(mActivity!!, b, false)
                }else if(notificationType.equals("order")){
                    var b = Bundle()
                    b.putString("request_id",bundle.getString("request_id"))
                    b.putString("help_type",bundle.getString("service_type"))
                    RequestDetailActivity.startActivity(mActivity!!,b)
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null)
            fetchArgumentsData(intent)
    }

    override fun setListeners() {

    }

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
        Log.d(
            "onLocationAvailable",
            "latitude=" + latLng!!.latitude + " longitude=" + latLng!!.longitude
        )
        App.sessionManager!!.setLatitute(latLng!!.latitude.toString())
        App.sessionManager!!.setLongitute(latLng!!.longitude.toString())
        if (App.sessionManager!!.isLogin()){
            val mUserDbReference =
                mDatabase!!.child(ChatConstants.RUNNERS).child(App.sessionManager!!.getUserID())
            val userMap = java.util.HashMap<String, String>()
            userMap[ChatConstants.LATITUDE] = latLng!!.latitude.toString()
            userMap[ChatConstants.LONGITUDE] = latLng!!.longitude.toString()
            userMap[ChatConstants.RUNNER_ID] = App.sessionManager!!.getUserID()
            mUserDbReference.setValue(userMap).addOnCompleteListener { task1 ->
            }
        }
    }

    private fun locationUpdateCall(latitude: String?, longitude: String?) {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty(Constants.LATITUDE_NAME, latitude)
        jsonObject.addProperty(Constants.LONGITUDE_NAME, longitude)
        viewModel!!.update_location(jsonObject)
    }



    override fun onFail(status: LocationManagerClient.LocationListener.Status?) {
        if (status == LocationManagerClient.LocationListener.Status.PERMISSION_DENIED) {
            AlertMessageDialog.showDialog(this,
                getString(R.string.need_permission),
                getString(R.string.app_need_location_permissin),
                getString(R.string.continue_),
                getString(R.string.cancel).toUpperCase(),
                object : AlertMessageDialog.AlertMessageCallback {
                    override fun onSuccessClick() {
                        locationManagerClient!!.checkLocationPermission()
                    }

                    override fun onCancelClick() {
                        finish()
                    }
                })
        } else if (status == LocationManagerClient.LocationListener.Status.GPS_DISABLED) {
            AlertMessageDialog.showDialog(this,
                getString(R.string.gps_seetings),
                getString(R.string.enable_gps),
                getString(R.string.ok).toUpperCase(),
                getString(R.string.no_thanks),
                object : AlertMessageDialog.AlertMessageCallback {
                    override fun onSuccessClick() {
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivity(intent)
                    }

                    override fun onCancelClick() {
                        finish()
                    }
                })
        }
    }


    private fun initControl() {
        mContext = this
        rlBottomTabsView = findViewById<View>(R.id.rlBottomTabsView) as RelativeLayout
        tvHome = findViewById<View>(R.id.tvHome) as TextView
        tvActivity = findViewById<View>(R.id.tvActivity) as TextView
        //tvBook = findViewById<View>(R.id.tvBook) as TextView
        tvNotification = findViewById<View>(R.id.tvNotification) as TextView
        tvSettings = findViewById<View>(R.id.tvSettings) as TextView

        llHomeNavigation = findViewById<View>(R.id.llHomeNavigation) as LinearLayout
        llActivityNavigation = findViewById<View>(R.id.llActivityNavigation) as LinearLayout
        llNotificationNavigation = findViewById<View>(R.id.llNotificationNavigation) as LinearLayout
        llSettingsNavigation = findViewById<View>(R.id.llSettingsNavigation) as LinearLayout


        ibHome = findViewById<View>(R.id.ibHome) as ImageButton
        ibActivity = findViewById<View>(R.id.ibActivity) as ImageButton
        ibNotification = findViewById<View>(R.id.ibNotification) as ImageButton
        ibSettings = findViewById<View>(R.id.ibSettings) as ImageButton


        ibHome!!.setOnClickListener(this)
        ibActivity!!.setOnClickListener(this)
        ibNotification!!.setOnClickListener(this)
        ibSettings!!.setOnClickListener(this)


        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */
        mFragmentManager = supportFragmentManager
        mFragmentTransaction = mFragmentManager!!.beginTransaction()
        mFragmentTransaction!!.replace(R.id.containerView, HomeFragment()).commit()

        rlBottomTabsView!!.setBackgroundColor(getColor(R.color.app_bg_color))
        locationUpdateCall(App.sessionManager!!.getLatitute(),App.sessionManager!!.getLongitute())
    }

    override fun onResume() {
        super.onResume()
        Log.e("OnResume", "DashboardMainAct")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationManagerClient!!.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        UiHelper.hideKeyboard(this)
        locationManagerClient!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View?) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 100) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        when (v?.id) {
            R.id.ibHome->{
                manageTabs(0,true,false,false,false)
            }
            R.id.ibActivity->{
                manageTabs(1,false,true,false,false)
            }
            R.id.ibNotification->{
                manageTabs(2,false,false,true,false)
            }
            R.id.ibSettings->{
                manageTabs(3,false,false,false,true)
            }
        }
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?, isClear: Boolean) {
            val intent = Intent(activity, DashboardActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            if (isClear) intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.startActivity(intent)
        }
    }

    private fun manageTabs(
        position: Int,
        home: Boolean,
        activity: Boolean,
        notifications: Boolean,
        account: Boolean
    ) {
        try {
            when (position) {
                0 -> {
                    mFragmentManager = supportFragmentManager
                    mFragmentTransaction = mFragmentManager!!.beginTransaction()
                    mFragmentTransaction!!.replace(R.id.containerView,
                        HomeFragment()
                    ).commit()
                }
                1 -> {
                    mFragmentManager = supportFragmentManager
                    mFragmentTransaction = mFragmentManager!!.beginTransaction()
                    mFragmentTransaction!!.replace(R.id.containerView,
                        ActivityFragment()
                    ).commit()
                }
                2 -> {
                    mFragmentManager = supportFragmentManager
                    mFragmentTransaction = mFragmentManager!!.beginTransaction()
                    mFragmentTransaction!!.replace(R.id.containerView,
                        TransactionsFragment()
                    ).commit()
                }
                3 -> {
                    mFragmentManager = supportFragmentManager
                    mFragmentTransaction = mFragmentManager!!.beginTransaction()
                    mFragmentTransaction!!.replace(R.id.containerView,
                        AccountFragment()
                    ).commit()
                }
            }
            if (home) {
                tvHome!!.setTextColor(getColor(R.color.black))
                ibHome!!.setImageDrawable(getDrawable(R.drawable.homeactive))
                rlBottomTabsView!!.setBackgroundColor(getColor(R.color.app_bg_color))
            } else {
                tvHome!!.setTextColor(getColor(R.color.GrayColor))
                ibHome!!.setImageDrawable(getDrawable(R.drawable.home))
            }
            if (activity) {
                tvActivity!!.setTextColor(getColor(R.color.black))
                ibActivity!!.setImageDrawable(getDrawable(R.drawable.ordersactive))
                rlBottomTabsView!!.setBackgroundColor(getColor(R.color.white))
            } else {
                tvActivity!!.setTextColor(getColor(R.color.GrayColor))
                ibActivity!!.setImageDrawable(getDrawable(R.drawable.orders))
            }
            if (notifications) {
                tvNotification!!.setTextColor(getColor(R.color.black))
                ibNotification!!.setImageDrawable(getDrawable(R.drawable.transactionselect))
                rlBottomTabsView!!.setBackgroundColor(getColor(R.color.white))
            } else {
                tvNotification!!.setTextColor(getColor(R.color.GrayColor))
                ibNotification!!.setImageDrawable(getDrawable(R.drawable.transactionunselect))
            }
            if (account) {
                tvSettings!!.setTextColor(getColor(R.color.black))
                ibSettings!!.setImageDrawable(getDrawable(R.drawable.settingsselect))
                rlBottomTabsView!!.setBackgroundColor(getColor(R.color.white))
            } else {
                tvSettings!!.setTextColor(getColor(R.color.GrayColor))
                ibSettings!!.setImageDrawable(getDrawable(R.drawable.settings))
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

}