package com.os.runner.model

data class CallResponse(
    val `data`: DataCall,
    val message: String,
    val status: Int
)

data class DataCall(
    val receiver_no: String
)