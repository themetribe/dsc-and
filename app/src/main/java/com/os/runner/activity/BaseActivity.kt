package com.os.runner.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.os.runner.utility.UiHelper

open class BaseActivity : AppCompatActivity() {

    open val RC_SIGN_IN = 9001
    var currentActivity: Context? = null

    companion object{
        var isBackground = false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentActivity = this
    }
    protected override fun onResume() {
        super.onResume()
        isBackground = false
        currentActivity = this
    }

    protected override fun onPause() {
        super.onPause()
        UiHelper.hideKeyboard(this)
        isBackground = true
    }
}