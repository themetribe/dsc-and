package com.os.runner.ui.activity.request_detail

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.gson.JsonObject
import com.os.drunnercustomer.ui.activity.request_detail.RequestDetailViewModel
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityHelpMeBuysendRequestDetailsBinding
import com.os.runner.model.*
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.chat.ChatActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.DropInfoDetailAdapter
import com.os.runner.ui.dialogs.AlertMessageDialog
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.ChatConstants
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.activity_help_me_buysend_request_details.*
import kotlin.collections.ArrayList

class RequestDetailActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: RequestDetailViewModel? = null
    private var binding: ActivityHelpMeBuysendRequestDetailsBinding? = null

    /*wallet history Adapter*/
    private var dropInfoRequestDetailList: ArrayList<DropInfoRequestDetail>? = ArrayList()
    private var dropInfoDetailAdapter: DropInfoDetailAdapter? = null
    private var onClickListener: View.OnClickListener? = null
    private var request_id = ""
    private var help_type = ""
    private var runner_latitude: Double = 0.0
    private var runner_longitude: Double = 0.0
    private var requestDetailResponse: RequestDetailResponse? = null

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(RequestDetailViewModel::class.java)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_help_me_buysend_request_details
        )
        onClickListener = this
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        getIntentData()
        initControls()
    }

    override fun onLocationAvailable(latLng: LatLng?) {
        Log.d(
            "onLocationAvailable",
            "latitude=" + latLng!!.latitude + " longitude=" + latLng!!.longitude
        )
        App.sessionManager!!.setLatitute(latLng!!.latitude.toString())
        App.sessionManager!!.setLongitute(latLng!!.longitude.toString())
        if (App.sessionManager!!.isLogin()){
            val mUserDbReference =
                mDatabase!!.child(ChatConstants.RUNNERS).child(App.sessionManager!!.getUserID())
            val userMap = java.util.HashMap<String, String>()
            userMap[ChatConstants.LATITUDE] = latLng!!.latitude.toString()
            userMap[ChatConstants.LONGITUDE] = latLng!!.longitude.toString()
            userMap[ChatConstants.RUNNER_ID] = App.sessionManager!!.getUserID()
            mUserDbReference.setValue(userMap).addOnCompleteListener { task1 ->
            }
        }
    }


    private fun getIntentData() {
        if (intent.extras != null) {
            request_id = intent.extras!!.getBundle("bundle")!!.getString("request_id") as String
            help_type = intent.extras!!.getBundle("bundle")!!.getString("help_type") as String
        }
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })

        viewModel!!.responseFavouriteData.observe(this, Observer {
            handleFavouriteResult(it)
        })

        viewModel!!.responseStartJobData.observe(this, Observer {
            handleStartJobResult(it)
        })

        viewModel!!.responseTwilioTokenData.observe(this, Observer {
            handleCallResult(it)
        })
    }

    private fun initControls() {

        if (help_type.equals("Help Me Buy")) {
            binding!!.txtPickupName.text = "Buying Location"
            binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.help_me_buy)
        } else if (help_type.equals("Help Me Send")) {
            binding!!.txtPickupName.text = "Pick up Location"
            binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.help_me_send)
        } else {
            binding!!.llSendBuy.visibility = View.GONE
            binding!!.llPickupQueueView.visibility = View.GONE
            binding!!.llQueue.visibility = View.VISIBLE
            binding!!.toolbarLayout.toolbarTitle.text = getString(R.string.help_me_queue)
            binding!!.txtPickupName.text = "Queue Location"
        }

        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.toolbarLayout.txtCancelRequest.setOnClickListener(this)
        binding!!.imvCall.setOnClickListener(this)
        binding!!.imvChat.setOnClickListener(this)
        binding!!.btnReachedAtLocation.setOnClickListener(this)
        binding!!.rlPickLocationMapView.setOnClickListener(this)

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding!!.recyclerView.layoutManager = mLayoutManager

        dropInfoDetailAdapter = DropInfoDetailAdapter(
            mActivity!!,
            dropInfoRequestDetailList!!,
            onClickListener
        )
        binding!!.recyclerView.adapter = dropInfoDetailAdapter
        request_detailCall()
    }

    private fun runnerPhoneCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        /*sender_no
        receiver_no*/
        jsonObject.addProperty("sender_no", "+65"+requestDetailResponse!!.data.runner_detail.runner_mobile)
        jsonObject.addProperty("receiver_no", "+65"+requestDetailResponse!!.data.user_mobile)
        jsonObject.addProperty("receiver_name", requestDetailResponse!!.data.user_name)
        jsonObject.addProperty("sender_name", requestDetailResponse!!.data.runner_detail.runner_name)
        viewModel!!.twilio_token(jsonObject)
    }

    private fun request_detailCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty("request_id", request_id)
        viewModel!!.runner_request_detail(jsonObject)
    }


    private fun startStopJobCall(status: String) {
        /*{
            "runner_id" : "564",
            "request_id" : "131",
            "status" : "at_location"
        }*/
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.RUNNER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty("request_id", request_id)
        jsonObject.addProperty("status", status)
        viewModel!!.start_job(jsonObject)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(205)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.imvCall -> {
                /*val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH)
                val tz = TimeZone.getTimeZone("Asia/Singapore")
                sdf.timeZone = tz

                val date = Date()
                val local = Timestamp(date.time)
                val strDate = sdf.format(date)
                Log.e("Local in String format:", strDate)*/
                checkPermission()
            }
            R.id.txtCancelRequest -> {
                AlertMessageDialog.showDialog(mActivity!!,
                    getString(R.string.cancel_request),
                    "Are you sure, you want to cancel this request?",
                    getString(R.string.yes),
                    getString(R.string.no),
                    object : AlertMessageDialog.AlertMessageCallback {
                        override fun onSuccessClick() {
                            var jsonObject = JsonObject()
                            jsonObject.addProperty(
                                Constants.RUNNER_ID,
                                App.sessionManager!!.getUserID()
                            )
                            jsonObject.addProperty("request_id", request_id)
                            viewModel!!.runner_cancel_request(jsonObject)
                        }

                        override fun onCancelClick() {

                        }
                    })
            }
            R.id.btnReachedAtLocation -> {
                var status = btnReachedAtLocation.text.toString().trim()
                if (status.equals("Reached at location")) {
                    status = "at_location"
                } else if (status.equals("Start job")) {
                    status = "start_job"
                } else if (status.equals("Complete job")) {
                    status = "completed"
                }

                if (status.equals("at_location") || status.equals("start_job")) {
                    /*if(requestDetailResponse!!.data.request_type.equals("now")){
                        startStopJobCall(status)
                    }else{
                        val c = Calendar.getInstance()
                        val dateFormat = SimpleDateFormat(
                            "dd/MM/yyyy hh:mm:ss a"
                        )
                        val date = dateFormat.format(c.getTime())
                        val dateafter = requestDetailResponse!!.data.schedule_date_time

                        var convertedDate: Date? = Date()
                        var convertedDate2 = Date()
                        try {
                            convertedDate = dateFormat.parse(date)
                            convertedDate2 = dateFormat.parse(dateafter)
                            if (convertedDate2.after(convertedDate)) {
                                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.allowed_job_msg))
                            } else {
                                startStopJobCall(status)
                            }
                        } catch (e: ParseException) {
                            // TODO Auto-generated catch block
                            e.printStackTrace()
                        }
                    }*/
                    startStopJobCall(status)
                } else {
                    var dropStatus: Boolean = false
                    for (i in 0 until requestDetailResponse!!.data.drop_info.size) {
                        if (requestDetailResponse!!.data.drop_info[i].status.equals("pending")) {
                            dropStatus = true
                        }
                    }
                    if (requestDetailResponse!!.data.pickup_info.status.equals("pending") || dropStatus) {
                        var bundle = Bundle()
                        bundle.putString("request_id", request_id)
                        ChangeRequestStatusActivity.startActivity(mActivity!!, bundle)
                    } else {
                        startStopJobCall(status)
                    }
                }

            }
            R.id.rlPickLocationMapView -> {
                var pickupLat = requestDetailResponse!!.data.pickup_info.latitude
                var pickupLong = requestDetailResponse!!.data.pickup_info.longitude

                val uri =
                    "http://maps.google.com/maps?daddr=" + runner_latitude + "," + runner_longitude + "+to:" + pickupLat + "," + pickupLong
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.setClassName(
                    "com.google.android.apps.maps",
                    "com.google.android.maps.MapsActivity"
                )
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent)
                }
            }
            R.id.rlLocationView -> {
                val position = v.tag as Int
                var dropoffLat = dropInfoRequestDetailList!![position].latitude
                var dropoffLong = dropInfoRequestDetailList!![position].longitude

                val uri =
                    "http://maps.google.com/maps?daddr=" + runner_latitude + "," + runner_longitude + "+to:" + dropoffLat + "," + dropoffLong
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.setClassName(
                    "com.google.android.apps.maps",
                    "com.google.android.maps.MapsActivity"
                )
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent)
                }
            }
            R.id.imvChat -> {
                val bundle = Bundle()
                bundle.putString("request_id", request_id)
                bundle.putString("customer_id", requestDetailResponse!!.data.user_id)
                ChatActivity.startActivity(mActivity!!, bundle, false)
            }
            R.id.txtCancelRequest -> {
                /*var bundle = Bundle()
                bundle.putString("request_id", request_id)
                ChangeRequestStatusActivity.startActivity(mActivity!!,bundle)*/
            }
        }
    }

    fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.CALL_PHONE
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42
                )
            }
        } else {
            // Permission has already been granted
            //callPhone()
            runnerPhoneCall()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                //callPhone()
                runnerPhoneCall()
            } else {
                // permission denied, boo! Disable the
                // functionality
            }
            return
        }
    }

    fun callPhone(receiverNo: String) {
        val intent = Intent(
            Intent.ACTION_CALL,
            Uri.parse("tel:" + receiverNo)
        )
        startActivity(intent)
    }


    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, RequestDetailActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent, 205)
        }
    }

    private fun handleStartJobResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    request_detailCall()
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }


    private fun handleFavouriteResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    request_detailCall()
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun handleCallResult(result: ApiResponse<CallResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    callPhone(response.data.receiver_no)
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun handleResult(result: ApiResponse<RequestDetailResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    requestDetailResponse = response
                    binding!!.nestedScrollView.visibility = View.VISIBLE
                    setData(response.data)
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setData(data: DataRequestDetail) {


        mDatabase!!.child(ChatConstants.RUNNERS).child(App.sessionManager!!.getUserID())
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.hasChild(ChatConstants.LATITUDE) && dataSnapshot.hasChild(
                            ChatConstants.LONGITUDE
                        )
                    ) {
                        runner_latitude =
                            dataSnapshot.child(ChatConstants.LATITUDE)
                                .getValue(String::class.java)!!.toDouble()
                        runner_longitude =
                            dataSnapshot.child(ChatConstants.LONGITUDE)
                                .getValue(String::class.java)!!.toDouble()

                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })

        if(requestDetailResponse!!.data.status.equals("Cancelled")){
            binding!!.toolbarLayout.txtCancelRequest.setText("Cancelled")
            binding!!.toolbarLayout.txtCancelRequest.isEnabled = false
        }

        if (requestDetailResponse!!.data.status.equals("Pending") || requestDetailResponse!!.data.status.equals(
                "Completed"
            ) || requestDetailResponse!!.data.status.equals("Cancelled")) {
            binding!!.llCallChat.visibility = View.GONE
            binding!!.btnReachedAtLocation.visibility = View.GONE
        } else {
            binding!!.llCallChat.visibility = View.VISIBLE
            binding!!.btnReachedAtLocation.visibility = View.VISIBLE
        }
        if (requestDetailResponse!!.data.status.equals("Accepted") && requestDetailResponse!!.data.at_job_location.equals(
                "no"
            )) {
            binding!!.toolbarLayout.txtCancelRequest.visibility = View.VISIBLE
        } else {
            binding!!.toolbarLayout.txtCancelRequest.visibility = View.GONE
        }

        if (requestDetailResponse!!.data.at_job_location.equals("yes") && requestDetailResponse!!.data.start_job.equals(
                "no"
            )) {
            binding!!.btnReachedAtLocation.text = "Start job"
        } else if (requestDetailResponse!!.data.at_job_location.equals("yes") && requestDetailResponse!!.data.start_job.equals(
                "yes"
            )) {
            binding!!.btnReachedAtLocation.text = "Complete job"
        }

        binding!!.rlRunnerView.visibility = View.VISIBLE
        binding!!.txtCustomerName.text = data.user_name
        binding!!.txtBookingId.text = "Booking ID : " + data.booking_id
        Glide.with(this).load(data.user_image).placeholder(R.drawable.default_user_pic).into(binding!!.imvRunnerProfile)

        binding!!.txtCreatedDate.text = UtilityMethod.parseDateFormated(data.schedule_date_time)
        if (!help_type.equals("Help Me Queue")) {
            binding!!.txtCategoryName.text = data.category_name
            binding!!.txtWeight.text = data.weight + " kg"
            binding!!.txtItemDetail.text = data.item_detail

        } else {
            binding!!.txtQueueTime.text = data.duration + " Minutes"
            binding!!.txtPlaceName.text = data.pickup_info.name
            binding!!.txtQueueDetail.text = data.item_detail
        }

        binding!!.txtMultistop.text = getString(R.string.singapur_doller) + " " + data.multistop_charge
        binding!!.txtWaitingCharge.text = getString(R.string.singapur_doller) + " " + data.waiting_charges
        binding!!.txtTotal.text = getString(R.string.singapur_doller) + " " + data.paid_amount
        binding!!.txtFairAmount.text = getString(R.string.singapur_doller) + " " + data.amount
        binding!!.txtDiscountAmount.text = "-"+getString(R.string.singapur_doller) + " " + data.offer_amount

        binding!!.txtName.text = data.pickup_info.name
        binding!!.txtMobile.text = data.pickup_info.contact_number
        binding!!.txtLandmark.text = data.pickup_info.landmark
        binding!!.txtLocation.text = data.pickup_info.address
        if(binding!!.txtPickupName.text.toString().trim().equals("Queue Location")&&data.status.equals("Completed")){
            binding!!.txtPickupStatus.setText("Finished")
        }else  if(binding!!.txtPickupName.text.toString().trim().equals("Pick up Location")||binding!!.txtPickupName.text.toString().trim().equals("Buying Location")&&data.status.equals("Completed")){
            binding!!.txtPickupStatus.setText("Completed")
        }else{
            binding!!.txtPickupStatus.setText("Pending")
        }
        //binding!!.txtPickupStatus.text = data.pickup_info.status

        /* drop off location info*/
        if (!help_type.equals("Help Me Queue")) {
            setAdapters(data.drop_info)
        }

    }

    private fun setAdapters(dropInfo: List<DropInfoRequestDetail>) {
        if (dropInfo != null) {
            dropInfoRequestDetailList!!.clear()
            dropInfoRequestDetailList!!.addAll(dropInfo)
            dropInfoDetailAdapter!!.notifyDataSetChanged()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==111){
            request_detailCall()
        }
    }

}
