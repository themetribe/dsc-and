package com.os.runner.ui.activity.pending_requests

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityPendingRequestBinding
import com.os.runner.model.DataRequest
import com.os.runner.model.DefaultPojo
import com.os.runner.model.RequestResponse
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.request_detail.RequestDetailActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.PendingRequestsAdapter
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.ChatConstants
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_view.view.*
import kotlin.collections.ArrayList

class PendingRequestsActivity : BaseBindingActivity(), View.OnClickListener {


    private var binding: ActivityPendingRequestBinding? = null
    private var viewModel: PendingRequestsViewModel? = null
    /*Pending Requests Adapter*/
    private var dataRequestList: ArrayList<DataRequest>? = ArrayList()
    private var pendingRequestsAdapter: PendingRequestsAdapter? = null
    private var onClickListener: View.OnClickListener? = null


    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(PendingRequestsViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pending_request)
        onClickListener= this
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, PendingRequestsActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    override fun setListeners() {


        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })

        viewModel!!.responseAcceptRejectData.observe(this, Observer {
            handleAcceptRejectResult(it)
        })
    }

    private fun initControl() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.pending))
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding!!.recyclerView.setLayoutManager(mLayoutManager)

        pendingRequestsAdapter = PendingRequestsAdapter(this, dataRequestList!!, onClickListener)
        binding!!.recyclerView.adapter = pendingRequestsAdapter
        requestsCall()
    }


    private fun requestsCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.RUNNER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty("type", "current")
        viewModel!!.requests_for_runner(jsonObject)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.btnAccept -> {
                val position = v.tag as Int
                acceptDeclineRequestCall(dataRequestList!![position].request_id, "accept")
            }
            R.id.btnReject -> {
                val position = v.tag as Int
                acceptDeclineRequestCall(dataRequestList!![position].request_id, "reject")
            }
            R.id.cvInProgress->{
                val position = v.tag as Int
                var bundle = Bundle()
                bundle.putString("request_id",dataRequestList!![position].request_id)
                bundle.putString("help_type",dataRequestList!![position].service_name)
                RequestDetailActivity.startActivity(mActivity!!,bundle)
            }
        }
    }

    private fun acceptDeclineRequestCall(requestId: String, type: String) {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.RUNNER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty(Constants.REQUEST_ID, requestId)
        if(type.equals("accept")){
            viewModel!!.accept_request(jsonObject)
        }else{
            viewModel!!.decline_request(jsonObject)
        }
    }

    private fun handleResult(result: ApiResponse<RequestResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if (response.data.size == 0) {
                        binding!!.txtNoDataFound.visibility = View.VISIBLE
                        binding!!.txtNoDataFound.txtMessage.text = getString(R.string.you_currently_have_no_pending_job_request)
                        binding!!.recyclerView.visibility = View.GONE
                    } else {
                        binding!!.txtNoDataFound.visibility = View.GONE
                        binding!!.recyclerView.visibility = View.VISIBLE
                        setAdapters(response.data)
                    }
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setAdapters(data: List<DataRequest>) {
        if(data!=null){
            dataRequestList!!.clear()
            dataRequestList!!.addAll(data)
            pendingRequestsAdapter!!.notifyDataSetChanged()
        }
    }

    private fun handleAcceptRejectResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if(response.message.equals("Please complete your current task before accepting a new task.")){
                        UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                    }else{
                        UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    }
                    requestsCall()
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    override fun onLocationAvailable(latLng: LatLng?) {
        Log.d(
            "onLocationAvailable",
            "latitude=" + latLng!!.latitude + " longitude=" + latLng!!.longitude
        )
        App.sessionManager!!.setLatitute(latLng!!.latitude.toString())
        App.sessionManager!!.setLongitute(latLng!!.longitude.toString())
        if (App.sessionManager!!.isLogin()){
            val mUserDbReference =
                mDatabase!!.child(ChatConstants.RUNNERS).child(App.sessionManager!!.getUserID())
            val userMap = java.util.HashMap<String, String>()
            userMap[ChatConstants.LATITUDE] = latLng!!.latitude.toString()
            userMap[ChatConstants.LONGITUDE] = latLng!!.longitude.toString()
            userMap[ChatConstants.RUNNER_ID] = App.sessionManager!!.getUserID()
            mUserDbReference.setValue(userMap).addOnCompleteListener { task1 ->
            }
        }
    }

}