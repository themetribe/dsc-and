package com.os.runner.ui.fragment.upcoming

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.FragmentInprogressBinding
import com.os.runner.model.DataRequest
import com.os.runner.model.RequestResponse
import com.os.runner.ui.activity.request_detail.RequestDetailActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.ActivityListAdapter
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.ui.fragment.BaseFragment
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.no_data_view.view.*

class UpcomingFragment : BaseFragment(),View.OnClickListener {

    private var binding: FragmentInprogressBinding? = null
    var viewModel: UpcomingViewModel? = null

    /*Activity Adapter*/
    private var dataActivityList: ArrayList<DataRequest>? = ArrayList()
    private var activityListAdapter: ActivityListAdapter? = null
    private var onClickListener: View.OnClickListener? = null

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?): ViewDataBinding {
        viewModel = ViewModelProvider(this).get(UpcomingViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_inprogress, container, false)
        binding!!.lifecycleOwner = this
        onClickListener =this
        return binding!!
    }

    override fun createActivityObject() {
        mActivity = activity
    }

    override fun initializeObject() {
        initControl()
    }

    private fun initControl() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerView.setLayoutManager(layoutManager)

        activityListAdapter = ActivityListAdapter(mActivity!!, dataActivityList!!, onClickListener)
        binding!!.recyclerView.adapter = activityListAdapter
    }

    override fun onResume() {
        super.onResume()
        inProgressApiCall()
    }

    private fun inProgressApiCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.RUNNER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty("type", "upcoming")
        viewModel!!.requests_for_runner(jsonObject)

    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, androidx.lifecycle.Observer {
            handleResult(it)
        })
    }


    private fun handleResult(result: ApiResponse<RequestResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if (response.data.size==0) {
                        binding!!.txtNoDataFound.visibility = View.VISIBLE
                        binding!!.txtNoDataFound.txtMessage.text = "You currently have no activity."
                        binding!!.recyclerView.visibility = View.GONE
                    } else {
                        binding!!.txtNoDataFound.visibility = View.GONE
                        binding!!.recyclerView.visibility = View.VISIBLE
                        setAdapters(response.data!!)

                    }

                } else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                }else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setAdapters(data: List<DataRequest>) {
        if (data != null) {
            dataActivityList!!.clear()
            dataActivityList!!.addAll(data)
            activityListAdapter!!.notifyDataSetChanged()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtHelpMeQueueCancel->{
               /* val position = v.tag as Int
                AlertMessageDialog.showDialog(mActivity!!,
                    getString(R.string.cancel_request),
                    getString(R.string.are_you_sure_you_want_to_cancel_this_request),
                    getString(R.string.yes),
                    getString(R.string.no),
                    object : AlertMessageDialog.AlertMessageCallback {
                        override fun onSuccessClick() {
                            var bundle = Bundle()
                            bundle.putString("request_id",dataActivityList!![position].request_id)
                            CancelRequestListActivity.startActivity(mActivity!!,bundle)
                        }

                        override fun onCancelClick() {

                        }
                    })*/
            }
            R.id.txtHelpMeSendBuyCancel->{
               /* val position = v.tag as Int
                AlertMessageDialog.showDialog(mActivity!!,
                    getString(R.string.cancel_request),
                    getString(R.string.are_you_sure_you_want_to_cancel_this_request),
                    getString(R.string.yes),
                    getString(R.string.no),
                    object : AlertMessageDialog.AlertMessageCallback {
                        override fun onSuccessClick() {
                            var bundle = Bundle()
                            bundle.putString("request_id",dataActivityList!![position].request_id)
                            CancelRequestListActivity.startActivity(mActivity!!,bundle)
                        }

                        override fun onCancelClick() {

                        }
                    })*/
            }
            R.id.cvInProgress->{
                val position = v.tag as Int
                var bundle = Bundle()
                bundle.putString("request_id",dataActivityList!![position].request_id)
                bundle.putString("help_type",dataActivityList!![position].service_name)
                RequestDetailActivity.startActivity(mActivity!!,bundle)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode==205&&requestCode==205){
            inProgressApiCall()
        }
    }
}