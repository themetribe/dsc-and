package com.os.runner.model

data class DefaultPojo(
    val message: String,
    val status: Int
)