package com.os.runner.ui.activity.chat

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import com.os.runner.model.UploadImageResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ChatsListViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<UploadImageResponse>>()
    var responseSendMessageData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiResponse: ApiResponse<UploadImageResponse>? = null
    var apiSendMessageResponse: ApiResponse<DefaultPojo>? = null


    private var subscription: Disposable? = null

    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiSendMessageResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun upload_image(
        image: File?,
        mRequestId: String
    ) {
        var body: MultipartBody.Part? = null
        if (image != null) {
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image)
            body = MultipartBody.Part.createFormData("image", image.name, requestFile)
        }
        val media = MediaType.parse("multipart/form-data")
        subscription = App.apiService!!.upload_image(body, RequestBody.create(media, mRequestId))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }


    fun sent_msg_notif(jsonObject: JsonObject) {
        subscription = App.apiService!!.sent_msg_notif(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSendMessageData.postValue(apiSendMessageResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseSendMessageData.postValue(apiSendMessageResponse!!.success(result))
                },
                { throwable ->
                    responseSendMessageData.postValue(apiSendMessageResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}