package com.os.runner.utility

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.text.SpannableString
import android.text.format.DateFormat
import android.text.style.UnderlineSpan
import android.view.View
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.app.dstarrunner.R
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

object UtilityMethod {


    object LocationConstants {
        @kotlin.jvm.JvmField
        val SUCCESS_RESULT = 0
        @kotlin.jvm.JvmField
        val FAILURE_RESULT = 1
        @kotlin.jvm.JvmField
        val PACKAGE_NAME = "com.os.diruba"
        @kotlin.jvm.JvmField
        var RECEIVER = "$PACKAGE_NAME.RECEIVER"
        @kotlin.jvm.JvmField
        val RESULT_DATA_KEY = "$PACKAGE_NAME.RESULT_DATA_KEY"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_EXTRA = "$PACKAGE_NAME.LOCATION_DATA_EXTRA"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_LATITUDE = "$PACKAGE_NAME.LOCATION_DATA_LATITUDE"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_LONGITUDE = "$PACKAGE_NAME.LOCATION_DATA_LONGITUDE"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_ADDRESS1 = "$PACKAGE_NAME.LOCATION_DATA_ADDRESS1"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_ADDRESS2 = "$PACKAGE_NAME.LOCATION_DATA_ADDRESS2"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_AREA = "$PACKAGE_NAME.LOCATION_DATA_AREA"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_CITY = "$PACKAGE_NAME.LOCATION_DATA_CITY"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_STATE = "$PACKAGE_NAME.LOCATION_DATA_STATE"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_COUNTRY = "$PACKAGE_NAME.LOCATION_DATA_COUNTRY"
        @kotlin.jvm.JvmField
        val LOCATION_DATA_POSTAL = "$PACKAGE_NAME.LOCATION_DATA_POSTAL"
    }

    fun hasInternet(context: Context): Boolean {
        try {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnectedOrConnecting
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }
    fun isValidPassword(password: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Za-z]).{6,20}$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }





    fun getTime(time: Long): String {

        val timeStamp = java.sql.Timestamp(time)
        val date: Date = timeStamp
        return DateFormat.format("hh:mm aa", date).toString()
    }

    fun getDate(time: Long): String {

        val timeStamp = java.sql.Timestamp(time)
        val date: Date = timeStamp
        return DateFormat.format("dd-MM-yyyy", date).toString()
    }

    fun getFormattedDate(context: Context?, smsTimeInMilis: Long): String? {
        val smsTime: Calendar = Calendar.getInstance()
        smsTime.setTimeInMillis(smsTimeInMilis)
        val now: Calendar = Calendar.getInstance()
        val timeFormatString = "h:mm aa"
        val dateTimeFormatString = "EEEE, MMMM d, h:mm aa"
        val HOURS = 60 * 60 * 60.toLong()
        return if (now.get(Calendar.DATE) === smsTime.get(Calendar.DATE)) {
            context?.getString(R.string.today)
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) === 1) {
            context?.getString(R.string.yesterday)
        } else if (now.get(Calendar.YEAR) === smsTime.get(Calendar.YEAR)) {
            DateFormat.format(dateTimeFormatString, smsTime).toString()
        } else {
            DateFormat.format("MMMM dd yyyy", smsTime).toString()
        }
    }





    fun parseRequestDateFormated(time: String?): String? {
        //val inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val outputPattern = "dd MMM"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }
    //16/11/2020 06:47:32 PM
    fun parseDateFormated(time: String?): String? {
        val inputPattern = "dd/MM/yyyy hh:mm:ss a"
        val outputPattern = "MMMM, dd yyyy hh:mm a"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    @Throws(ParseException::class)
    fun convertDateTimeToMilliSeconds(
        dateTime: String?,
        comingFormat: String?
    ): Long {
        var timeInSec: Long = 0
        try {
            val sdf = SimpleDateFormat(comingFormat)
            sdf.timeZone = TimeZone.getDefault()
            timeInSec = sdf.parse(dateTime).time
            return timeInSec
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return 0
    }

    fun getAddressFromLatLong(latitude: Double, longitude: Double, context: Context): String {
        var address = ""
        try {
            val addresses: List<Address>
            val geoCoder = Geocoder(context, Locale.getDefault())
            addresses = geoCoder.getFromLocation(latitude, longitude, 1)
            address = addresses[0].getAddressLine(0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return address
    }

    fun underlineTextView(mTextView: TextView) {
        val content = SpannableString(mTextView.text.toString())
        content.setSpan(UnderlineSpan(), 0, mTextView.text.toString().length, 0)
        mTextView.text = content
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(target).matches();
    }

    fun setZeroBeforeNine(digit: Int): String {
        return if (digit <= 9) "0$digit" else "" + digit
    }

    fun convertnewDateFormat(date: Date, format: String): String {
        var formattedDate = ""
        try {
            val outputFormat = SimpleDateFormat(format)
            formattedDate = outputFormat.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return formattedDate
    }
//    fun showMessageDialog(activity: Activity, message: String) {
//        MaterialDialog.Builder(activity).message(message).negativeText(activity.getString(R.string.ok))
//            .show()
//    }

    fun showToastMessageDefault(activity: Activity, message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }


    fun showSnackBar(view: View, message: String, hasSuccess: Boolean) {
        val snackbar =
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction(
                view.context.getString(R.string.ok),
                null
            )
        snackbar.setActionTextColor(Color.BLUE)
        val snackbarView = snackbar.view
        if (hasSuccess)
            snackbarView.setBackgroundColor(ContextCompat.getColor(view.context, R.color.green_700))
        else
            snackbarView.setBackgroundColor(ContextCompat.getColor(view.context, R.color.red_600))
        val tv = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        tv.setTextColor(ContextCompat.getColor(view.context, R.color.white))
        snackbar.show()
    }

    fun isNumeric(str: String): Boolean {
        return str.matches(Regex("-?\\d+(.\\d+)?"))
    }

    fun readJSONFromAsset(context: Context, fileName: String): String? {

        var json: String? = null
        try {
            val `is` = context.assets.open(fileName)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }





    fun hideKeyboard(activity: Activity) {
        var imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    fun slideUp(view: View) {

        view.visibility = View.VISIBLE
        val animate = TranslateAnimation(
            0f, // fromXDelta
            0f, // toXDelta
            2000f, // fromYDelta
            0f // toYDelta
        )
        animate.duration = 500
//        animate.fillAfter = true
        view.startAnimation(animate)
    }

    fun slideDown(view: View) {
        view.visibility = View.GONE
        val animate = TranslateAnimation(
            0f, // fromXDelta
            0f, // toXDelta
            0f, // fromYDelta
            2000f // toYDelta
        )
        animate.duration = 500
//        animate.fillAfter = true
        view.startAnimation(animate)
    }
}