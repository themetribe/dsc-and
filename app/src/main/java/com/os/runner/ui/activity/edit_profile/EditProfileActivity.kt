package com.os.runner.ui.activity.edit_profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityEditProfileBinding
import com.os.runner.model.Data
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.dashboard.DashboardActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_view.view.*
import java.io.File


class EditProfileActivity : BaseBindingActivity(), View.OnClickListener {

    private var profileFile: File? = null
    private var nricFrontImageFile: File? = null
    private var nricBackImageFile: File? = null
    private var viewModel: EditProfileViewModel? = null
    private var binding: ActivityEditProfileBinding? = null
    private var fromActivity: String? = null
    private var userPojo:Data?=null

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
    }


    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?, isClear: Boolean) {
            val intent = Intent(activity, EditProfileActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            if (isClear) intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.startActivity(intent)
        }
    }
    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        getIntentData()
        initControls()
    }

    private fun getIntentData() {
        if (intent.extras != null) {
            fromActivity = intent.extras!!.getBundle("bundle")!!.getString("fromActivity") as String
        }
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })

        viewModel!!.responseProfileLiveData.observe(this, Observer {
            handleProfileResult(it)
        })
    }

    private fun initControls() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.profile))
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        if (fromActivity.equals("splash")){
            binding!!.toolbarLayout.imgBack.visibility = View.GONE
            binding!!.edtEmailAddress.isEnabled = false
        }else{
            binding!!.toolbarLayout.imgBack.visibility = View.VISIBLE
            binding!!.edtEmailAddress.isEnabled = true
        }
        binding!!.rlViewBankInformation.setOnClickListener(this)
        binding!!.rlViewNRIC.setOnClickListener(this)
        binding!!.rlViewDeclarations.setOnClickListener(this)
        binding!!.imvProfile.setOnClickListener(this)
        binding!!.btnUpdate.setOnClickListener(this)
        binding!!.edtMobileNumber.setOnClickListener(this)
        val spinnerCountArrayAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            resources.getStringArray(R.array.vehicle_type)
        )
        binding!!.spVehicleInfo.setAdapter(spinnerCountArrayAdapter)


        binding!!.spVehicleInfo.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                // your code here
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        })
        profileApiCall()
    }


    private fun profileApiCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        viewModel!!.profile_api(jsonObject)
    }


    private fun setData(userPojo: Data) {
        try {
            binding!!.txtUserName.text = userPojo!!.first_name + " " + userPojo!!.last_name
            binding!!.txtUniqueId.text = "uid: " + userPojo!!.user_id
            binding!!.edtUserName.setText(userPojo!!.first_name + " " + userPojo!!.last_name)
            binding!!.edtEmailAddress.setText(userPojo!!.email)
            binding!!.edtMobileNumber.setText(userPojo!!.mobile)
            Glide.with(this).load(userPojo!!.profile_pic).placeholder(R.drawable.default_user_pic).into(
                binding!!.imvProfile
            )
            if(!userPojo!!.country_code.isNullOrBlank())
            binding!!.ccp.setCountryForPhoneCode(userPojo!!.country_code.toInt())

            if(userPojo!!.bank_info.account_holder_name.isNullOrBlank()||userPojo!!.bank_info.account_number.isNullOrBlank()||userPojo!!.bank_info.bank_code.isNullOrBlank()||userPojo!!.bank_info.bank_name.isNullOrBlank()||userPojo!!.bank_info.branch_code.isNullOrBlank()||userPojo!!.bank_info.postal_code.isNullOrBlank()){
                binding!!.imvBankCheck.setImageDrawable(getDrawable(R.drawable.check_red))
            }else{
                binding!!.imvBankCheck.setImageDrawable(getDrawable(R.drawable.check))
            }
            if(userPojo!!.nric_info.nric_number.isNullOrBlank()||userPojo!!.nric_info.nric_back_image.isNullOrBlank()||userPojo!!.nric_info.nric_front_image.isNullOrBlank()){
                binding!!.imvNIRCCheck.setImageDrawable(getDrawable(R.drawable.check_red))
            }else{
                binding!!.imvNIRCCheck.setImageDrawable(getDrawable(R.drawable.check))
            }
            if(!userPojo!!.declaration.type_1||!userPojo!!.declaration.type_2||!userPojo!!.declaration.type_3){
                binding!!.imvDeclarationsCheck.setImageDrawable(getDrawable(R.drawable.check_red))
            }else{
                binding!!.imvDeclarationsCheck.setImageDrawable(getDrawable(R.drawable.check))
            }
            if(userPojo!!.bank_info.account_holder_name.isNullOrBlank()||userPojo!!.bank_info.account_number.isNullOrBlank()||userPojo!!.bank_info.bank_code.isNullOrBlank()||userPojo!!.bank_info.bank_name.isNullOrBlank()||userPojo!!.bank_info.branch_code.isNullOrBlank()||userPojo!!.bank_info.postal_code.isNullOrBlank()||userPojo!!.nric_info.nric_number.isNullOrBlank()||userPojo!!.nric_info.nric_back_image.isNullOrBlank()||userPojo!!.nric_info.nric_front_image.isNullOrBlank()||!userPojo!!.declaration.type_1||!!userPojo!!.declaration.type_2||!userPojo!!.declaration.type_3){
                binding!!.svMainView.visibility = View.VISIBLE
                binding!!.txtAdminApproval.visibility = View.GONE
            }else{
                if(fromActivity.equals("splash")){
                    binding!!.svMainView.visibility = View.GONE
                    binding!!.txtAdminApproval.visibility = View.VISIBLE
                    binding!!.txtAdminApproval.txtMessage.setText(getString(R.string.admin_approval))
                }else{
                    binding!!.svMainView.visibility = View.VISIBLE
                    binding!!.txtAdminApproval.visibility = View.GONE
                }
            }
            if(userPojo!!.is_approved.equals("1") && fromActivity.equals("splash")){
                App.sessionManager!!.setApproved(true)
                DashboardActivity.startActivity(mActivity!!, null, true)
            }else{
                App.sessionManager!!.setApproved(false)
            }
            if(userPojo!!.transport_category.equals("motorcycle")){
                binding!!.spVehicleInfo.setSelection(2)
            }else if(userPojo!!.transport_category.equals("car")){
                binding!!.spVehicleInfo.setSelection(1)
            }else if(userPojo!!.transport_category.equals("other")){
                binding!!.spVehicleInfo.setSelection(3)
            }else {
                binding!!.spVehicleInfo.setSelection(0)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    /*{
        "user_id": "503",
        "first_name": "Ram",
        "last_name": "Kumar",
        "email": "1993nareshj@gmail.com",
        "country_code": "+91",
        "mobile": "31313153",
        "address": "",
        "bank_name":"",
        "account_number":"000123456"
        "account_holder_name":""
        "nric_number":"",
        "nric_front_image":""
        "nric_back_image":"","transport_category_id":""
        "declaration": {"type_1": false,"type_2": true,"type_3": false} ,
        "branch_code":"1100"
        "bank_code":"000"
        "postal_code":"218745"
    }*/


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.edtMobileNumber -> {
                var bundle = Bundle()
                bundle.putSerializable("data", userPojo)
                bundle.putString("fromActivity", fromActivity)
                EnterMobileActivity.startActivity(this, bundle)
            }
            R.id.btnUpdate -> {
                val vehicle_type: String = binding!!.spVehicleInfo.selectedItem.toString()
                var full_name = binding!!.edtUserName.text.toString().trim()
                var email = binding!!.edtEmailAddress.text.toString().trim()
                var mobile = binding!!.edtMobileNumber.text.toString().trim()

                var first_name = ""
                var last_name = ""
                val stringArray: List<String> = full_name.split(" ")
                if(stringArray.size>0){
                    first_name = stringArray[0]
                    if(stringArray.size>1)
                        last_name = stringArray[1]
                }
                var transport_category = ""
                if (vehicle_type.equals("Car")) {
                    transport_category = "2"
                } else if (vehicle_type.equals("Motorcycle")) {
                    transport_category = "1"
                } else if(vehicle_type.equals("Others(Walker,Bicycle,PMD)")){
                    transport_category = "3"
                }else{
                    transport_category = ""
                }
                if (isValidFormData(full_name, email, mobile)) {
                    val reqData: HashMap<String, Any> = HashMap()
                    reqData["user_id"] = App.sessionManager!!.getUserID()
                    reqData["first_name"] = first_name
                    reqData["last_name"] = last_name
                    reqData["email"] = binding!!.edtEmailAddress.text.toString().trim()
                    reqData["country_code"] = binding!!.ccp.selectedCountryCode
                    reqData["mobile"] = binding!!.edtMobileNumber.text.toString().trim()
                    reqData["address"] = ""
                    reqData["bank_name"] = ""
                    reqData["transport_category_id"] = transport_category
                    reqData["account_number"] = ""
                    reqData["account_holder_name"] = ""
                    reqData["nric_number"] = ""
                    reqData["declaration"] = ""
                    reqData["branch_code"] = ""
                    reqData["bank_code"] = ""
                    reqData["postal_code"] = ""
                    viewModel!!.runner_edit_profile(
                        reqData,
                        profileFile,
                        nricFrontImageFile,
                        nricBackImageFile
                    )
                }
            }
            R.id.rlViewBankInformation -> {
                var bundle = Bundle()
                bundle.putSerializable("data", userPojo)
                BankInformationActivity.startActivity(this, bundle)
            }
            R.id.rlViewNRIC -> {
                var bundle = Bundle()
                bundle.putSerializable("data", userPojo)
                NIRCActivity.startActivity(this, bundle)
            }
            R.id.rlViewDeclarations -> {
                var bundle = Bundle()
                bundle.putSerializable("data", userPojo)
                DeclarationsActivity.startActivity(this, bundle)
            }
            R.id.imvProfile -> {
                ImagePicker.with(mActivity!!)
                    .crop() //Crop image(Optional), Check Customization for more option
                    .compress(1024)            //Final image size will be less than 1 MB(Optional)
                    .maxResultSize(
                        1080,
                        1080
                    )    //Final image resolution will be less than 1080 x 1080(Optional)
                    .start()
            }

        }
    }

    private fun isValidFormData(
        full_name: String,
        email: String,
        phone: String
    ): Boolean {

        if (TextUtils.isEmpty(full_name)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter full name")
            return false
        }
        if (full_name.length < 2 || full_name.length > 30) {
            UiHelper.showErrorToastMsgShort(
                mActivity!!,
                getString(R.string.err_msg_fullname_length)
            )
            return false
        }
        if (TextUtils.isEmpty(email)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter email")
            return false
        }
        if (!UtilityMethod.isValidEmail(email)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter valid email")
            return false
        }
        if (TextUtils.isEmpty(phone)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter mobile number")
            return false
        }

        if (phone.length < 7 || phone.length >= 15) {
            UiHelper.showErrorToastMsgShort(
                mActivity!!,
                "Please enter valid mobile number"
            )
            return false
        }

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode==111||resultCode==145){
            profileApiCall()
        }
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data

            profileFile = ImagePicker.getFile(data)
            Glide.with(this)
                .load(profileFile)
                .into(binding!!.imvProfile)

            //You can get File object from intent
            val file: File? = ImagePicker.getFile(data)

            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(data)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            //Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }




    private fun handleProfileResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    userPojo = response.data!!
                    App.sessionManager!!.setUserData(response.data)
                    setData(response.data!!)
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()

                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }



    private fun handleResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    //userPojo = response.data
                    UiHelper.showSucessToastMsgShort(mActivity!!, "Profile updated successfully")
                    App.sessionManager!!.setUserData(response.data)
                    if (fromActivity.equals("account")) {
                        setResult(201)
                        finish()
                    } else {
                        profileApiCall()
                    }
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }
}
