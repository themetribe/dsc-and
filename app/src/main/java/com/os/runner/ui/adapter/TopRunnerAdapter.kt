package com.os.runner.ui.adapter

import android.app.Activity
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.app.dstarrunner.R
import com.os.runner.model.DataTopTenRunner
import kotlinx.android.synthetic.main.row_top_runner.view.*

class TopRunnerAdapter(
    private val context: Activity?,
    private val list: ArrayList<DataTopTenRunner>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_top_runner

    override fun getViewModel(position: Int): Any? = list[position]
    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {

        Glide.with(context!!).load(list[position].image).placeholder(R.drawable.default_user_pic).into(viewDataBinding.root.imvRunnerProfile)
        viewDataBinding.root.txtRunnerName.text = list[position].name
        viewDataBinding.root.txtCompletedTrip.text = list[position].complete_orders
        viewDataBinding.root.txtRating.text = list[position].rating
        viewDataBinding.root.txtCancelledTrip.text = list[position].cancelled_orders
        var posi = position
        posi = posi+1
        viewDataBinding.root.txtRunnerRank.text = posi.toString()

        if(position==0){
            viewDataBinding.root.txtRunnerRank.visibility  = View.VISIBLE
            DrawableCompat.setTint(
                DrawableCompat.wrap(viewDataBinding.root.imvRanking.getDrawable()),
                ContextCompat.getColor(context, R.color.colorPrimaryDark)
            )
        }else if(position==1){
            viewDataBinding.root.txtRunnerRank.visibility  = View.VISIBLE
            DrawableCompat.setTint(
                DrawableCompat.wrap(viewDataBinding.root.imvRanking.getDrawable()),
                ContextCompat.getColor(context, R.color.GrayColor)
            )
        }else if(position==2){
            viewDataBinding.root.txtRunnerRank.visibility  = View.VISIBLE
            DrawableCompat.setTint(
                DrawableCompat.wrap(viewDataBinding.root.imvRanking.getDrawable()),
                ContextCompat.getColor(context, R.color.orange_800)
            )
        }else{
            viewDataBinding.root.imvRanking.visibility  = View.GONE
            DrawableCompat.setTint(
                DrawableCompat.wrap(viewDataBinding.root.imvRanking.getDrawable()),
                ContextCompat.getColor(context, R.color.black)
            )
        }

        if(list[position].view_status.equals("online")){
            viewDataBinding.root.rlViewStatus.setBackgroundDrawable(context.resources.getDrawable(R.drawable.bg_view_status_green))
        }else{
            viewDataBinding.root.rlViewStatus.setBackgroundDrawable(context.resources.getDrawable(R.drawable.bg_view_status_red))
        }
    }
    override fun getItemCount(): Int = list.size
}