package com.os.runner.ui.adapter

import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.app.dstarrunner.R
import com.os.runner.model.DataMyEarning
import kotlinx.android.synthetic.main.row_my_earning.view.*

class MyEarningAdapter(
    private val context: Activity?,
    private val list: ArrayList<DataMyEarning>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_my_earning

    override fun getViewModel(position: Int): Any? = list[position]

    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        viewDataBinding.root.txtMonth.text = list[position].month
        viewDataBinding.root.txtMonthEarning.text = "S$ "+list[position].total_amount
        viewDataBinding.root.cvEarningView.tag = position
        viewDataBinding.root.cvEarningView.setOnClickListener(onClickListener)
    }

    override fun getItemCount(): Int = list.size
}