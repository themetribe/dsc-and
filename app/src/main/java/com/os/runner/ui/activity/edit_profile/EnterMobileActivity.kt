package com.os.runner.ui.activity.edit_profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityEnterMobileBinding
import com.os.runner.model.Data
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.activity.verify_mobile.VerifyMobileActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.activity_enter_mobile.*
import java.io.File

class EnterMobileActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: EditProfileViewModel? = null
    private var binding: ActivityEnterMobileBinding? = null
    private var userPojo: Data? = null
    private var profileFile: File? = null
    private var nricFrontImageFile: File? = null
    private var nricBackImageFile: File? = null
    private var fromActivity: String? = null

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_enter_mobile)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        if (intent.extras != null) {
            userPojo = intent.getBundleExtra("bundle")!!.getSerializable("data") as Data
            fromActivity = intent.extras!!.getBundle("bundle")!!.getString("fromActivity") as String
        }
        initControls()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, EnterMobileActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivityForResult(intent, 111)
        }
    }


    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleResult(it)
        })
    }

    private fun initControls() {
        binding!!.imgBack.setOnClickListener(this)
        binding!!.btnSubmit.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                finish()
            }
            R.id.btnSubmit -> {
                var phone_number = binding!!.edtPhoneNumber.text.toString().trim()

                if (isValidFormData(phone_number)) {
                    val reqData: HashMap<String, Any> = HashMap()
                    reqData["user_id"] = App.sessionManager!!.getUserID()
                    reqData["first_name"] = userPojo!!.first_name
                    reqData["last_name"] = userPojo!!.last_name
                    reqData["email"] = userPojo!!.email
                    reqData["country_code"] = binding!!.ccp.selectedCountryCode
                    reqData["mobile"] = binding!!.edtPhoneNumber.text.toString().trim()
                    reqData["address"] = ""
                    reqData["bank_name"] = ""
                    reqData["transport_category_id"] = ""
                    reqData["account_number"] = ""
                    reqData["account_holder_name"] = ""
                    reqData["nric_number"] = ""
                    reqData["declaration"] = ""
                    reqData["branch_code"] = ""
                    reqData["bank_code"] = ""
                    reqData["postal_code"] = ""
                    viewModel!!.runner_edit_profile(
                        reqData,
                        profileFile,
                        nricFrontImageFile,
                        nricBackImageFile
                    )
                }
            }
        }
    }

    private fun isValidFormData(
        phone: String
    ): Boolean {

        if (TextUtils.isEmpty(phone)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter Phone number")
            return false
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 222) {
            setResult(145)
            finish()
        }
    }



    private fun handleResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    Log.e("tag", "result->" + response.data)
                    //userPojo = response.data
                    //UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    var bundle = Bundle()
                    bundle.putString("otp", response.data.otp)
                    bundle.putString("fromActivity", fromActivity)
                    bundle.putString("mobile", binding!!.edtPhoneNumber.text.toString().trim())
                    bundle.putString("country_code", binding!!.ccp.selectedCountryCode)
                    VerifyMobileActivity.startActivity(this, bundle)
                } else if (response.status == -1) {
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!, null, true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

}
