package com.os.runner.ui.activity.settings

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SettingsViewModel : ViewModel() {

    var responseLogoutData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiLogoutResponse: ApiResponse<DefaultPojo>? = null
    private var subscription: Disposable? = null


    init {
        apiLogoutResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }
    fun logout(jsonObject: JsonObject) {
        subscription = App.apiService!!.logout(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLogoutData.postValue(apiLogoutResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLogoutData.postValue(apiLogoutResponse!!.success(result))
                },
                { throwable ->
                    responseLogoutData.postValue(apiLogoutResponse!!.error(throwable))
                }
            )
    }

    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}