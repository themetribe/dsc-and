package com.os.runner.ui.fragment.transactions

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.TransactionsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class TransactionsViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<TransactionsResponse>>()
    var apiResponse: ApiResponse<TransactionsResponse>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun transactions_history(jsonObject: JsonObject) {
        subscription = App.apiService!!.transactions_history(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}