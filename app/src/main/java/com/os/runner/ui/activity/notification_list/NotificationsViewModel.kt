package com.os.runner.ui.activity.notification_list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import com.os.runner.model.NotifiationResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class NotificationsViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<NotifiationResponse>>()
    var responseReadData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiResponse: ApiResponse<NotifiationResponse>? = null
    var apiReadResponse: ApiResponse<DefaultPojo>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiReadResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }

    fun notification_list(jsonObject: JsonObject) {
        subscription = App.apiService!!.notification_list(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }

    fun notification_read_unread(jsonObject: JsonObject) {
        subscription = App.apiService!!.notification_read_unread(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseReadData.postValue(apiReadResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseReadData.postValue(apiReadResponse!!.success(result))
                },
                { throwable ->
                    responseReadData.postValue(apiReadResponse!!.error(throwable))
                }
            )
    }

    fun notification_delete(jsonObject: JsonObject) {
        subscription = App.apiService!!.notification_delete(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseReadData.postValue(apiReadResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseReadData.postValue(apiReadResponse!!.success(result))
                },
                { throwable ->
                    responseReadData.postValue(apiReadResponse!!.error(throwable))
                }
            )
    }


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}