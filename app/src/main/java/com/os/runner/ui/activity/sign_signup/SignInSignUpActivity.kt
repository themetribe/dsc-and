package com.os.runner.ui.activity.sign_signup

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.facebook.*
import com.facebook.internal.CallbackManagerImpl
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.SigninSignupLayoutBinding
import com.os.runner.model.DefaultPojo
import com.os.runner.model.UserPojo
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.dashboard.DashboardActivity
import com.os.runner.ui.activity.edit_profile.EditProfileActivity
import com.os.runner.ui.activity.static_page.StatisContentPagesActivity
import com.os.runner.ui.activity.verify_mobile.VerifyMobileActivity
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import com.os.runner.utility.UtilityMethod
import org.json.JSONException
import org.json.JSONObject
import java.net.URL
import java.util.*


class SignInSignUpActivity : BaseBindingActivity(), View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private var viewModel: SignInSignUpViewModel? = null
    private var binding: SigninSignupLayoutBinding? = null
    private var latitude = 0.0
    private var longitude = 0.0
    private var action_type = ""
    private var type = ""
    var isMobileNumber: Boolean = false
    private val RC_SIGN_IN = 9001
    private var mGoogleApiClient: GoogleApiClient? = null
    private var callbackManager: CallbackManager? = null

    override fun onLocationAvailable(latLng: LatLng?) {
        super.onLocationAvailable(latLng)
        if (latLng != null) {
            latitude = latLng.latitude
            longitude = latLng.longitude
        }
    }

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(SignInSignUpViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.signin_signup_layout)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        binding!!.llGoForSignIn.setOnClickListener(this)
        binding!!.llGoForSignUp.setOnClickListener(this)
        binding!!.llFooterSignUpView.setOnClickListener(this)
        binding!!.llFooterSignInView.setOnClickListener(this)
        binding!!.llSignInView.setOnClickListener(this)
        binding!!.llSignUpView.setOnClickListener(this)
        binding!!.txtTermsOfCondition.setOnClickListener(this)
        binding!!.txtPrivacyPolicy.setOnClickListener(this)
        binding!!.txtCodeOfConduct.setOnClickListener(this)
        binding!!.txtForgotPassword.setOnClickListener(this)
        binding!!.imvFacebook.setOnClickListener(this)
        binding!!.imvGoogle.setOnClickListener(this)


        val spinnerCountArrayAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            resources.getStringArray(R.array.vehicle_type)
        )
        binding!!.spinVehicleType.setAdapter(spinnerCountArrayAdapter)

        binding!!.edtEmailMobile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!s!!.isNullOrBlank()) {
                    if (UtilityMethod.isNumeric(s.toString())) {
                        isMobileNumber = true
                    } else {
                        isMobileNumber = false
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        initControls()
    }

    override fun setListeners() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(mActivity!!, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
    }

    private fun initControls() {
        viewModel!!.responseLiveData.observe(this, Observer {
            handleSignUpResult(it)
        })

        viewModel!!.responseSocialLoginData.observe(this, Observer {
            handleSocialSignUpResult(it)
        })

        viewModel!!.responseForgotPasswordData.observe(this, Observer {
            handleForgotPasswordResult(it)
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.llGoForSignIn -> {
                action_type = "signin"
                if (isMobileNumber)
                    type = "mobile"
                else
                    type = "email"
                if (isValidLoginFormData(
                        binding!!.edtEmailMobile.text.toString().trim(),
                        binding!!.edtLoginPassword.text.toString().trim()
                    )
                ) {
                    var jsonObject = JsonObject()
                    jsonObject.addProperty(Constants.USER_TYPE, "1")
                    jsonObject.addProperty(Constants.LOGIN_TYPE, type)
                    jsonObject.addProperty(
                        Constants.EMAIL,
                        binding!!.edtEmailMobile.text.toString().trim()
                    )
                    jsonObject.addProperty(
                        Constants.PASSWORD,
                        binding!!.edtLoginPassword.text.toString().trim()
                    )
                    jsonObject.addProperty(
                        Constants.COUNTRY_CODE,
                        ""
                    )
                    jsonObject.addProperty(
                        Constants.DEVICE_ID,
                        App.sessionManager!!.getDeviceToken()
                    )
                    jsonObject.addProperty(Constants.DEVICE_TYPE, "android")
                    viewModel!!.login(jsonObject)
                }
            }
            R.id.llGoForSignUp -> {
                action_type = "signup"
                val vehicle_type: String = binding!!.spinVehicleType.selectedItem.toString()
                if (isValidFormData(
                        binding!!.edtFullName.text.toString().trim(),
                        binding!!.edtEmail.text.toString().trim(),
                        binding!!.edtPhoneNumber.text.toString().trim(),
                        binding!!.edtPassword.text.toString().trim(),
                        vehicle_type
                    )
                ) {
                    var full_name = binding!!.edtFullName.text.toString().trim()
                    val stringArray: List<String> = full_name.split(" ")

                    var first_name = ""
                    var last_name = ""
                    try {
                        first_name = stringArray[0]
                        last_name = stringArray[1]
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    /*var transport_category = ""
                    if (vehicle_type.equals("Car")) {
                        transport_category = "car"
                    } else if (vehicle_type.equals("Motorcycle")) {
                        transport_category = "motorcycle"
                    } else {
                        transport_category = "other"
                    }*/

                    var transport_category = ""
                    if (vehicle_type.equals("Car")) {
                        transport_category = "2"
                    } else if (vehicle_type.equals("Motorcycle")) {
                        transport_category = "1"
                    } else if(vehicle_type.equals("Others(Walker,Bicycle,PMD)")){
                        transport_category = "3"
                    }else{
                        transport_category = ""
                    }


                    var jsonObject = JsonObject()
                    jsonObject.addProperty(Constants.USER_TYPE, "1")
                    jsonObject.addProperty(Constants.TRASPORT_CATEGORY, transport_category)
                    jsonObject.addProperty(Constants.FIRST_NAME, first_name)
                    jsonObject.addProperty(Constants.LAST_NAME, last_name)
                    jsonObject.addProperty(
                        Constants.EMAIL,
                        binding!!.edtEmail.text.toString().trim()
                    )
                    jsonObject.addProperty(Constants.LATITUDE_NAME, latitude)
                    jsonObject.addProperty(Constants.LONGITUDE_NAME, longitude)
                    jsonObject.addProperty(
                        Constants.COUNTRY_CODE,
                        binding!!.ccp.selectedCountryCode
                    )
                    jsonObject.addProperty(
                        Constants.MOBILE,
                        binding!!.edtPhoneNumber.text.toString().trim()
                    )
                    jsonObject.addProperty(
                        Constants.DEVICE_ID,
                        App.sessionManager!!.getDeviceToken()
                    )
                    jsonObject.addProperty(Constants.DEVICE_TYPE, "android")
                    jsonObject.addProperty(
                        Constants.PASSWORD,
                        binding!!.edtPassword.text.toString().trim()
                    )
                    jsonObject.addProperty(
                        Constants.REFERRAL_CODE,
                        binding!!.edtReferralCode.text.toString().trim()
                    )
                    viewModel!!.signup(jsonObject)
                }
            }
            R.id.txtTermsOfCondition -> {
                val bundle = Bundle()
                bundle.putString("slug", "terms_condition")
                StatisContentPagesActivity.startActivity(this, bundle)
            }
            R.id.txtForgotPassword -> {
                if (TextUtils.isEmpty(binding!!.edtEmailMobile.text.toString().trim())) {
                    UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter email")
                } else {
                    var jsonObject = JsonObject()
                    jsonObject.addProperty(
                        Constants.EMAIL,
                        binding!!.edtEmailMobile.text.toString().trim()
                    )
                    viewModel!!.forgotPassword(jsonObject)
                }
            }
            R.id.txtPrivacyPolicy -> {
                val bundle = Bundle()
                bundle.putString("slug", "policy_privacy")
                StatisContentPagesActivity.startActivity(this, bundle)
            }
            R.id.txtCodeOfConduct -> {
                val bundle = Bundle()
                bundle.putString("slug", "code_of_conduct")
                StatisContentPagesActivity.startActivity(this, bundle)
            }
            R.id.llFooterSignUpView -> {
                binding!!.llForgotPassword.visibility = View.GONE
                binding!!.viewSignIn.visibility = View.GONE
                binding!!.viewSignUp.visibility = View.VISIBLE
                binding!!.llGoForSignIn.visibility = View.GONE
                binding!!.llGoForSignUp.visibility = View.VISIBLE
                binding!!.llSignInInsideView.visibility = View.GONE
                binding!!.llSignUpInsideView.visibility = View.VISIBLE
                binding!!.llFooterSignUpView.visibility = View.GONE
                binding!!.llFooterSignInView.visibility = View.VISIBLE
            }
            R.id.llFooterSignInView -> {
                binding!!.viewSignIn.visibility = View.VISIBLE
                binding!!.llForgotPassword.visibility = View.VISIBLE
                binding!!.viewSignUp.visibility = View.GONE
                binding!!.llGoForSignIn.visibility = View.VISIBLE
                binding!!.llGoForSignUp.visibility = View.GONE
                binding!!.llSignInInsideView.visibility = View.VISIBLE
                binding!!.llSignUpInsideView.visibility = View.GONE
                binding!!.llFooterSignUpView.visibility = View.VISIBLE
                binding!!.llFooterSignInView.visibility = View.GONE
            }
            R.id.llSignInView -> {
                binding!!.viewSignIn.visibility = View.VISIBLE
                binding!!.llForgotPassword.visibility = View.VISIBLE
                binding!!.llTermsCondition.visibility = View.GONE
                binding!!.viewSignUp.visibility = View.GONE
                binding!!.llGoForSignIn.visibility = View.VISIBLE
                binding!!.llGoForSignUp.visibility = View.GONE
                binding!!.llSignInInsideView.visibility = View.VISIBLE
                binding!!.llSignUpInsideView.visibility = View.GONE
                binding!!.llFooterSignUpView.visibility = View.VISIBLE
                binding!!.llFooterSignInView.visibility = View.GONE
            }
            R.id.llSignUpView -> {
                binding!!.llForgotPassword.visibility = View.GONE
                binding!!.llTermsCondition.visibility = View.VISIBLE
                binding!!.viewSignIn.visibility = View.GONE
                binding!!.viewSignUp.visibility = View.VISIBLE
                binding!!.llGoForSignIn.visibility = View.GONE
                binding!!.llGoForSignUp.visibility = View.VISIBLE
                binding!!.llSignInInsideView.visibility = View.GONE
                binding!!.llSignUpInsideView.visibility = View.VISIBLE
                binding!!.llFooterSignUpView.visibility = View.GONE
                binding!!.llFooterSignInView.visibility = View.VISIBLE

            }
            R.id.imvGoogle -> {
                getGoogleUserInfo()
            }
            R.id.imvFacebook -> {
                getFacebookUserInfo()
            }
        }
    }


    private fun getGoogleUserInfo() {
        if (UtilityMethod.hasInternet(mActivity!!)) {
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, RC_SIGN_IN)
        } else {
            UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.network_error))
        }
    }

    private fun isValidLoginFormData(
        email: String,
        password: String
    ): Boolean {

        if(isMobileNumber){
            if (TextUtils.isEmpty(password)) {
                UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter password")
                return false
            }
        }else{
            if (TextUtils.isEmpty(email)) {
                UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter email")
                return false
            }
        }
        return true
    }

    private fun isValidFormData(
        full_name: String,
        email: String,
        phone: String,
        password: String,
        vehicle_type: String
    ): Boolean {

        if (TextUtils.isEmpty(full_name)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter full name")
            return false
        }

        if (full_name.length < 2 || full_name.length > 30) {
            UiHelper.showErrorToastMsgShort(
                mActivity!!,
                getString(R.string.err_msg_fullname_length)
            )
            return false
        }

        if (TextUtils.isEmpty(email)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter email")
            return false
        }
        if (!UtilityMethod.isValidEmail(email)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter valid email")
            return false
        }
        if (TextUtils.isEmpty(phone)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter phone number")
            return false
        }

        if (phone.length < 7 || phone.length > 15) {
            UiHelper.showErrorToastMsgShort(
                mActivity!!,
                "Please enter valid mobile number"
            )
            return false
        }

        if (TextUtils.isEmpty(password)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please enter password")
            return false
        }

        if (password.length < 6 || password.length > 30) {
            UiHelper.showErrorToastMsgShort(
                mActivity!!,
                getString(R.string.err_msg_password_valid)
            )
            return false
        }

        if (!UtilityMethod.isValidPassword(password)) {
            UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.password_alpha_bets))
            return false
        }

        if (vehicle_type.isNullOrBlank() || vehicle_type.equals("Select your vehicle type")) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "please select vehicle type")
            return false
        }

        if (binding!!.checkboxTermsConfition.isChecked==false) {
            UiHelper.showErrorToastMsgShort(mActivity!!, "Please select terms of condition")
            return false
        }
        return true
    }

    private fun isValidPhoneNumber(phoneNumber: CharSequence): Boolean {
        return if (!TextUtils.isEmpty(phoneNumber)) {
            Patterns.PHONE.matcher(phoneNumber).matches()
        } else false
    }

    companion object {

        fun startActivity(activity: Activity, bundle: Bundle?, isClear: Boolean) {
            val intent = Intent(activity, SignInSignUpActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            if (isClear) intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.startActivity(intent)
        }
    }

    private fun handleSignUpResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    Log.e("tag", "result->" + response.data)
                    if (action_type.equals("signin")) {
                        if (response.data.is_mobile_verify.equals("0")) {
                            App.sessionManager!!.setUserData(response.data)
                            App.sessionManager!!.setAuthToken(response.data.auth_token)
                            var bundle = Bundle()
                            bundle.putString("otp", response.data.otp)
                            bundle.putString("fromActivity", "splash")
                            bundle.putString("mobile", response.data.mobile)
                            bundle.putString("country_code", response.data.country_code)
                            VerifyMobileActivity.startActivity(this, bundle)
                        } else if (response.data.is_approved.equals("0")) {
                            App.sessionManager!!.setUserData(response.data)
                            App.sessionManager!!.setAuthToken(response.data.auth_token)
                            App.sessionManager!!.setLogin(true)
                            val bundle = Bundle()
                            bundle.putSerializable("fromActivity", "splash")
                            EditProfileActivity.startActivity(mActivity!!, bundle,true)
                            finish()
                        } else {
                            App.sessionManager!!.setUserData(response.data)
                            App.sessionManager!!.setAuthToken(response.data.auth_token)
                            App.sessionManager!!.setLogin(true)
                            DashboardActivity.startActivity(this, null, true)
                            finish()
                        }
                    } else {
                        App.sessionManager!!.setUserData(response.data)
                        App.sessionManager!!.setAuthToken(response.data.auth_token)
                        var bundle = Bundle()
                        bundle.putString("otp", response.data.otp)
                        bundle.putString("fromActivity", "splash")
                        bundle.putString("mobile", response.data.mobile)
                        bundle.putString("country_code", response.data.country_code)
                        VerifyMobileActivity.startActivity(this, bundle)
                    }
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }


    private fun handleSocialSignUpResult(result: ApiResponse<UserPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                    Log.e("tag", "result->" + response.data)
                    if (response.data.is_approved.equals("0")) {
                        App.sessionManager!!.setUserData(response.data)
                        App.sessionManager!!.setAuthToken(response.data.auth_token)
                        App.sessionManager!!.setLogin(true)
                        val bundle = Bundle()
                        bundle.putSerializable("fromActivity", "splash")
                        EditProfileActivity.startActivity(mActivity!!, bundle,true)
                    } else {
                        App.sessionManager!!.setUserData(response.data)
                        App.sessionManager!!.setAuthToken(response.data.auth_token)
                        App.sessionManager!!.setLogin(true)
                        DashboardActivity.startActivity(this, null, true)
                    }
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }


    private fun handleForgotPasswordResult(result: ApiResponse<DefaultPojo>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(this)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    UiHelper.showSucessToastMsgShort(mActivity!!, response.message)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    fun getFacebookUserInfo() {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    var fb_LoginToken: String = loginResult.accessToken.toString()
                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken,
                        object : GraphRequest.GraphJSONObjectCallback {
                            override fun onCompleted(
                                `object`: JSONObject,
                                response: GraphResponse?
                            ) {
                                if (response != null) {
                                    try {
                                        var id = `object`.getString("id")
                                        var name = `object`.getString("name")
                                        //var email = `object`.getString("email")
                                        val profile_pic =
                                            URL("https://graph.facebook.com/$id/picture?width=200&height=150")
                                        val uri = Uri.parse(profile_pic.toString())
                                        socialSignupCall(id, name, "", uri, "facebook")
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }
                                }
                            }
                        })

                    request.executeAsync()
                }

                override fun onCancel() {
                    if (AccessToken.getCurrentAccessToken() != null)
                        LoginManager.getInstance().logOut()
                }

                override fun onError(exception: FacebookException) {
                    if (exception.message!!.contains("CONNECTION_FAILURE")) {
                    } else if (exception is FacebookAuthorizationException) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut()

                        }
                    }
                }
            })
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.d("bett", "onConnectionFailed:" + connectionResult)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result!!)
        }else if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }

        if(resultCode==222){
            finish()
        }
    }


    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            socialSignupCall(
                result!!.signInAccount!!.id,
                result!!.signInAccount!!.displayName,
                result!!.signInAccount!!.email,
                result!!.signInAccount!!.photoUrl,
                "google"
            )
        }
    }

    private fun socialSignupCall(
        id: String?,
        displayName: String?,
        email: String?,
        profile: Uri?,
        type: String
    ) {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_TYPE, "1")
        jsonObject.addProperty(Constants.FIRST_NAME, displayName!!.split(" ")[0])
        jsonObject.addProperty(Constants.LAST_NAME, displayName!!.split(" ")[1])
        jsonObject.addProperty(Constants.EMAIL, email)
        jsonObject.addProperty(Constants.DEVICE_ID, App.sessionManager!!.getDeviceToken())
        jsonObject.addProperty(Constants.DEVICE_TYPE, "android")
        jsonObject.addProperty(Constants.LATITUDE_NAME, latitude)
        jsonObject.addProperty(Constants.LONGITUDE_NAME, longitude)
        jsonObject.addProperty(Constants.SOCIAL_ID, id)
        jsonObject.addProperty(Constants.SOCIAL_TYPE, type)
        jsonObject.addProperty("profile_pic", profile.toString())
        viewModel!!.social_signup(jsonObject)
    }
}
