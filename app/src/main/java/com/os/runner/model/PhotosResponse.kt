package com.os.runner.model

import java.io.Serializable

data class PhotosResponse(
    val response: ResponsePhotos
)

data class ResponsePhotos(
    val `data`: List<PhotosList>,
    val message: String,
    val status: Int
)

data class PhotosList(
    var id: Int=0,
    val user_id: Int=0,
    val image: String="",
    val status: String="",
    val created_at: String="",
    val updated_at: String=""
):Serializable