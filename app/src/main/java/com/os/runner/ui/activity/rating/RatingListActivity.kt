package com.os.runner.ui.activity.rating

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityRatingListBinding
import com.os.runner.model.*
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.RatingListAdapter
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.no_data_view.view.*

class RatingListActivity : BaseBindingActivity(), View.OnClickListener {


    private var binding: ActivityRatingListBinding? = null
    private var viewModel: CustomerRatingViewModel? = null

    /*Top Runner Adapter*/
    private var dataRatingList: ArrayList<DataRatingList>? = ArrayList()
    private var ratingLitAdapter: RatingListAdapter? = null
    private var onClickListener: View.OnClickListener? = null

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(CustomerRatingViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rating_list)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, RatingListActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    override fun setListeners() {
        viewModel!!.responseRatingLiveData.observe(this, Observer {
            handleResult(it)
        })

    }

    private fun initControl() {
        binding!!.toolbarLayout.toolbarTitle.setText("Ratings")
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerView.setLayoutManager(mLayoutManager)

        ratingLitAdapter = RatingListAdapter(this, dataRatingList!!, onClickListener)
        binding!!.recyclerView.adapter = ratingLitAdapter
        myRatingApiCall()
    }


    private fun myRatingApiCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        viewModel!!.my_rating(jsonObject)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }


    private fun handleResult(result: ApiResponse<RatingListResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if (response.data.size==0) {
                        binding!!.txtNoDataFound.visibility = View.VISIBLE
                        binding!!.txtNoDataFound.txtMessage.text = response.message
                        binding!!.recyclerView.visibility = View.GONE
                    } else {
                        binding!!.txtNoDataFound.visibility = View.GONE
                        binding!!.recyclerView.visibility = View.VISIBLE
                        setAdapters(response.data)
                    }
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setAdapters(data: List<DataRatingList>) {
        if (data != null) {
            dataRatingList!!.clear()
            dataRatingList!!.addAll(data)
            ratingLitAdapter!!.notifyDataSetChanged()
        }
    }
}