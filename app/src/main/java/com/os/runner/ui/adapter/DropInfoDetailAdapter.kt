package com.os.runner.ui.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.app.dstarrunner.R
import com.os.runner.model.DropInfoRequestDetail
import kotlinx.android.synthetic.main.row_drop_info_item_location.view.*

class DropInfoDetailAdapter(
    private val context: Activity?,
    private val list: ArrayList<DropInfoRequestDetail>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_drop_info_item_location

    override fun getViewModel(position: Int): Any? = list[position]

    @SuppressLint("SetTextI18n")
    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        viewDataBinding.root.txtName.text = list[position].name
        viewDataBinding.root.txtMobile.text = list[position].contact_number
        viewDataBinding.root.txtLandmark.text = list[position].landmark
        viewDataBinding.root.txtLocation.text = list[position].address

        if(list[position].status.equals("submit")){
            viewDataBinding.root.txtDropOffStatus.text = "Delivered"
        }else{
            viewDataBinding.root.txtDropOffStatus.text = "Pending"
        }
        //viewDataBinding.root.txtDropOffStatus.text = list[position].status
        viewDataBinding.root.rlLocationView.tag = position
        viewDataBinding.root.rlLocationView.setOnClickListener(onClickListener)
    }

    override fun getItemCount(): Int = list.size
}