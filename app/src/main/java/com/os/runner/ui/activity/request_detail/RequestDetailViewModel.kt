package com.os.drunnercustomer.ui.activity.request_detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.CallResponse
import com.os.runner.model.DefaultPojo
import com.os.runner.model.RequestDetailResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RequestDetailViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<RequestDetailResponse>>()
    var responseFavouriteData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var responseStartJobData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var responseTwilioTokenData = MutableLiveData<ApiResponse<CallResponse>>()
    var apiResponse: ApiResponse<RequestDetailResponse>? = null
    var apiFavouriteResponse: ApiResponse<DefaultPojo>? = null
    var apiStartJobResponse: ApiResponse<DefaultPojo>? = null
    var apiTwilioTokenResponse: ApiResponse<CallResponse>? = null
    private var subscription: Disposable? = null

    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiFavouriteResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiStartJobResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiTwilioTokenResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun runner_request_detail(jsonObject: JsonObject) {
        subscription = App.apiService!!.runner_request_detail(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }

    fun start_job(jsonObject: JsonObject) {
        subscription = App.apiService!!.start_job(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseStartJobData.postValue(apiStartJobResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseStartJobData.postValue(apiStartJobResponse!!.success(result))
                },
                { throwable ->
                    responseStartJobData.postValue(apiStartJobResponse!!.error(throwable))
                }
            )
    }
    fun runner_cancel_request(jsonObject: JsonObject) {
        subscription = App.apiService!!.runner_cancel_request(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseStartJobData.postValue(apiStartJobResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseStartJobData.postValue(apiStartJobResponse!!.success(result))
                },
                { throwable ->
                    responseStartJobData.postValue(apiStartJobResponse!!.error(throwable))
                }
            )
    }


    fun action_start_job(hashMap: HashMap<String, Any>) {
        subscription = App.apiService!!.action_start_job(hashMap)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseStartJobData.postValue(apiStartJobResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseStartJobData.postValue(apiStartJobResponse!!.success(result))
                },
                { throwable ->
                    responseStartJobData.postValue(apiStartJobResponse!!.error(throwable))
                }
            )
    }

    fun twilio_token(jsonObject: JsonObject) {
        subscription = App.apiService!!.twilio_token(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseTwilioTokenData.postValue(apiTwilioTokenResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseTwilioTokenData.postValue(apiTwilioTokenResponse!!.success(result))
                },
                { throwable ->
                    responseTwilioTokenData.postValue(apiTwilioTokenResponse!!.error(throwable))
                }
            )
    }

    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}