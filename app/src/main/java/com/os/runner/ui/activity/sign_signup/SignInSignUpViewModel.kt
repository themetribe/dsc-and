package com.os.runner.ui.activity.sign_signup

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import com.os.runner.model.UserPojo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SignInSignUpViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<UserPojo>>()
    var apiResponse: ApiResponse<UserPojo>? = null

    var responseSocialLoginData = MutableLiveData<ApiResponse<UserPojo>>()
    var apiSocialLoginResponse: ApiResponse<UserPojo>? = null

    var responseForgotPasswordData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiForgotPasswordResponse: ApiResponse<DefaultPojo>? = null

    private var subscription: Disposable? = null

    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiSocialLoginResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiForgotPasswordResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    fun signup(jsonObject: JsonObject) {
        subscription = App.apiService!!.signup(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }


    fun social_signup(jsonObject: JsonObject) {
        subscription = App.apiService!!.social_signup(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSocialLoginData.postValue(apiSocialLoginResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseSocialLoginData.postValue(apiSocialLoginResponse!!.success(result))
                },
                { throwable ->
                    responseSocialLoginData.postValue(apiSocialLoginResponse!!.error(throwable))
                }
            )
    }

    fun forgotPassword(jsonObject: JsonObject) {
        subscription = App.apiService!!.forgotPassword(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseForgotPasswordData.postValue(apiForgotPasswordResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseForgotPasswordData.postValue(apiForgotPasswordResponse!!.success(result))
                },
                { throwable ->
                    responseForgotPasswordData.postValue(apiForgotPasswordResponse!!.error(throwable))
                }
            )
    }



    fun login(jsonObject: JsonObject) {
        subscription = App.apiService!!.login(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }



    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}