package com.os.runner.ui.activity.top_runner

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.app.dstarrunner.R
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.app.dstarrunner.databinding.ActivityTopRunnerBinding
import com.os.runner.model.*
import com.os.runner.ui.activity.BaseBindingActivity
import com.os.runner.ui.activity.sign_signup.SignInSignUpActivity
import com.os.runner.ui.adapter.TopRunnerAdapter
import com.os.runner.ui.dialogs.ProgressDialog
import com.os.runner.utility.Constants
import com.os.runner.utility.UiHelper
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_view.view.*

class TopRunnerActivity : BaseBindingActivity(), View.OnClickListener {


    private var binding: ActivityTopRunnerBinding? = null
    private var viewModel: TopRunnerViewModel? = null

    /*Top Runner Adapter*/
    private var dataTopTenRunnerList: ArrayList<DataTopTenRunner>? = ArrayList()
    private var topRunnerAdapter: TopRunnerAdapter? = null
    private var onClickListener: View.OnClickListener? = null

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(TopRunnerViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_top_runner)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    override fun initializeObject() {
        initControl()
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, TopRunnerActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    override fun setListeners() {
        viewModel!!.responseLiveData.observe(this, androidx.lifecycle.Observer {
            handleResult(it)
        })
    }

    private fun initControl() {
        binding!!.toolbarLayout.toolbarTitle.setText(getString(R.string.ranking))
        binding!!.toolbarLayout.imgBack.setOnClickListener(this)
        binding!!.txtRunnerRanking.text = App.sessionManager!!.getRunnerPosition()

        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerView.setLayoutManager(mLayoutManager)

        topRunnerAdapter = TopRunnerAdapter(this, dataTopTenRunnerList!!, onClickListener)
        binding!!.recyclerView.adapter = topRunnerAdapter
        topTenApiCall()
    }


    private fun topTenApiCall() {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constants.USER_ID, App.sessionManager!!.getUserID())
        jsonObject.addProperty(Constants.LATITUDE_NAME,App.sessionManager!!.getLatitute())
        jsonObject.addProperty(Constants.LONGITUDE_NAME, App.sessionManager!!.getLongitute())
        jsonObject.addProperty("transport_category", "")
        viewModel!!.top_ten_runners(jsonObject)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }


    private fun handleResult(result: ApiResponse<TopTenRunnerResponse>?) {
        when (result!!.status) {
            ApiResponse.Status.ERROR -> {
                ProgressDialog.hideProgressDialog()
                UiHelper.showErrorToastMsgShort(mActivity!!, getString(R.string.server_error))
            }
            ApiResponse.Status.LOADING -> {
                ProgressDialog.showProgressDialog(mActivity!!)
            }
            ApiResponse.Status.SUCCESS -> {
                ProgressDialog.hideProgressDialog()
                val response = result.data!!
                if (response.status == 1) {
                    if (response.data.size==0) {
                        binding!!.txtNoDataFound.visibility = View.VISIBLE
                        binding!!.txtNoDataFound.txtMessage.text = response.message
                        binding!!.recyclerView.visibility = View.GONE
                    } else {
                        binding!!.txtNoDataFound.visibility = View.GONE
                        binding!!.recyclerView.visibility = View.VISIBLE
                        setAdapters(response.data)
                    }
                }else if(response.status == -1){
                    App.sessionManager!!.setLogout()
                    SignInSignUpActivity.startActivity(mActivity!!,null,true)
                } else {
                    UiHelper.showErrorToastMsgShort(mActivity!!, response.message)
                }
            }
        }
    }

    private fun setAdapters(data: List<DataTopTenRunner>) {
        if (data != null) {
            dataTopTenRunnerList!!.clear()
            dataTopTenRunnerList!!.addAll(data)
            topRunnerAdapter!!.notifyDataSetChanged()
        }
    }
}