package com.os.runner.model

data class NotifiationResponse(
    val `data`: DataNotification,
    val message: String,
    val status: Int
)

data class DataNotification(
    val notif_data: List<NotifData>,
    val notification_count: String,
    val unread_notification_count: String
)

data class NotifData(
    val created_at: String,
    val is_read: String,
    val notification: String,
    val notification_id: String,
    val notification_type: String,
    val request_id: String,
    val service_name: String,
    val title: String,
    val user_id: String
)