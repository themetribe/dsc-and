package com.os.runner.ui.dialogs


import android.app.Activity
import android.text.TextUtils
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.app.dstarrunner.R


object AlertMessageDialog: LifecycleObserver {
    val dialog: AlertDialog? = null
    var isAppDestroy = false

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onActivityDestroy() {
        isAppDestroy = true
        if (dialog != null) dialog.dismiss()
    }


    fun showDialog(
        activity: Activity,
        title: String,
        message: String,
        okText: String,
        cancelText: String,
        mAlertMessageCallback: AlertMessageCallback
    ) {
        if (isAppDestroy) return
        val alertDialog = AlertDialog.Builder(activity)
        if (!TextUtils.isEmpty(title))
            alertDialog.setTitle(title)
        alertDialog.setMessage(message)

        alertDialog.setPositiveButton(
            okText
        ) { dialog, which ->
            mAlertMessageCallback.onSuccessClick()
            dialog.dismiss()
        }
        if (!TextUtils.isEmpty(cancelText))
            alertDialog.setNegativeButton(activity.getString(R.string.cancel)) { dialog, which ->
                mAlertMessageCallback.onCancelClick()
                dialog.dismiss()
            }
        val dialog = alertDialog.create()
        dialog.show()
    }

    interface AlertMessageCallback {
        fun onSuccessClick()

        fun onCancelClick()


    }


}