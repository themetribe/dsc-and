package com.os.runner.ui.activity.edit_profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.os.runner.application.ApiResponse
import com.os.runner.application.App
import com.os.runner.model.DefaultPojo
import com.os.runner.model.UserPojo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class EditProfileViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<UserPojo>>()
    var responseProfileLiveData = MutableLiveData<ApiResponse<UserPojo>>()
    var apiResponse: ApiResponse<UserPojo>? = null
    var apiProfileResponse: ApiResponse<UserPojo>? = null
    var responseLogoutData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiLogoutResponse: ApiResponse<DefaultPojo>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
        apiProfileResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }

    fun logout(jsonObject: JsonObject) {
        subscription = App.apiService!!.logout(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLogoutData.postValue(apiLogoutResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLogoutData.postValue(apiLogoutResponse!!.success(result))
                },
                { throwable ->
                    responseLogoutData.postValue(apiLogoutResponse!!.error(throwable))
                }
            )
    }


    /*{
        "user_id": "503",
        "first_name": "Ram",
        "last_name": "Kumar",
        "email": "1993nareshj@gmail.com",
        "country_code": "+91",
        "mobile": "31313153",
        "address": "",
        "bank_name":"",
        "account_number":"000123456"
        "account_holder_name":""
        "nric_number":"",
        "nric_front_image":""
        "nric_back_image":"","transport_category_id":""
        "declaration": {"type_1": false,"type_2": true,"type_3": false} ,
        "branch_code":"1100"

        "bank_code":"000"

        "postal_code":"218745"

    }*/


    fun profile_api(jsonObject: JsonObject) {
        subscription = App.apiService!!.profile_api(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseProfileLiveData.postValue(apiProfileResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseProfileLiveData.postValue(apiProfileResponse!!.success(result))
                },
                { throwable ->
                    responseProfileLiveData.postValue(apiProfileResponse!!.error(throwable))
                }
            )
    }




    fun runner_edit_profile(
        reqData: HashMap<String, Any>,
        image: File?,
        nricFrontImageFile: File?,
        nricBackImageFile: File?
    ) {

        val mediaType = MediaType.parse("multipart/form-data")
        var imageBody: MultipartBody.Part? = null
        var nricFrontImageFileBody: MultipartBody.Part? = null
        var nricBackImageFileBody: MultipartBody.Part? = null
        if (image != null) {
            val requestFile = RequestBody.create(mediaType, image)
            imageBody = MultipartBody.Part.createFormData("image", image.getName(), requestFile)
        }

        if (nricFrontImageFile != null) {
            val requestFile = RequestBody.create(mediaType, nricFrontImageFile)
            nricFrontImageFileBody = MultipartBody.Part.createFormData("nric_front_image", nricFrontImageFile.getName(), requestFile)
        }
        if (nricBackImageFile != null) {
            val requestFile = RequestBody.create(mediaType, nricBackImageFile)
            nricBackImageFileBody = MultipartBody.Part.createFormData("nric_back_image", nricBackImageFile.getName(), requestFile)
        }

        subscription = App.apiService!!!!.runner_edit_profile(
            RequestBody.create(mediaType, reqData["user_id"].toString()),
            RequestBody.create(mediaType, reqData["first_name"].toString()),
            RequestBody.create(mediaType, reqData["last_name"].toString()),
            RequestBody.create(mediaType, reqData["email"].toString()),
            RequestBody.create(mediaType, reqData["country_code"].toString()),
            RequestBody.create(mediaType, reqData["mobile"].toString()),
            RequestBody.create(mediaType, reqData["address"].toString()),
            RequestBody.create(mediaType, reqData["bank_name"].toString()),
            RequestBody.create(mediaType, reqData["transport_category_id"].toString()),
            RequestBody.create(mediaType, reqData["account_number"].toString()),
            RequestBody.create(mediaType, reqData["account_holder_name"].toString()),
            RequestBody.create(mediaType, reqData["nric_number"].toString()),
            RequestBody.create(mediaType, reqData["declaration"].toString()),
            RequestBody.create(mediaType, reqData["branch_code"].toString()),
            RequestBody.create(mediaType, reqData["bank_code"].toString()),
            RequestBody.create(mediaType, reqData["postal_code"].toString()),
            imageBody,
            nricFrontImageFileBody,
            nricBackImageFileBody
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(
                        apiResponse!!.success(
                            result
                        )
                    )
                },
                { throwable ->
                    responseLiveData.postValue(
                        apiResponse!!.error(
                            throwable
                        )
                    )
                }
            )
    }

    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}