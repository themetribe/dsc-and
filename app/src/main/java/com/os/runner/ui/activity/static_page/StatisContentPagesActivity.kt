package com.os.runner.ui.activity.static_page

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import androidx.lifecycle.ViewModelProvider
import com.app.dstarrunner.R
import com.os.runner.ui.activity.BaseBindingActivity
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import kotlinx.android.synthetic.main.static_page_layout.*


class StatisContentPagesActivity : BaseBindingActivity(), View.OnClickListener {

    private var viewModel: StaticContentPagesViewModel? = null
    private var slug=""

    override fun createContentView() {
        viewModel = ViewModelProvider(this).get(StaticContentPagesViewModel::class.java)
        setContentView(R.layout.static_page_layout)
    }

    override fun createActivityObject() {
        mActivity = this
    }

    companion object {
        fun startActivity(activity: Activity, bundle: Bundle?) {
            val intent = Intent(activity, StatisContentPagesActivity::class.java)
            if (bundle != null) intent.putExtra("bundle", bundle)
            activity.startActivity(intent)
        }
    }

    /*let AboutUs                                                 = "https://72.octaldevs.com/dstarrunner/page/about-us"
    let TS                                                      = "https://72.octaldevs.com/dstarrunner/page/terms-and-condition"
    let PP                                                      = "https://72.octaldevs.com/dstarrunner/page/privacy-policy"
    let CC                                                      = "https://72.octaldevs.com/dstarrunner/page/code-of-conduct"
*/
    override fun initializeObject() {
        slug = intent.getBundleExtra("bundle").getString("slug") as String

        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setLoadWithOverviewMode(true)
        webView.getSettings().setUseWideViewPort(true)
        webView.getSettings().setBuiltInZoomControls(true)
        webView.getSettings().setPluginState(WebSettings.PluginState.ON)

        if(slug.equals("terms_condition")){
            toolbarLayout.toolbarTitle.text = getString(R.string.terms_condition)
            webView.loadUrl("https://72.octaldevs.com/dstarrunner/page/terms-and-condition")
        }else if(slug.equals("policy_privacy")){
            toolbarLayout.toolbarTitle.text = getString(R.string.policy_privacy)
            webView.loadUrl("https://72.octaldevs.com/dstarrunner/page/privacy-policy")
        }else if(slug.equals("code_of_conduct")){
            toolbarLayout.toolbarTitle.text = getString(R.string.code_of_conduct)
            webView.loadUrl("https://72.octaldevs.com/dstarrunner/page/code-of-conduct")
        }else{
            toolbarLayout.toolbarTitle.text = getString(R.string.about_us)
            webView.loadUrl("https://72.octaldevs.com/dstarrunner/page/about-us")
        }
        toolbarLayout.imgBack.visibility = View.VISIBLE
        toolbarLayout.imgBack.setOnClickListener(this)
    }

    override fun setListeners() {
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.imgBack -> finish()
        }
    }

}