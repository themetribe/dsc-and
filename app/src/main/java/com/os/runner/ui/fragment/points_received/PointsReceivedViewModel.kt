package com.os.runner.ui.fragment.points_received

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.os.runner.application.ApiResponse
import com.os.runner.model.DefaultPojo
import io.reactivex.disposables.Disposable

class PointsReceivedViewModel : ViewModel() {

    var responseLiveData = MutableLiveData<ApiResponse<DefaultPojo>>()
    var apiResponse: ApiResponse<DefaultPojo>? = null
    private var subscription: Disposable? = null


    init {
        apiResponse = ApiResponse(ApiResponse.Status.LOADING, null, null)
    }


    /*fun myOrderHistory(jsonObject: JsonObject) {
        subscription = App.apiService!!.myOrderHistory(jsonObject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseLiveData.postValue(apiResponse!!.loading())
            }
            .subscribe(
                { result ->
                    responseLiveData.postValue(apiResponse!!.success(result))
                },
                { throwable ->
                    responseLiveData.postValue(apiResponse!!.error(throwable))
                }
            )
    }*/


    fun disposeSubscriber() {
        if (subscription != null)
            subscription!!.dispose()
    }
}