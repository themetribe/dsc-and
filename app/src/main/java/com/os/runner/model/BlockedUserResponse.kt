package com.os.runner.model

import java.io.Serializable

data class BlockedUserResponse(
    val response: ResponseBlockedUser
)

data class ResponseBlockedUser(
    val `data`: List<DataBlockedUser>,
    val message: String,
    val status: Int
)
data class DataBlockedUser(
    var id: Int? =null,
    val user_id: Int? =null,
    val friend_id: Int? =null,
    val report_msg: String="",
    val status: String="",
    val created_at: String="",
    val updated_at: String="",
    val friend_info: DataFriendInfo? =null

):Serializable

data class DataFriendInfo(
    var id: Int? =null,
    val type: String="",
    val name: String="",
    val mobile: String="",
    val email: String="",
    val email_verified_at: String="",
    val isOtpVerified: String="",
    val snapchat_id: String="",
    val gender: String="",
    val age: String="",
    val dob: String="",
    val bio: String="",
    val country: String="",
    val show_dob: String="",
    val search_gender: String="",
    val search_country: String="",
    val want_to_meet: String="",
    val image: String="",
    val device_id: String="",
    val device_type: String="",
    val latitude: String="",
    val longitude: String="",
    val gems: String="",
    val status: String="",
    val created_at: String="",
    val updated_at: String=""
):Serializable