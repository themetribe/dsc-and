package com.os.runner.ui.adapter

import android.app.Activity
import android.view.View
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.app.dstarrunner.R
import com.os.runner.model.EarningData
import com.os.runner.utility.UtilityMethod
import kotlinx.android.synthetic.main.row_earning.view.*

class EarningAdapter(
    private val context: Activity?,
    private val list: ArrayList<EarningData>,
    private val onClickListener: View.OnClickListener?
) : RecyclerBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int = R.layout.row_earning

    override fun getViewModel(position: Int): Any? = list[position]

    override fun putViewDataBinding(viewDataBinding: ViewDataBinding, position: Int) {
        Glide.with(context!!).load(list[position].user_image).placeholder(R.drawable.default_user_pic).into(viewDataBinding.root.imvCustomerProfile)
        viewDataBinding.root.txtCustomerName.text = list[position].user_name
        viewDataBinding.root.txtSName.text = list[position].service_name
        viewDataBinding.root.txtCreatedDate.text = UtilityMethod.parseDateFormated(list[position].created_at)
        viewDataBinding.root.txtEarningAmount.text = context.getString(R.string.singapur_doller)+list[position].earning
        viewDataBinding.root.txtTipAmount.text = context.getString(R.string.singapur_doller)+list[position].tip
        viewDataBinding.root.txtPickupAddress.text = list[position].pickup_info.address
        if(list[position].drop_info!!.size!=0){
            viewDataBinding.root.txtDropOffAddress.text = list[position].drop_info!![0].address
        }else{
            viewDataBinding.root.rlDropOffHelpMeQueue.visibility  = View.GONE
        }
    }

    override fun getItemCount(): Int = list.size
}