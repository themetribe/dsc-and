package com.os.runner.model

data class StaticPagesResponse(
    val response: ResponseStaticPages
)

data class ResponseStaticPages(
    val `data`: DataStaticPages,
    val message: String,
    val status: Int
)

data class DataStaticPages(
    val description: String,
    val slug: String,
    val name: String
)